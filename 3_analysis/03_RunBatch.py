import sys
import subprocess

all_configs = [
  # 2016 config

  ["config_2016.json", "ttbar", "medium_wp", "no_iso", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2016.json", "ttbar", "medium_wp", "no_iso", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2016.json", "ttbar", "medium_wp", "FCTight", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2016.json", "ttbar", "medium_wp", "FCTight", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2016.json", "ttbar", "medium_wp", "FCTTO", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2016.json", "ttbar", "medium_wp", "FCTTO", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2016.json", "ttbar", "highpt_wp", "no_iso", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2016.json", "ttbar", "highpt_wp", "no_iso", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2016.json", "ttbar", "highpt_wp", "FCTight", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2016.json", "ttbar", "highpt_wp", "FCTight", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2016.json", "ttbar", "highpt_wp", "FCTTO", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2016.json", "ttbar", "highpt_wp", "FCTTO", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2016.json", "wjets", "medium_wp", "no_iso", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2016.json", "wjets", "medium_wp", "no_iso", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2016.json", "wjets", "medium_wp", "FCTight", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2016.json", "wjets", "medium_wp", "FCTight", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2016.json", "wjets", "medium_wp", "FCTTO", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2016.json", "wjets", "medium_wp", "FCTTO", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2016.json", "wjets", "highpt_wp", "no_iso", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2016.json", "wjets", "highpt_wp", "no_iso", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2016.json", "wjets", "highpt_wp", "FCTight", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2016.json", "wjets", "highpt_wp", "FCTight", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2016.json", "wjets", "highpt_wp", "FCTTO", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2016.json", "wjets", "highpt_wp", "FCTTO", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  # 2017 config

  ["config_2017.json", "ttbar", "medium_wp", "no_iso", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2017.json", "ttbar", "medium_wp", "no_iso", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2017.json", "ttbar", "medium_wp", "FCTight", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2017.json", "ttbar", "medium_wp", "FCTight", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2017.json", "ttbar", "medium_wp", "FCTTO", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2017.json", "ttbar", "medium_wp", "FCTTO", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2017.json", "ttbar", "highpt_wp", "no_iso", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2017.json", "ttbar", "highpt_wp", "no_iso", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2017.json", "ttbar", "highpt_wp", "FCTight", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2017.json", "ttbar", "highpt_wp", "FCTight", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2017.json", "ttbar", "highpt_wp", "FCTTO", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2017.json", "ttbar", "highpt_wp", "FCTTO", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2017.json", "wjets", "medium_wp", "no_iso", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2017.json", "wjets", "medium_wp", "no_iso", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2017.json", "wjets", "medium_wp", "FCTight", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2017.json", "wjets", "medium_wp", "FCTight", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2017.json", "wjets", "medium_wp", "FCTTO", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2017.json", "wjets", "medium_wp", "FCTTO", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2017.json", "wjets", "highpt_wp", "no_iso", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2017.json", "wjets", "highpt_wp", "no_iso", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2017.json", "wjets", "highpt_wp", "FCTight", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2017.json", "wjets", "highpt_wp", "FCTight", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2017.json", "wjets", "highpt_wp", "FCTTO", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2017.json", "wjets", "highpt_wp", "FCTTO", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  # 2018 config

  ["config_2018.json", "ttbar", "medium_wp", "no_iso", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2018.json", "ttbar", "medium_wp", "no_iso", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2018.json", "ttbar", "medium_wp", "FCTight", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2018.json", "ttbar", "medium_wp", "FCTight", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2018.json", "ttbar", "medium_wp", "FCTTO", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2018.json", "ttbar", "medium_wp", "FCTTO", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2018.json", "ttbar", "highpt_wp", "no_iso", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2018.json", "ttbar", "highpt_wp", "no_iso", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2018.json", "ttbar", "highpt_wp", "FCTight", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2018.json", "ttbar", "highpt_wp", "FCTight", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2018.json", "ttbar", "highpt_wp", "FCTTO", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2018.json", "ttbar", "highpt_wp", "FCTTO", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2018.json", "wjets", "medium_wp", "no_iso", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2018.json", "wjets", "medium_wp", "no_iso", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2018.json", "wjets", "medium_wp", "FCTight", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2018.json", "wjets", "medium_wp", "FCTight", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2018.json", "wjets", "medium_wp", "FCTTO", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2018.json", "wjets", "medium_wp", "FCTTO", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2018.json", "wjets", "highpt_wp", "no_iso", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2018.json", "wjets", "highpt_wp", "no_iso", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2018.json", "wjets", "highpt_wp", "FCTight", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2018.json", "wjets", "highpt_wp", "FCTight", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2018.json", "wjets", "highpt_wp", "FCTTO", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2018.json", "wjets", "highpt_wp", "FCTTO", "nominal", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  #
  #
  # b-tag variations

  ["config_2016.json", "ttbar", "medium_wp", "no_iso", "btag_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2016.json", "ttbar", "medium_wp", "no_iso", "btag_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2016.json", "ttbar", "highpt_wp", "no_iso", "btag_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2016.json", "ttbar", "highpt_wp", "no_iso", "btag_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2016.json", "wjets", "medium_wp", "no_iso", "btag_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2016.json", "wjets", "medium_wp", "no_iso", "btag_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2016.json", "wjets", "highpt_wp", "no_iso", "btag_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2016.json", "wjets", "highpt_wp", "no_iso", "btag_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  # 2017 config

  ["config_2017.json", "ttbar", "medium_wp", "no_iso", "btag_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2017.json", "ttbar", "medium_wp", "no_iso", "btag_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2017.json", "ttbar", "highpt_wp", "no_iso", "btag_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2017.json", "ttbar", "highpt_wp", "no_iso", "btag_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2017.json", "wjets", "medium_wp", "no_iso", "btag_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2017.json", "wjets", "medium_wp", "no_iso", "btag_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2017.json", "wjets", "highpt_wp", "no_iso", "btag_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2017.json", "wjets", "highpt_wp", "no_iso", "btag_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  # 2018 config

  ["config_2018.json", "ttbar", "medium_wp", "no_iso", "btag_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2018.json", "ttbar", "medium_wp", "no_iso", "btag_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2018.json", "ttbar", "highpt_wp", "no_iso", "btag_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2018.json", "ttbar", "highpt_wp", "no_iso", "btag_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2018.json", "wjets", "medium_wp", "no_iso", "btag_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2018.json", "wjets", "medium_wp", "no_iso", "btag_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2018.json", "wjets", "highpt_wp", "no_iso", "btag_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2018.json", "wjets", "highpt_wp", "no_iso", "btag_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  #
  #
  # met variations
  # 2016 config

  ["config_2016.json", "ttbar", "medium_wp", "no_iso", "met_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2016.json", "ttbar", "medium_wp", "no_iso", "met_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2016.json", "ttbar", "highpt_wp", "no_iso", "met_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2016.json", "ttbar", "highpt_wp", "no_iso", "met_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2016.json", "wjets", "medium_wp", "no_iso", "met_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2016.json", "wjets", "medium_wp", "no_iso", "met_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2016.json", "wjets", "highpt_wp", "no_iso", "met_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2016.json", "wjets", "highpt_wp", "no_iso", "met_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  # 2017 config

  ["config_2017.json", "ttbar", "medium_wp", "no_iso", "met_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2017.json", "ttbar", "medium_wp", "no_iso", "met_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2017.json", "ttbar", "highpt_wp", "no_iso", "met_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2017.json", "ttbar", "highpt_wp", "no_iso", "met_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2017.json", "wjets", "medium_wp", "no_iso", "met_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2017.json", "wjets", "medium_wp", "no_iso", "met_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2017.json", "wjets", "highpt_wp", "no_iso", "met_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2017.json", "wjets", "highpt_wp", "no_iso", "met_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  # 2018 config

  ["config_2018.json", "ttbar", "medium_wp", "no_iso", "met_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2018.json", "ttbar", "medium_wp", "no_iso", "met_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2018.json", "ttbar", "highpt_wp", "no_iso", "met_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2018.json", "ttbar", "highpt_wp", "no_iso", "met_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2018.json", "wjets", "medium_wp", "no_iso", "met_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2018.json", "wjets", "medium_wp", "no_iso", "met_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2018.json", "wjets", "highpt_wp", "no_iso", "met_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2018.json", "wjets", "highpt_wp", "no_iso", "met_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  #
  #
  # jet pt variations
  # 2016 config

  ["config_2016.json", "ttbar", "medium_wp", "no_iso", "jetpt_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2016.json", "ttbar", "medium_wp", "no_iso", "jetpt_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2016.json", "ttbar", "highpt_wp", "no_iso", "jetpt_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2016.json", "ttbar", "highpt_wp", "no_iso", "jetpt_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2016.json", "wjets", "medium_wp", "no_iso", "jetpt_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2016.json", "wjets", "medium_wp", "no_iso", "jetpt_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2016.json", "wjets", "highpt_wp", "no_iso", "jetpt_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2016.json", "wjets", "highpt_wp", "no_iso", "jetpt_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  # 2017 config

  ["config_2017.json", "ttbar", "medium_wp", "no_iso", "jetpt_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2017.json", "ttbar", "medium_wp", "no_iso", "jetpt_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2017.json", "ttbar", "highpt_wp", "no_iso", "jetpt_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2017.json", "ttbar", "highpt_wp", "no_iso", "jetpt_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2017.json", "wjets", "medium_wp", "no_iso", "jetpt_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2017.json", "wjets", "medium_wp", "no_iso", "jetpt_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2017.json", "wjets", "highpt_wp", "no_iso", "jetpt_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2017.json", "wjets", "highpt_wp", "no_iso", "jetpt_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  # 2018 config

  ["config_2018.json", "ttbar", "medium_wp", "no_iso", "jetpt_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2018.json", "ttbar", "medium_wp", "no_iso", "jetpt_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2018.json", "ttbar", "highpt_wp", "no_iso", "jetpt_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2018.json", "ttbar", "highpt_wp", "no_iso", "jetpt_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2018.json", "wjets", "medium_wp", "no_iso", "jetpt_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2018.json", "wjets", "medium_wp", "no_iso", "jetpt_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],

  ["config_2018.json", "wjets", "highpt_wp", "no_iso", "jetpt_var", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
  ["config_2018.json", "wjets", "highpt_wp", "no_iso", "jetpt_var", "endcaps", "HLT_mu26_ivarmedium_OR_HLT_mu50"],
]


if __name__ == "__main__":
  print len(all_configs)
  config = all_configs[int(sys.argv[-1])]
  #subprocess.call(["./HACK.py", "config_2016.json", "ttbar", "medium_wp", "no_iso", "nominal", "barrel", "HLT_mu26_ivarmedium_OR_HLT_mu50"])
  subprocess.call(["./HACK.py", config[0], config[1], config[2], config[3], config[4], config[5], config[6]]) 
