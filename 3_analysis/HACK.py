#!/usr/bin/env python2
import ROOT
import glob
from array import array
import json
import os
import sys
import argparse

ROOT.gROOT.SetBatch(ROOT.kTRUE) # non-interactive
ROOT.TH1.SetDefaultSumw2()      # for error propagation
ROOT.gStyle.SetOptStat(0)       # turn off box with histogram info
#ROOT.TH1.AddDirectory(ROOT.kFALSE) # do not remove histograms when files are closed

ROOT.gROOT.LoadMacro("atlasstyle/AtlasStyle.C")
ROOT.gROOT.LoadMacro("atlasstyle/AtlasLabels.C")


##############################################################################
#
# config handling
#
##############################################################################

def read_config(config_path):
  """
  read external json config
  :param config_path: path to json file containing config
  :returns: dict containing all config information
  """
  if config_path == __file__:
    print '# execute via \"python', __file__, 'path/to/config.json\"'
    raise SystemExit

  try:
    with open(config_path) as f:
      text_without_comments = ""
      for line in f.readlines():
        if line.lstrip("\t ")[0:2] == "//":
          continue # skip comments
        else:
          text_without_comments += line

      config = json.loads(text_without_comments) # check e.g. http://jsonparseronline.com/ if config is invalid
      config["MCStackOrder"] = [u"allMC"]
      config["DefaultCuts"] = u"(1==1)"
      config["DownloadFolder"] = u"../2_download"
      config["Weights"] = u"weight_mc*weight_pileup*weight_norm*weight_jvt*weight_bTagSF_MV2c10_77*weight_indiv_SF_MU_ID*weight_indiv_SF_MU_Isol*weight_indiv_SF_MU_TTVA*((mcChannelNumber!=364162) || (eventNumber!=2120342 && eventNumber!=2345564 && eventNumber!=4368178 && eventNumber!=3838541 && eventNumber!=9251681 && eventNumber!=8692844 && eventNumber!=7801624))*((mcChannelNumber!=364165) || (eventNumber!=1705696 && eventNumber!=2934190 && eventNumber!=3034681 && eventNumber!=3962367 && eventNumber!=3844585))*((mcChannelNumber!=364168) || (eventNumber!=1012352))*((mcChannelNumber!=364162) || (eventNumber!=17827521 && eventNumber!=11285952 && eventNumber!=10363655 && eventNumber!=14399953 && eventNumber!=15930424 && eventNumber!=11860722 && eventNumber!=16053304 && eventNumber!=11573496 && eventNumber!=16989000 && eventNumber!=17048735 && eventNumber!=13501685 && eventNumber!=54165515 && eventNumber!=54148722 && eventNumber!=53547476))*((mcChannelNumber!=364163) || (eventNumber!=9363572 && eventNumber!=8921075))*((mcChannelNumber!=364165) || (eventNumber!=9985436 && eventNumber!=8028996 && eventNumber!=7835598 && eventNumber!=10555373 && eventNumber!=10873645))*((mcChannelNumber!=364166) || (eventNumber!=5741275))*((mcChannelNumber!=364168) || (eventNumber!=8397442 && eventNumber!=7824699))*((mcChannelNumber!=364162) || (eventNumber!=67045574 && eventNumber!=69948097 && eventNumber!=69969708 && eventNumber!=67760504 && eventNumber!=67782234 && eventNumber!=69760807 && eventNumber!=70456784 && eventNumber!=70737475 && eventNumber!=70900233 && eventNumber!=56236619 && eventNumber!=59502744 && eventNumber!=60558323 && eventNumber!=57428055 && eventNumber!=62742642 && eventNumber!=61966206 && eventNumber!=62581802 && eventNumber!=64693225 && eventNumber!=60056768))*((mcChannelNumber!=364163) || (eventNumber!=53232141 && eventNumber!=32595423))*((mcChannelNumber!=364165) || (eventNumber!=19038204 && eventNumber!=19353282 && eventNumber!=14333636 && eventNumber!=16381369))*((mcChannelNumber!=364166) || (eventNumber!=8148834))*((mcChannelNumber!=364168) || (eventNumber!=22004592 && eventNumber!=23054020 && eventNumber!=16318618))*((mcChannelNumber!=364193) || (eventNumber!=18987824))"
      config["Plots"]["mu_pt"]["Bins"] = [ 0.0, 60.0, 80.0, 100.0, 120.0, 140.0, 160.0, 180.0, 200.0, 250.0, 300.0, 600.0]
      print "!!! overwrote MCStackOrder, DefaultCuts, DownloadFolder, Weights, mu_pt bin"
    return config

  except Exception as e:
    print "# unable to read config file:\n", e
    print "# line count is wrong if comments are used in config"
    raise SystemExit


##############################################################################
#
# class definitions
#
##############################################################################

class dataset():
  """
  class representing a dateset (data or MC root file)
  and information contained therein
  """
  def __init__(self, s):
    """
    :param s: string containing path to root file
    """
    self.path      = s
    self.treename  = 'nominal'                         # currently hardcoded
    self.dsid      = s.split('/')[-2].split('.')[2]
    self.category  = None
    self.hist      = None                              # needs to be set via setYieldHist()
    self.tag_hist  = None                              # needs to be set via setEff()
    self.probe_hist = None                              # needs to be set via setEff()
    self.isData    = 'physics_Main' in s.split('/')[-2] or 'AllYear' in s.split('/')[-2]

  def __repr__(self):
    return (' ').join([self.category, self.dsid])

  def getCategory(self, config):
    """
    :param config: configuration object built by read_config()
    """
    if self.isData:
      self.category = 'data'
    else:
      matches = []
      for cat in config["MCStackOrder"]:
        if int(self.dsid) in config["CategoryDefinitions"][cat]["DSIDs"]:
          #print 'identified', self.dsid, 'as', cat
          matches.append(cat)
      if len(matches)!=1:
        print self.dsid, "not uniquely matched to any category in MCStackOrder, will ignore sample"
        return -1
      else:
        self.category = matches[0]

  def setYieldHist(self, variable, cut, bins):
    """
    need to think whether this should become part of the constructor
    might be useful to have standalone if many variables are plotted
    :param variable: variable to bin in
    :param cut: cut applied to histogram
    :param bins: binning used
    """
    # create a template
    bin_array = array('d', bins)
    template = ROOT.TH1D('template', '', len(bins)-1, bin_array)

    # define internal name of histogram, can the be found via ROOT.gROOT.FindObject(hist_name)
    hist_name = "hist_" + self.category + "_" + self.dsid

    can = ROOT.TCanvas("tmp_can", "temporary canvas", 800, 800)
    f         = ROOT.TFile(self.path,"READ")
    t         = f.Get(self.treename)
    self.hist = template.Clone(hist_name)     # create histogram from template with proper binning
    t.Draw(variable + ">>" + hist_name, cut)  # draw to histogram
    self.hist.SetDirectory(0)                 # keep alive after file is closed
    f.Close()
    can.Close()

  def setEff(self, variable, cut, bins, region, trigger, year):
    """
    need to think whether this should become part of the constructor
    might be useful to have standalone if many variables are plotted
    :param variable: variable to bin in
    :param cut: cut applied to histogram
    :param bins: binning used
    """
    # create a template
    bin_array = array('d', bins)
    template = ROOT.TH1D('template', '', len(bins)-1, bin_array)

    # define internal name of histogram, can the be found via ROOT.gROOT.FindObject(hist_name)
    hist_name_tag   = "hist_tag_" + self.category + "_" + self.dsid
    hist_name_probe = "hist_probe_" + self.category + "_" + self.dsid

    probe_cut = 'probe_' + trigger + '_' + year
    if trigger == "HLT_mu26_ivarmedium_OR_HLT_mu50":
      probe_cut = '(probe_HLT_mu50_' + year + "||" + 'probe_HLT_mu26_ivarmedium_' + year + ')'

    if region == "barrel":
      #print "barrel region"
      region_cut = '(abs(mu_eta[0])<=1.05)' # barrel
    elif region == "endcaps":
      #print "endcaps region"
      region_cut = '(abs(mu_eta)>1.05)' # endcaps

    # TODO FIX THIS HACK
    #cut += '*(mu>40)'

    can = ROOT.TCanvas("tmp_can", "temporary canvas", 800, 800)
    f     = ROOT.TFile(self.path,"READ")
    t     = f.Get(self.treename)
    tag   = template.Clone(hist_name_tag)     # create histogram from template with proper binning
    probe = template.Clone(hist_name_probe)     # create histogram from template with proper binning
    t.Draw(variable + ">>" + hist_name_tag, cut + '*' + region_cut)  # draw to histogram
    t.Draw(variable + ">>" + hist_name_probe, cut + '*' + probe_cut + '*' + region_cut)  # draw to histogram
    self.tag   = tag
    self.probe = probe
    self.tag.SetDirectory(0)                 # keep alive after file is closed
    self.probe.SetDirectory(0)                 # keep alive after file is closed
    #print self.tag,
    #print self.probe
    f.Close()
    can.Close()


class histogramCollection():
  """
  class representing summed histograms
  contains styling information to modify in bulk more easily
  """
  def __init__(self, cat, tag, probe, title, isData):
    """
    :param cat: category, e.g. tt, W+jets, data, ...
    :param hist: ROOT histogram
    :param title: title string to use for legend
    :param isData: bool to flag data
    """
    self.category = cat
    self.tag      = tag
    self.probe    = probe
    #self.nevts    = hist.Integral()
    self.title    = title
    self.isData   = isData
    self.ratio    = self.probe.Clone()
    self.ratio.Divide(self.tag)

  def __repr__(self):
    return (' ').join([self.title, str(self.hist)])

  def setStyle(self, color=ROOT.kBlack):
    """
    :param color: color to use for plot
    """
    self.ratio.SetMarkerStyle(ROOT.kFullCircle)
    self.ratio.SetLineWidth(2)
    self.ratio.SetMaximum(1.0)
    self.ratio.SetMinimum(0.0)

    if self.isData:
      self.ratio.SetLineColor(color)
      self.ratio.SetMarkerSize(2)

    else:
      self.ratio.SetLineColor(color)
      self.ratio.SetFillColor(color)
      self.ratio.SetMarkerSize(0)
      self.ratio.SetLineWidth(2)


##############################################################################
#
# utility functions
#
##############################################################################

def check_dataset_list(dslist):
  wjets_count = 0
  data_count = 0
  ttbar_count = 0
  for d in dslist:
    if d.dsid != "AllYear":
      cur_dsid = int(d.dsid)
    else:
      cur_dsid = -99
    if cur_dsid >= 364156 and cur_dsid <= 364197: wjets_count += 1
    if d.isData: data_count += 1
    if cur_dsid == 410470: ttbar_count += 1
  print "data:", data_count, "ttbar:", ttbar_count, "wjets:", wjets_count
  if data_count != 4 or ttbar_count != 1:
    print "wrong amount of samples"
    #raise SystemExit


def get_datasets(config, sub_path):
  """
  return all relevant datasets, using info in the config
  optional setting to match only a specific category
  :param config: configuration object built by read_config()
  :returns: a list of dataset instances
  """
  dataset_list = []
  # search recursively through folder containing downloaded files
  search_path = config["DownloadFolder"] + "/" + sub_path
  print '# searching for files with name', config["OutputFilename"], 'in', search_path
  for x in os.walk(search_path):
    # identify merged samples (these will be used)
    if config["OutputFilename"] in x[2]:
      new_dataset = dataset(x[0]+'/'+config["OutputFilename"])

      if config["mcCampaign"] not in x[0] and config["dataIdentifier"] not in x[0]:
        continue # skip mc16a or mc16c
      elif new_dataset.getCategory(config) == -1:
        continue # ignore dataset, not matched via config
      #elif "2017" in x[0] or "2018" in x[0] or "2016" in x[0]: continue # DEBUG
      else:
        print '# found', new_dataset.category, 'sample', new_dataset.dsid,'in', x[0]
        dataset_list.append(new_dataset)
  check_dataset_list(dataset_list)
  return dataset_list


def select_plot(config):
  """
  let the user select one plot from the options in the config
  :param config: configuration object built by read_config()
  :returns: the key of the chosen plot
  """
  print '\n# select plot to produce:'
  plots_available = config["Plots"].keys()  # find all possible plots defined in config
  plots_available.sort()
  for i, p in enumerate(plots_available):
    print i, '-', config["Plots"][p]["Title"]

  plot_choice = '-1'
  while plot_choice not in [str(x) for x in range(len(plots_available))]:
    plot_choice = raw_input("select number of chosen plot: ")

  return plots_available[int(plot_choice)]


def sum_histograms(h_list, sum_name):
  """
  sum together a list of histograms
  :param h_list: list of histograms, usually TH1Ds
  :param sum_name: internal name of histogram containing sum
  :returns: return summed histogram
  """
  # sum histograms together if list contains multiple entries
  if isinstance(h_list,list):
    hist_sum = h_list[0].Clone(sum_name)
    for h in h_list[1:]:
      hist_sum.Add(h)
  else:
    hist_sum = h_list.Clone(sum_name)
  return hist_sum


def produce_all_histograms(config, datasets, plot_key, region, trigger, year, extra_cuts):
  """
  calls setYieldHist() for all datasets in a given list
  currently only one hist per dataset is supported
  :param config: configuration object built by read_config()
  :param datasets: list of datasets
  :param plot_key: string to select plot from config, can be produced via select_plot()
  """
  cut_data = config["DefaultCuts"] + '*' + extra_cuts
  cut_mc   = config["DefaultCuts"] + '*' + config["Weights"] + '*' + str(config["Lumi"]) + '*' + extra_cuts
  variable = config["Plots"][plot_key]["Variable"]
  bins     = config["Plots"][plot_key]["Bins"]

  for d in datasets:
    if d.isData:
      #d.setYieldHist(variable, cut_data, bins)
      d.setEff(variable, cut_data, bins, region, trigger, year)
    else:
      #d.setYieldHist(variable, cut_mc, bins)
      d.setEff(variable, cut_mc, bins, region, trigger, year)


def produce_plot(config, datasets, plot_key):
  """
  produce a plot and save to disk
  :param config: configuration object built by read_config()
  :param datasets: dataset list produced by get_datasets()
  :param plot_key: string to select plot from config, can be produced via select_plot()
  """
  title = config["Plots"][plot_key]["Title"] # x axis title
  mc_categories_for_plot = config["MCStackOrder"]

  # dict containing all information needed for plots ("plot category dict" = pcd)
  pcd = {}

  # process all categories defined in the config, produce summed histograms
  # and write all important information to pcd object
  for cat in mc_categories_for_plot + ['data']:
    isData = (cat == 'data')
    cat_title = config["CategoryDefinitions"][cat]["Title"]
    cat_color = config["CategoryDefinitions"][cat]["Color"]
    hists_matched = [d.hist for d in datasets if d.category == cat]

    print '# processing', cat, ': found', len(hists_matched), 'histogram(s)'
    if len(hists_matched)>0:
      pcd[cat] = histogramCollection(cat, sum_histograms(hists_matched, cat), cat_title, isData)
      pcd[cat].setStyle(eval(cat_color)) # color needs to be translated via eval()
    else:
      pcd[cat] = None
      pass # could add an exception here to deal with categories for which no histograms exist

  # create reference to data histogram for plotting
  if pcd['data'] == None:
    print "# no data histogram found, aborting"
    return -1
  data_hist = pcd['data'].hist

  # build a stack of all MC
  mc_stack = ROOT.THStack(title + '_stack', '')
  for cat in reversed(mc_categories_for_plot):
    if pcd[cat] != None: # verify that a histogram exist
      mc_stack.Add(pcd[cat].hist)

  # clone and style full stack of MC, needed to plot uncertainties
  mc_stack_sum = mc_stack.GetStack().Last().Clone(title + "_mc_stack_sum")
  mc_stack_sum.SetFillColor(ROOT.kGray+2)
  mc_stack_sum.SetLineColor(ROOT.kGray+2)
  mc_stack_sum.SetFillStyle(3354)

  can = ROOT.TCanvas("c1", "canvas_1", 1200, 1200)
  pad_main = ROOT.TPad("pad_main", "", 0, 0.3, 1, 1)
  pad_main.SetBottomMargin(0.05)
  #pad_main.SetGridx()
  pad_main.Draw()
  pad_ratio = ROOT.TPad("pad_ratio", "", 0, 0.05, 1, 0.3)
  pad_ratio.SetBottomMargin(0.30) # needs to be large enough to not cut off title
  #pad_ratio.SetGridx()
  pad_ratio.Draw()

  pad_main.cd()

  # create legend
  leg = ROOT.TLegend(0.62,0.6,0.88,0.88)
  leg.SetBorderSize(0)
  leg.AddEntry(data_hist, 'data: ' + str(round(pcd['data'].nevts,1)), 'lep')
  for cat in mc_categories_for_plot:
    if pcd[cat] != None: # verify that a histogram exist
      leg.AddEntry(pcd[cat].hist, pcd[cat].title + ': ' + str(round(pcd[cat].nevts,1)), 'F')

  mc_stack.Draw("Fhist")            # draw MC stack of histograms
  mc_stack_sum.Draw("SAME E2")      # draw error for MC stack
  data_hist.Draw("SAME E")          # draw data
  leg.Draw("SAME")                  # draw legend

  # set style options for plot
  mc_stack.SetMinimum(0)
  y_max = max(data_hist.GetMaximum(), mc_stack.GetMaximum())
  mc_stack.SetMaximum(config["PlotYMaxFactor"]*y_max)

  ROOT.ATLASLabel(0.15, 0.85, "work in progress")

  # luminosity label
  LumiLabel = ROOT.TLatex()
  LumiLabel.SetTextFont(42)
  LumiLabel.SetTextSize(0.035)
  LumiLabel.SetNDC()
  LumiLabel.DrawLatex(0.15, 0.80, config["LumiLabel"])

  # custom extra label
  customLabel = ROOT.TLatex()
  customLabel.SetTextFont(42)
  customLabel.SetTextSize(0.035)
  customLabel.SetNDC()
  customLabel.DrawLatex(0.15, 0.75,config["customStringForPlot"])

  # ##########################################################################
  # do the data/MC ratio next
  # ##########################################################################

  # create a copy of the summed mc contribution with errors set to 0
  # in order to propagate only the data stat unc. to the ratio
  mc_stack_sum_no_errors = mc_stack_sum.Clone()
  for ibin in range(mc_stack_sum_no_errors.GetNbinsX()+2):
    mc_stack_sum_no_errors.SetBinError(ibin, 0)

  # create another copy of the summed mc contribution, set the
  # bin contents to 1, keep the relative uncertainty
  # this then contains the unc. contribution to the ratio from MC
  mc_error_contribution = mc_stack_sum.Clone()
  for ibin in range(mc_error_contribution.GetNbinsX()+2):
    if mc_error_contribution.GetBinContent(ibin) != 0:
      rel_unc = mc_error_contribution.GetBinError(ibin) / float(mc_error_contribution.GetBinContent(ibin))
    else:
      rel_unc = 0 # for empty bins
    mc_error_contribution.SetBinContent(ibin, 1)
    mc_error_contribution.SetBinError(ibin, rel_unc)

  # calculate ratio, using only the data uncertainties
  ratio = data_hist.Clone()
  ratio.Divide(mc_stack_sum_no_errors)

  # style settings
  ratio_plot_range = config["ratioPlotRange"]
  mc_error_contribution.SetMinimum(1-ratio_plot_range)
  mc_error_contribution.SetMaximum(1+ratio_plot_range)
  mc_error_contribution.SetMarkerStyle(ROOT.kFullCircle)
  mc_error_contribution.GetXaxis().SetTitle(title)
  mc_error_contribution.GetXaxis().SetTitleSize(0.12)
  mc_error_contribution.GetXaxis().SetTitleOffset(1.1)
  mc_error_contribution.GetXaxis().SetLabelSize(0.1)
  mc_error_contribution.GetYaxis().SetLabelSize(0.1)
  #mc_error_contribution.GetYaxis().SetNdivisions(505)
  mc_error_contribution.GetYaxis().SetTitle("data/MC")
  mc_error_contribution.GetYaxis().SetTitleSize(0.1)
  mc_error_contribution.GetYaxis().SetTitleOffset(0.4)

  # create line through unity
  x1 = mc_error_contribution.GetXaxis().GetBinLowEdge(1)
  x2 = mc_error_contribution.GetXaxis().GetBinCenter(mc_error_contribution.GetNbinsX())
  x2 += 0.5*mc_error_contribution.GetBinWidth(mc_error_contribution.GetNbinsX()+1)
  unityLine = ROOT.TLine(x1, 1, x2, 1)


  pad_ratio.cd()
  mc_error_contribution.Draw("E2 L")
  unityLine.Draw("SAME")
  ratio.Draw("E SAME")

  can.Update()
  can.SaveAs(plot_key + "_" + config["mcCampaign"] + ".pdf")
  can.Close()


def produce_SF(config, datasets, plot_key, region, trigger, year, topology, qual, iso, other):
  """
  produce a plot and save to disk
  :param config: configuration object built by read_config()
  :param datasets: dataset list produced by get_datasets()
  :param plot_key: string to select plot from config, can be produced via select_plot()
  """
  title = config["Plots"][plot_key]["Title"] # x axis title
  mc_categories_for_plot = config["MCStackOrder"]

  # dict containing all information needed for plots ("plot category dict" = pcd)
  pcd = {}

  # process all categories defined in the config, produce summed histograms
  # and write all important information to pcd object
  for cat in mc_categories_for_plot + ['data']:
    isData = (cat == 'data')
    cat_title = config["CategoryDefinitions"][cat]["Title"]
    cat_color = config["CategoryDefinitions"][cat]["Color"]
    tag_matched = [d.tag for d in datasets if d.category == cat]
    probe_matched = [d.probe for d in datasets if d.category == cat]

    print '# processing', cat, ': found', len(tag_matched), 'histogram(s)'
    if len(tag_matched)>0:
      pcd[cat] = histogramCollection(cat, sum_histograms(tag_matched, cat), sum_histograms(probe_matched, cat), cat_title, isData)
      pcd[cat].setStyle(eval(cat_color)) # color needs to be translated via eval()
    else:
      pcd[cat] = None
      pass # could add an exception here to deal with categories for which no histograms exist

  # create reference to data histogram for plotting
  if pcd['data'] == None:
    print "# no data histogram found, aborting"
    return -1
  data_hist = pcd['data'].ratio
  print pcd['data'].ratio

  # get required asym hists
  nBins = data_hist.GetSize()-2
  print nBins
  hist_eff_data = ROOT.TGraphAsymmErrors(nBins)
  hist_eff_data.Divide(pcd['data'].probe, pcd['data'].tag)
  hist_eff_mc   = ROOT.TGraphAsymmErrors(nBins)
  hist_eff_mc.Divide(pcd['allMC'].probe, pcd['allMC'].tag)

  data_hist.SetLineColor(ROOT.kGray+3)
  data_hist.SetFillColorAlpha(ROOT.kGray+2, 0.5)

  # clone and style full stack of MC, needed to plot uncertainties
  mc_stack_sum = pcd['allMC'].ratio
  mc_stack_sum.SetFillColor(ROOT.kGreen+1)
  mc_stack_sum.SetLineColor(ROOT.kGreen+2)
  mc_stack_sum.SetFillStyle(3354)
  mc_stack_sum.GetYaxis().SetTitleSize(0.045)
  mc_stack_sum.GetYaxis().SetTitle("Efficiency")

  # open file to save to
  out_name = trigger + "." + region + "." + year + "." + topology + "." + qual + "." + iso + "." + other + ".root"
  print out_name
  outFile = ROOT.TFile("output_tmp/" + out_name,"recreate")

  can = ROOT.TCanvas("c1", "canvas_1", 1200, 1200)
  width_bottom = 0.4
  offset = 0.02
  pad_main = ROOT.TPad("pad_main", "", 0, width_bottom-offset, 1, 1)
  pad_main.SetFillColor(0)
  pad_main.SetFillStyle(0)
  pad_main.SetBottomMargin(0.02)
  pad_main.SetLeftMargin(0.12)
  #pad_main.SetGridx()
  pad_main.Draw()
  pad_ratio = ROOT.TPad("pad_ratio", "", 0, 0.05, 1, width_bottom*(1-offset)) # 0.294)
  pad_ratio.SetFillColor(0)
  pad_ratio.SetFillStyle(0)
  pad_ratio.SetTopMargin(0.0)
  pad_ratio.SetLeftMargin(0.12)
  pad_ratio.SetBottomMargin(0.25) # needs to be large enough to not cut off title
  #pad_ratio.SetGridx()
  pad_ratio.Draw()

  pad_main.cd()

  # create legend
  leg = ROOT.TLegend(0.68,0.1,0.89,0.27)
  leg.SetBorderSize(0)
  leg.AddEntry(data_hist, 'Data', 'lep')
  for cat in mc_categories_for_plot:
    if pcd[cat] != None: # verify that a histogram exist
      leg.AddEntry(mc_stack_sum, 'Simulation', 'leF')

  mc_stack_sum.Draw("E2")      # draw error for MC stack
  mc_stack_sum.Draw("SAME E")      # draw error for MC stack
  data_hist.Draw("SAME E")          # draw data
  leg.Draw("SAME")                  # draw legend

  ROOT.ATLASLabel(0.15+0.02, 0.46, "work in progress")

  mc_stack_sum.GetXaxis().SetLabelSize(0)

  # luminosity label
  LumiLabel = ROOT.TLatex()
  LumiLabel.SetTextFont(42)
  LumiLabel.SetTextSize(0.042)
  LumiLabel.SetNDC()
  if topology == "ttbar": top_string = "t#bar{t} selection"
  elif topology == "wjets": top_string = "W+jets selection"
  else: topology = ""
  LumiLabel.DrawLatex(0.15+0.02, 0.40, "#sqrt{s} = 13 TeV, " + config["LumiLabel"])
  LumiLabel.DrawLatex(0.15+0.02, 0.35, top_string)

  # custom extra label
  customLabel = ROOT.TLatex()
  customLabel.SetTextFont(42)
  customLabel.SetTextSize(0.042)
  customLabel.SetNDC()
  #customLabel.DrawLatex(0.15+0.02, 0.10, trigger + ", " + region)
  if region == "barrel": region_str = "Barrel"
  if region == "endcaps": region_str = "End-caps"
  customLabel.DrawLatex(0.15+0.02, 0.30, region_str)

  # ##########################################################################
  # do the data/MC ratio next
  # ##########################################################################

  # calculate ratio, using both data and MC unc, not yet asymmetric!!

  # get non-asym ratio
  ratio = data_hist.Clone()
  ratio.Divide(mc_stack_sum.Clone())

  # Calculate proper errors for SF histogram
  xval_data = ROOT.Double()
  yval_data = ROOT.Double()
  xval_mc   = ROOT.Double()
  yval_mc   = ROOT.Double()

  for binNumber in range(1, ratio.GetNbinsX() + 1):
    hist_eff_data.GetPoint(binNumber-1, xval_data, yval_data)
    hist_eff_mc.GetPoint(binNumber-1, xval_mc, yval_mc)
    if ratio.GetBinContent(binNumber) == 0:
      print '--- !!! WARNING: empty bin in SF calculation, will most likely cause issues - consider rebinning around', xval_mc, xval_data, 'GeV'
    try:
      asym_err = ratio.GetBinContent(binNumber) * ( (hist_eff_data.GetErrorY(binNumber-1) / yval_data)**2\
                                                      + (hist_eff_mc.GetErrorY(binNumber-1) / yval_mc)**2 )**0.5
    except:
      asym_err = 0.0
      print "ERR SET TO 0!!!"
    ratio.SetBinError(binNumber, asym_err)

  # save efficiency histograms and SF
  hist_eff_data.Write(out_name + "." + "eff_data")
  hist_eff_mc.Write(out_name + "." + "eff_mc")
  ratio.Write(out_name + "." + "SF")

  # style settings
  ratio_plot_range = 0.25
  ratio.SetMinimum(1-ratio_plot_range)
  ratio.SetMaximum(1+0.15)
  ratio.SetMarkerStyle(ROOT.kFullCircle)
  ratio.SetMarkerSize(0)
  ratio.GetXaxis().SetTitle(title)
  ratio.GetXaxis().SetTitleSize(0.1)
  ratio.GetXaxis().SetTitleOffset(1.1)
  ratio.GetXaxis().SetLabelSize(0.065)
  ratio.GetYaxis().SetLabelSize(0.065)
  ratio.GetYaxis().SetNdivisions(505)
  ratio.GetYaxis().SetTitle("Data eff. / Simul. eff.")
  ratio.GetYaxis().SetTitleSize(0.08)
  ratio.GetYaxis().SetTitleOffset(0.6)
  ratio.SetFillColorAlpha(ROOT.kBlue, 0.35)
  ratio.SetLineColorAlpha(ROOT.kBlue, 0.5)
  #ratio.SetFillStyle(3044) #3244

  # create line through unity
  x1 = ratio.GetXaxis().GetBinLowEdge(1)
  x2 = ratio.GetXaxis().GetBinCenter(ratio.GetNbinsX())
  x2 += 0.5*ratio.GetBinWidth(ratio.GetNbinsX()+1)
  unityLine = ROOT.TLine(x1, 1, x2, 1)
  unityLine.SetLineStyle(2)

  # fit the SF
  pad_ratio.cd()
  Fit_SF = ROOT.TF1('pol0','[0]', 100, 600) # fit 100 - 600 GeV
  Fit_SF.SetParameter(0,1)
  Fit_SF.SetLineColor(ROOT.kRed+1)
  Fit_SF.SetLineWidth(0)

  ratio.Fit(Fit_SF, 'R Q') # order 0

  Fit_P0_SF     = Fit_SF.GetParameter(0)
  Fit_P0_Err_SF = Fit_SF.GetParError(0)

  print "SF:", Fit_P0_SF, Fit_P0_Err_SF

  bestFitHi = ROOT.TLine(100, Fit_P0_SF+Fit_P0_Err_SF, x2, Fit_P0_SF+Fit_P0_Err_SF) # start from 100 GeV
  bestFitLo = ROOT.TLine(100, Fit_P0_SF-Fit_P0_Err_SF, x2, Fit_P0_SF-Fit_P0_Err_SF)
  bestFitHi.SetLineColor(ROOT.kRed)
  bestFitLo.SetLineColor(ROOT.kRed)
  bestFitHi.SetLineWidth(3)
  bestFitLo.SetLineWidth(3)
  bestFitHi.SetLineStyle(2)
  bestFitLo.SetLineStyle(2)

  pad_ratio.cd()
  ratio.Draw("E2")
  unityLine.Draw("SAME")
  #bestFitHi.Draw("SAME")
  #bestFitLo.Draw("SAME")
  ratio.Draw("E SAME")

  # save SF fit
  Fit_SF.Write(out_name + "." + "SF_fit")

  pad_main.cd()

  Fit_da = ROOT.TF1('pol0','[0]', 100, 600) # fit 100 - 600 GeV
  Fit_da.SetParameter(0,1)
  Fit_da.SetLineColor(ROOT.kRed+1)
  Fit_da.SetLineWidth(3)
  hist_eff_data.Fit(Fit_da, 'R Q') # order 0
  Fit_P0_da     = Fit_da.GetParameter(0)
  Fit_P0_Err_da = Fit_da.GetParError(0)

  Fit_mc = ROOT.TF1('pol0','[0]', 100, 600) # fit 100 - 600 GeV
  Fit_mc.SetParameter(0,1)
  Fit_mc.SetLineColor(ROOT.kRed+1)
  Fit_mc.SetLineWidth(3)
  hist_eff_mc.Fit(Fit_mc, 'R Q') # order 0
  Fit_P0_mc     = Fit_mc.GetParameter(0)
  Fit_P0_Err_mc = Fit_mc.GetParError(0)

  print "data eff", Fit_P0_da, Fit_P0_Err_da
  print "MC eff", Fit_P0_mc, Fit_P0_Err_mc

  SFLabel = ROOT.TLatex()
  SFLabel.SetTextFont(42)
  SFLabel.SetTextSize(0.042)
  SFLabel.SetNDC()
  SFLabel.DrawLatex(0.15+0.02, 0.10, "SF:")
  SFLabel.DrawLatex(0.27+0.02, 0.10, "{0:.2f}".format(Fit_P0_SF*100) + "% #pm " + "{0:.2f}".format(Fit_P0_Err_SF*100) + "%")

  mcLabel = ROOT.TLatex()
  mcLabel.SetTextFont(42)
  mcLabel.SetTextSize(0.042)
  mcLabel.SetNDC()
  mcLabel.DrawLatex(0.15+0.02, 0.15, "Simul. eff.:")
  mcLabel.DrawLatex(0.27+0.02, 0.15, "{0:.2f}".format(Fit_P0_mc*100) + "% #pm " + "{0:.2f}".format(Fit_P0_Err_mc*100) + "%")

  daLabel = ROOT.TLatex()
  daLabel.SetTextFont(42)
  daLabel.SetTextSize(0.042)
  daLabel.SetNDC()
  daLabel.DrawLatex(0.15+0.02, 0.20, "Data eff.:")
  daLabel.DrawLatex(0.27+0.02, 0.20, "{0:.2f}".format(Fit_P0_da*100) + "% #pm " + "{0:.2f}".format(Fit_P0_Err_da*100) + "%")

  # save efficiencies
  Fit_da.Write(out_name + "." + "eff_data_fit")
  Fit_mc.Write(out_name + "." + "eff_mc_fit")

  can.Update()
  if other=="nominal" and iso=="no_iso" and qual=="medium_wp":
    can.SaveAs(plot_key+"_"+trigger+"_"+region+"_" + year + "_" + topology + ".pdf")
  can.Close()

  outFile.Close()


if __name__ == '__main__':
  parser = argparse.ArgumentParser(description="trigger efficiencies")
  parser.add_argument("config_path", type=str, help="path to config")
  parser.add_argument("topology", type=str, help="topology: ttbar or wjets")
  parser.add_argument("qual", type=str, help="quality: medium_wp or highpt_wp")
  parser.add_argument("iso", type=str, help="isolation: no_iso, FCTight, FCTTO")
  parser.add_argument("other", type=str, help="other flags: nominal, btag_var, met_var, jetpt_var")
  parser.add_argument("region", type=str, help="region: barrel or endcaps")
  parser.add_argument("trigger", type=str, help="trigger: HLT_mu26_ivarmedium, HLT_mu50, or OR for both")
  args = parser.parse_args()
  print args.topology
  config_path = args.config_path #sys.argv[-1]

  #year = sys.argv[-1].split('_')[-1].split('.')[0]
  year = args.config_path.split('_')[-1].split('.')[0]
  print year

  # CONFIG STUFF HERE
  topology = args.topology # ttbar, wjets
  qual = args.qual # medium_wp, highpt_wp
  iso = args.iso # no_iso, FCTight, FCTTO
  other = args.other # nominal, btag_var, met_var, jetpt_var

  extra_cuts = "(tag_" + topology + "_" + year + ")"

  if other != "met_var":
    extra_cuts += "*(met_met/1000.0>200)" # default MET cut
  else:
    extra_cuts += "*(met_met/1000.0>150)" # met variation

  if other == "jetpt_var":
    extra_cuts += "*(Sum$(jet_pt<=30000) == 0)" # jet pT variation

  if other == "btag_var":
    if topology == "ttbar":
      extra_cuts += "*(Sum$(jet_isbtagged_MV2c10_70)>=1)"
    elif topology == "wjets":
      #extra_cuts += "*(Sum$(jet_isbtagged_MV2c10_70)<1)"
      extra_cuts += "*(Sum$(jet_isbtagged_MV2c10_85)<1)"

  print extra_cuts

  # where to find datasets
  if qual == "medium_wp" and iso == "no_iso":
    sub_path = "FILES_med_noIso"
  elif qual == "medium_wp" and iso == "FCTTO":
    sub_path = "FILES_med_FCTTO"
  elif qual == "medium_wp" and iso == "FCTight":
    sub_path = "FILES_med_FCTight"
  elif qual == "highpt_wp" and iso == "no_iso":
    sub_path = "FILES_HPt_noIso"
  elif qual == "highpt_wp" and iso == "FCTTO":
    sub_path = "FILES_HPt_FCTTO"
  elif qual == "highpt_wp" and iso == "FCTight":
    sub_path = "FILES_HPt_FCTight"
  else:
    print "unknown setting"
    raise SystemExit

  # read config
  config   =  read_config(config_path)

  # get list of datasets to be used when plotting
  datasets = get_datasets(config, sub_path)

  # select which plot to do
  #plot_key = select_plot(config)
  plot_key = 'mu_pt'

  #regions = ["barrel", "endcaps"]
  #for ir, r in enumerate(regions):
  #  print ir, r
  #region_selection = int(raw_input("# select region: "))
  #region = regions[region_selection]
  region = args.region

  #triggers = ["HLT_mu26_ivarmedium", "HLT_mu50"]
  #for it, t in enumerate(triggers):
  #  print it, t
  #trigger_selection = int(raw_input("select trigger: "))
  #trigger = triggers[trigger_selection]
  trigger = args.trigger

  # generate all histograms needed (one per dataset)
  produce_all_histograms(config, datasets, plot_key, region, trigger, year, extra_cuts)

  # produce the plot
  #produce_plot(config, datasets, plot_key)
  produce_SF(config, datasets, plot_key, region, trigger, year, topology, qual, iso, other)
