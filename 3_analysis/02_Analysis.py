import ROOT
import glob
from array import array
import json
import os
import sys

ROOT.gROOT.SetBatch(ROOT.kTRUE) # non-interactive
ROOT.TH1.SetDefaultSumw2()      # for error propagation
ROOT.gStyle.SetOptStat(0)       # turn off box with histogram info
#ROOT.TH1.AddDirectory(ROOT.kFALSE) # do not remove histograms when files are closed

ROOT.gROOT.LoadMacro("atlasstyle/AtlasStyle.C")
ROOT.gROOT.LoadMacro("atlasstyle/AtlasLabels.C")


##############################################################################
#
# config handling
#
##############################################################################

def read_config(config_path):
  """
  read external json config
  :param config_path: path to json file containing config
  :returns: dict containing all config information
  """
  if config_path == __file__:
    print '# execute via \"python', __file__, 'path/to/config.json\"'
    raise SystemExit

  try:
    with open(config_path) as f:
      text_without_comments = ""
      for line in f.readlines():
        if line.lstrip("\t ")[0:2] == "//":
          continue # skip comments
        else:
          text_without_comments += line

      config = json.loads(text_without_comments) # check e.g. http://jsonparseronline.com/ if config is invalid
    return config

  except Exception as e:
    print "# unable to read config file:\n", e
    print "# line count is wrong if comments are used in config"
    raise SystemExit


##############################################################################
#
# class definitions
#
##############################################################################

class dataset():
  """
  class representing a dateset (data or MC root file)
  and information contained therein
  """
  def __init__(self, s):
    """
    :param s: string containing path to root file
    """
    self.path      = s
    self.treename  = 'nominal'                         # currently hardcoded
    self.dsid      = s.split('/')[-2].split('.')[2]
    self.category  = None
    self.yieldHist = None                              # needs to be set via setYieldHist()
    self.isData    = 'physics_Main' in s.split('/')[-2] or 'AllYear' in s.split('/')[-2]

  def __repr__(self):
    return (' ').join([self.category, self.dsid])

  def getCategory(self, config):
    """
    :param config: configuration object built by read_config()
    """
    if self.isData:
      self.category = 'data'
    else:
      matches = []
      for cat in config["MCStackOrder"]:
        if int(self.dsid) in config["CategoryDefinitions"][cat]["DSIDs"]:
          #print 'identified', self.dsid, 'as', cat
          matches.append(cat)
      if len(matches)!=1:
        print self.dsid, "not uniquely matched to any category in MCStackOrder, will ignore sample"
        return -1
      else:
        self.category = matches[0]

  def setYieldHist(self, variable, cut, bins):
    """
    need to think whether this should become part of the constructor
    might be useful to have standalone if many variables are plotted
    :param variable: variable to bin in
    :param cut: cut applied to histogram
    :param bins: binning used
    """
    # create a template
    bin_array = array('d', bins)
    template = ROOT.TH1D('template', '', len(bins)-1, bin_array)

    # define internal name of histogram, can the be found via ROOT.gROOT.FindObject(hist_name)
    hist_name = "hist_" + self.category + "_" + self.dsid

    can = ROOT.TCanvas("tmp_can", "temporary canvas", 800, 800)
    f   = ROOT.TFile(self.path,"READ")
    t   = f.Get(self.treename)
    self.yieldHist = template.Clone(hist_name)     # create histogram from template with proper binning
    t.Draw(variable + ">>" + hist_name, cut)       # draw to histogram
    self.yieldHist.SetDirectory(0)                 # keep alive after file is closed
    f.Close()
    can.Close()


class histogramCollection():
  """
  class representing summed histograms
  contains styling information to modify in bulk more easily
  """
  def __init__(self, cat, hist, title, isData):
    """
    :param cat: category, e.g. tt, W+jets, data, ...
    :param hist: ROOT histogram
    :param title: title string to use for legend
    :param isData: bool to flag data
    """
    self.category = cat
    self.hist     = hist
    self.nevts    = hist.Integral()
    self.title    = title
    self.isData   = isData

  def __repr__(self):
    return (' ').join([self.title, str(self.hist)])

  def setStyle(self, color=ROOT.kBlack):
    """
    :param color: color to use for plot
    """
    self.hist.SetMarkerStyle(ROOT.kFullCircle)
    self.hist.SetLineWidth(2)
    self.hist.SetMaximum(1.5*self.hist.GetMaximum())

    if self.isData:
      self.hist.SetLineColor(color)
      self.hist.SetMarkerSize(2)

    else:
      self.hist.SetLineColor(color)
      self.hist.SetFillColor(color)
      self.hist.SetMarkerSize(0)
      self.hist.SetLineWidth(2)


##############################################################################
#
# utility functions
#
##############################################################################

def get_datasets(config):
  """
  return all relevant datasets, using info in the config
  optional setting to match only a specific category
  :param config: configuration object built by read_config()
  :returns: a list of dataset instances
  """
  dataset_list = []
  # search recursively through folder containing downloaded files
  search_path = config["DownloadFolder"]
  print '# searching for files with name', config["OutputFilename"], 'in', search_path
  for x in os.walk(search_path):
    # identify merged samples (these will be used)
    if config["OutputFilename"] in x[2]:
      new_dataset = dataset(x[0]+'/'+config["OutputFilename"])

      if config["mcCampaign"] not in x[0] and config["dataIdentifier"] not in x[0]:
        continue # skip mc16a or mc16c
      elif new_dataset.getCategory(config) == -1:
        continue # ignore dataset, not matched via config
      #elif "2017" in x[0] or "2018" in x[0]: continue # FOR DEBUGGING
      else:
        print '# found', new_dataset.category, 'sample', new_dataset.dsid,'in', x[0]
        dataset_list.append(new_dataset)
  return dataset_list


def select_plot(config):
  """
  let the user select one plot from the options in the config
  :param config: configuration object built by read_config()
  :returns: the key of the chosen plot
  """
  print '\n# select plot to produce:'
  plots_available = config["Plots"].keys()  # find all possible plots defined in config
  plots_available.sort()
  for i, p in enumerate(plots_available):
    print i, '-', config["Plots"][p]["Title"]

  plot_choice = '-1'
  while plot_choice not in [str(x) for x in range(len(plots_available))]:
    plot_choice = raw_input("select number of chosen plot: ")

  return plots_available[int(plot_choice)]


def sum_histograms(h_list, sum_name):
  """
  sum together a list of histograms
  :param h_list: list of histograms, usually TH1Ds
  :param sum_name: internal name of histogram containing sum
  :returns: return summed histogram
  """
  # sum histograms together if list contains multiple entries
  if isinstance(h_list,list):
    hist_sum = h_list[0].Clone(sum_name)
    for h in h_list[1:]:
      hist_sum.Add(h)
  else:
    hist_sum = h_list.Clone(sum_name)
  return hist_sum


def produce_all_histograms(config, datasets, plot_key):
  """
  calls setYieldHist() for all datasets in a given list
  currently only one hist per dataset is supported
  :param config: configuration object built by read_config()
  :param datasets: list of datasets
  :param plot_key: string to select plot from config, can be produced via select_plot()
  """
  cut_data = config["DefaultCuts"]
  cut_mc   = config["DefaultCuts"] + '*' + config["Weights"] + '*' + str(config["Lumi"])
  variable = config["Plots"][plot_key]["Variable"]
  bins     = config["Plots"][plot_key]["Bins"]

  for d in datasets:
    if d.isData:
      d.setYieldHist(variable, cut_data, bins)
    else:
      d.setYieldHist(variable, cut_mc, bins)


def produce_plot(config, datasets, plot_key):
  """
  produce a plot and save to disk
  :param config: configuration object built by read_config()
  :param datasets: dataset list produced by get_datasets()
  :param plot_key: string to select plot from config, can be produced via select_plot()
  """
  title = config["Plots"][plot_key]["Title"] # x axis title
  mc_categories_for_plot = config["MCStackOrder"]

  # dict containing all information needed for plots ("plot category dict" = pcd)
  pcd = {}

  # process all categories defined in the config, produce summed histograms
  # and write all important information to pcd object
  for cat in mc_categories_for_plot + ['data']:
    isData = (cat == 'data')
    cat_title = config["CategoryDefinitions"][cat]["Title"]
    cat_color = config["CategoryDefinitions"][cat]["Color"]
    hists_matched = [d.yieldHist for d in datasets if d.category == cat]

    print '# processing', cat, ': found', len(hists_matched), 'histogram(s)'
    if len(hists_matched)>0:
      pcd[cat] = histogramCollection(cat, sum_histograms(hists_matched, cat), cat_title, isData)
      pcd[cat].setStyle(eval(cat_color)) # color needs to be translated via eval()
    else:
      pcd[cat] = None
      pass # could add an exception here to deal with categories for which no histograms exist

  # create reference to data histogram for plotting
  if pcd['data'] == None:
    print "# no data histogram found, aborting"
    return -1
  data_hist = pcd['data'].hist

  # build a stack of all MC
  mc_stack = ROOT.THStack(title + '_stack', ';;Events')
  for cat in reversed(mc_categories_for_plot):
    if pcd[cat] != None: # verify that a histogram exist
      mc_stack.Add(pcd[cat].hist)

  # clone and style full stack of MC, needed to plot uncertainties
  mc_stack_sum = mc_stack.GetStack().Last().Clone(title + "_mc_stack_sum")
  mc_stack_sum.SetFillColor(ROOT.kGray+2)
  mc_stack_sum.SetLineColor(ROOT.kGray+2)
  mc_stack_sum.SetFillStyle(3354)

  can = ROOT.TCanvas("c1", "canvas_1", 1200, 1200)
  pad_main = ROOT.TPad("pad_main", "", 0, 0.3-0.02, 1, 1)
  pad_main.SetFillColor(0)
  pad_main.SetFillStyle(0)
  pad_main.SetBottomMargin(0.02)#5)
  pad_main.SetLeftMargin(0.12)
  #pad_main.SetGridx()
  #pad_main.Draw()
  pad_ratio = ROOT.TPad("pad_ratio", "", 0, 0.05, 1, 0.294)
  pad_ratio.SetFillColor(0)
  pad_ratio.SetFillStyle(0)
  pad_ratio.SetTopMargin(0.0)
  pad_ratio.SetBottomMargin(0.30) # needs to be large enough to not cut off title
  #pad_ratio.SetGridx()
  pad_ratio.SetLeftMargin(0.12)
  pad_ratio.Draw()
  pad_main.Draw()

  pad_main.cd()

  # create legend
  leg = ROOT.TLegend(0.7,0.52,0.88,0.85)
  leg.SetBorderSize(0)
  #leg.AddEntry(data_hist, 'data: ' + str(round(pcd['data'].nevts,1)), 'lep')
  leg.AddEntry(data_hist, 'Data', 'lep')
  for cat in mc_categories_for_plot:
    if pcd[cat] != None: # verify that a histogram exist
      #leg.AddEntry(pcd[cat].hist, pcd[cat].title + ': ' + str(round(pcd[cat].nevts,1)), 'F')
      leg.AddEntry(pcd[cat].hist, pcd[cat].title, 'F')

  # set style options for plot
  mc_stack.SetMinimum(0)
  y_max = max(data_hist.GetMaximum(), mc_stack.GetMaximum())
  mc_stack.SetMaximum(config["PlotYMaxFactor"]*y_max)
  mc_stack_sum.SetMaximum(config["PlotYMaxFactor"]*y_max)
  print "setting", config["PlotYMaxFactor"]*y_max
  print "setting", y_max

  mc_stack_sum.Draw("AXIS")         # draw y axis label
  mc_stack.Draw("HIST")             # draw MC stack of histograms
  mc_stack_sum.Draw("SAME E2")      # draw error for MC stack
  data_hist.Draw("SAME E")          # draw data
  leg.Draw("SAME")                  # draw legend

  # remove x-axis label from main plot
  #mc_stack_sum.GetXaxis().SetLabelOffset(999)
  mc_stack_sum.GetXaxis().SetLabelSize(0)
  #mc_stack.GetXaxis().SetLabelOffset(999)
  mc_stack.GetXaxis().SetLabelSize(0)

  ROOT.ATLASLabel(0.17, 0.80, "work in progress")

  # luminosity label
  LumiLabel = ROOT.TLatex()
  LumiLabel.SetTextFont(42)
  LumiLabel.SetTextSize(0.04)
  LumiLabel.SetNDC()
  LumiLabel.DrawLatex(0.17, 0.74, config["LumiLabel"])

  # custom extra label
  customLabel = ROOT.TLatex()
  customLabel.SetTextFont(42)
  customLabel.SetTextSize(0.04)
  customLabel.SetNDC()
  customLabel.DrawLatex(0.17, 0.68,config["customStringForPlot"])

  # do the data/MC ratio next

  # create a copy of the summed mc contribution with errors set to 0
  # in order to propagate only the data stat unc. to the ratio
  mc_stack_sum_no_errors = mc_stack_sum.Clone()
  for ibin in range(mc_stack_sum_no_errors.GetNbinsX()+2):
    mc_stack_sum_no_errors.SetBinError(ibin, 0)

  # create another copy of the summed mc contribution, set the
  # bin contents to 1, keep the relative uncertainty
  # this then contains the unc. contribution to the ratio from MC
  mc_error_contribution = mc_stack_sum.Clone()
  for ibin in range(mc_error_contribution.GetNbinsX()+2):
    if mc_error_contribution.GetBinContent(ibin) != 0:
      rel_unc = mc_error_contribution.GetBinError(ibin) / float(mc_error_contribution.GetBinContent(ibin))
    else:
      rel_unc = 0 # for empty bins
    mc_error_contribution.SetBinContent(ibin, 1)
    mc_error_contribution.SetBinError(ibin, rel_unc)

  # calculate ratio, using only the data uncertainties
  ratio = data_hist.Clone()
  ratio.Divide(mc_stack_sum_no_errors)

  # style settings
  ratio_plot_range = config["ratioPlotRange"]
  mc_error_contribution.SetMinimum(1-ratio_plot_range)
  mc_error_contribution.SetMaximum(1+ratio_plot_range)
  mc_error_contribution.SetMarkerStyle(ROOT.kFullCircle)
  mc_error_contribution.GetXaxis().SetTitle(title)
  mc_error_contribution.GetXaxis().SetTitleSize(0.12)
  mc_error_contribution.GetXaxis().SetTitleOffset(1.1)
  mc_error_contribution.GetXaxis().SetLabelSize(0.1)
  mc_error_contribution.GetYaxis().SetLabelSize(0.1)
  mc_error_contribution.GetYaxis().SetNdivisions(505)
  mc_error_contribution.GetYaxis().SetTitle("Data / Pred.")
  mc_error_contribution.GetYaxis().SetTitleSize(0.1)
  mc_error_contribution.GetYaxis().SetTitleOffset(0.4)

  # create line through unity
  x1 = mc_error_contribution.GetXaxis().GetBinLowEdge(1)
  x2 = mc_error_contribution.GetXaxis().GetBinCenter(mc_error_contribution.GetNbinsX())
  x2 += 0.5*mc_error_contribution.GetBinWidth(mc_error_contribution.GetNbinsX()+1)
  unityLine = ROOT.TLine(x1, 1, x2, 1)

  pad_ratio.cd()
  mc_error_contribution.Draw("E2 L")
  unityLine.Draw("SAME")
  ratio.Draw("E SAME")

  can.Update()
  extra_string_for_topology = ""
  if "bar" in config["customStringForPlot"]:
    extra_string_for_topology = "ttbar"
  elif "jets" in config["customStringForPlot"]:
    extra_string_for_topology = "wjets"
  can.SaveAs(plot_key + "_" + config["mcCampaign"] + extra_string_for_topology + ".pdf")
  can.Close()


if __name__ == '__main__':
  config_path = sys.argv[-1]

  # read config
  config   =  read_config(config_path)

  # get list of datasets to be used when plotting
  datasets = get_datasets(config)

  # select which plot to do
  plot_key = select_plot(config)

  # generate all histograms needed (one per dataset)
  produce_all_histograms(config, datasets, plot_key)

  # produce the plot
  produce_plot(config, datasets, plot_key)
