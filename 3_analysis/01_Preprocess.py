import sys
import math
from array import array
import glob
import codecs
import shlex
import os
import ROOT


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# options
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
file_type          = '*'                                 # by default, this is '*' to process all categories
# taken xsec_file from https://atlas-groupdata.web.cern.ch/atlas-groupdata/dev/AnalysisTop/TopDataPreparation/
xsec_file          = 'XSection-MC15-13TeV_20190319.data' # update with most recent top data prep file
download_directory = '../2_download/Files'
output_directory   = '../2_download/FILES_*'             # location where merged samples are located
target_filename    = 'output.root'                       # name of root file to merge to
selection_name     = 'tag_wjets_2016'                    # name of any selection used in cuts file
weight_tree        = 'nominal_norm'                      # name of tree containing normalization branch
mc16a_tag          = "r9364"
mc16d_tag          = "r10201"
mc16e_tag          = "r10724"
stream_tag         = "med_noIso"                          # process each production stream separately
# med_noIso, med_FCTight, med_FCTTO, HPt_noIso, HPt_FCTight, HPt_FCTTO
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# end of options
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


def merge_files():
  categories = glob.glob(download_directory+'/*/*root*')
  categories.sort()

  # remove everything not belonging to stream, and remove data
  categories = [c for c in categories if (stream_tag in c) and ("AllYear" not in c)]

  # important: need to first find all cases where multiple datasets exist per cpmpaign and dsid
  dsids = [cat.split('/')[-1].split('.')[2] for cat in categories]
  dsids = list(set(dsids)) # exactly one entry per unique dsid
  dsids.sort()

  #dsids.append("data_2015")
  #dsids.append("data_2016")
  #dsids.append("data_2017")

  for i, dsid in enumerate(dsids):
    #if i%10 == 0: raw_input("continue?")
    matches = [cat for cat in categories if dsid in cat]
    mc16a_part = [m for m in matches if mc16a_tag in m]
    mc16d_part = [m for m in matches if mc16d_tag in m]
    mc16e_part = [m for m in matches if mc16e_tag in m]
    rest       = [m for m in matches if ((m not in mc16a_part) and (m not in mc16d_part) and (m not in mc16e_part))]

    if len([r for r in rest if 'data' not in r])>0:
      print "!!! unexpected behavior, files in rest category not data", rest

    print "# processing:", dsid

    # for each dsid, process all campaigns sequentially
    for hits, campaign in zip([mc16a_part, mc16d_part, mc16e_part, rest], [mc16a_tag, mc16d_tag, mc16e_tag, "rest"]):
      stacked_list_files = [glob.glob(h + "/*root*") for h in hits]
      stacked_list_files.sort()
      files_to_merge = [i for sublist in stacked_list_files for i in sublist]

      if len(hits)==0:   # usually means was not processed in campaign
        continue
        raw_input("no hits, enter to continue")
      elif len(hits)>1:
        print "   # INFO: multiple jobs need to be merged together, this should not happen any more, aborting"
        raise SystemExit

      stream_target_path = ('/').join(hits[0].split('/')[0:-3] + ["FILES_" + stream_tag] + hits[0].split('/')[-2:-1])
      target_directory = stream_target_path + "/user.alheld." + dsid + ".MERGED." + campaign
      print target_directory

      if not os.path.exists(target_directory):
        os.makedirs(target_directory) # create target directory if it does not exist

      if len(files_to_merge) == 0:
        print "   # no files found - either missing, or no events passed the selection"

      else: # files found that can be merged
        files_to_merge_string = (" ").join(files_to_merge)
        if not os.path.isfile(target_directory + '/' + target_filename):
          merge_command = 'hadd ' + target_directory + '/' + target_filename + ' ' + files_to_merge_string
          #print merge_command
          os.system(merge_command)
        else:
          print "   # already merged"


def GetXS(sampleNumber):
  sampleNumber = str(sampleNumber)
  XSFile = codecs.open(xsec_file, 'r').read().splitlines()
  for line in XSFile:
    if len(line) == 0 or not line[0].isdigit():
      continue

    buff = shlex.split(line, '\t')
    if str(buff[0]) == sampleNumber:
      return float(buff[1]), float(buff[2]), 1.0
  return -1


def getN_fromcutflow(root_file):
  # this method does not work with skimmed samples!
  cutflowHist = root_file.Get(selection_name + '/cutflow_mc_pu')
  N = cutflowHist.GetBinContent(1)
  return N


def getN(root_file, cur_dsid):
  # recommended way of getting normalization
  t = root_file.Get("sumWeights")
  sumWeightedEvts = 0
  for i in range(t.GetEntries()):
    t.GetEntry(i)
    if t.dsid == int(cur_dsid):
      sumWeightedEvts += t.totalEventsWeighted
    else:
      print 'weird behavior, wrong dsid found in tree'
      print 'aborting, check this manually'
      raise SystemExit
  return sumWeightedEvts


def addFriends(file_list):
  for i, cur_file in enumerate(file_list):
    cur_dsid = cur_file.split('/')[-2].split('.')[2]
    print ' # processing', cur_dsid

    in_file = ROOT.TFile.Open(cur_file, "update")

    # continue and add tree only if it does not exist yet
    if in_file.GetListOfKeys().Contains(weight_tree):
      print '   # tree already exists'

    else:
      print '   # adding friend tree'

      # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      # Get normalization factors
      # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      if ('period' not in cur_dsid) and ('AllYear' not in cur_dsid): # identify mc
        N = getN(in_file, cur_dsid)
        # print '   number of initial events', N
        try:
          XS, kFactor, filterEff = GetXS(cur_dsid)
        except:
          raw_input('   # skipping (not found in x-sec file), hit enter to continue')
          continue
        mc_weight = XS*kFactor*filterEff / float(N)
      else:
        mc_weight = 1 # for data
      print '   # mc weight', mc_weight


      # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      # Add friend trees with normalization weights
      # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


      itree = in_file.Get('nominal')
      otree = ROOT.TTree(weight_tree, 'recreate')

      itree.AddFriend( otree )
      weight_norm = array( 'f', [0] )
      otree.Branch( 'weight_norm', weight_norm, 'weight_norm/F' )

      for ientry in xrange( itree.GetEntries() ):
        #if ientry % 250 == 0:
        #  print 'at entry', ientry

        nb = itree.GetEntry( ientry )
        if nb <= 0:
            print 'Error reading file'
            sys.exit()

        weight_norm[0] = mc_weight
        otree.Fill()

      itree.Write()
      otree.Write()

    in_file.Close()


def get_files_for_weights():
  #mc_campaign = "mc16a"
  all_files = glob.glob(output_directory + '/' + file_type + "/*/" + target_filename)
  #all_files = [f for f in all_files if stream_tag in f]    # match the tag
  #all_files = [f for f in all_files if mc_campaign in f]   # match the MC campaign
  all_files.sort()
  print "# found", len(all_files), "files"
  return all_files

if __name__ == '__main__':
  print "\n ### MERGING FILES ###\n"
  merge_files()

  all_files = get_files_for_weights()
  print "\n ###  ADDING NORMALIZATION WEIGHTS ###\n"
  addFriends(all_files)

  print "Done preprocessing! :)"
  print "!! note that data is NOT merged by default and needs to be handled manually !!"
