#!/usr/bin/env python
import TopExamples.grid

scope          = 'user.srettie'
datasetPattern = '*WPlusJets_WithBJetVeto_output.root'
#datasetPattern = '*WPlusJets_BackgroundZPlusJets_output.root'
#datasetPattern = '*WPlusJets_BackgroundSingleTop_output.root'
#datasetPattern = '*WPlusJets_BackgroundTT_output.root'
#datasetPattern = '*WPlusJets_DataWithBJetVeto_output.root'
#directory      = '/global/srettie/ServiceWork/OutputFiles/'
directory = '/global/srettie/ServiceWork/OutputFiles/Ztest'

TopExamples.grid.download(scope, datasetPattern, directory)
