LibraryNames libTopEventSelectionTools libTopEventReconstructionTools

### FROM Top Group
### Good Run List
GRLDir GoodRunsLists
GRLFile data16_13TeV/20160725/physics_25ns_20.7.xml

### Pile-up reweighting tool - this is now mandatory
### Need to setup the following three options
PRWConfigFiles TopCorrections/PRW.410000.mc15c.r7725_r7676.root
PRWLumiCalcFiles GoodRunsLists/data16_13TeV/20160725/physics_25ns_20.7_TriggerMenu1e34only.lumicalc.OflLumi-13TeV-005.root
PRWDefaultChannel 410000

ElectronCollectionName Electrons
MuonCollectionName Muons
JetCollectionName AntiKt4EMTopoJets
TrackJetCollectionName None
LargeJetCollectionName None 
LargeJetSubstructure None
TauCollectionName None
PhotonCollectionName None

BTaggingWP FixedCutBEff_70 FixedCutBEff_77

TruthCollectionName TruthParticles
TruthLargeRJetCollectionName None 
TruthJetCollectionName AntiKt4TruthJets

### TopPartonHistory only for nominal ttbar
TopPartonHistory False
TopParticleLevel False
TruthBlockInfo False
PDFInfo False 

ObjectSelectionName top::ObjectLoaderStandardCuts
OutputFormat top::EventSaverFlatNtuple
OutputEvents SelectedEvents
OutputFilename out.root
PerfStats None

Systematics Nominal
OutputFileSetAutoFlushZero True

JetUncertainties_BunchSpacing 25ns
JetUncertainties_NPModel 19NP
JetJERSmearingModel Simple

JetEta 2.5
LargeRJetPt 150000
LargeRJetEta 2.0
TrackJetEta 2.5
TrackJetPt 10000

ElectronID TightLH
ElectronIDLoose MediumLH
ElectronIsolation Gradient
ElectronIsolationLoose None
MuonQuality Medium
MuonQualityLoose Medium
MuonIsolation None
MuonIsolationLoose None

FakesControlRegionDoLooseMC False
LooseMETCollectionName MET_Reference_AntiKt4EMTopo
OverlapRemovalLeptonDef Tight
ApplyElectronInJetSubtraction False

#NEvents 1000


############################################

# tag trigger 2016 https://twiki.cern.ch/twiki/bin/view/Atlas/LowestUnprescaled - not listed for all periods!
SELECTION Tag_HLT_xe100_mht_L1XE50_2016
INITIAL
GRL
GOODCALO
PRIVTX
TRIGDEC HLT_xe100_mht_L1XE50
MU_N 15000 >= 1
MU_N 15000 == 1
EL_N 15000 == 0
JETCLEAN LooseBad
JET_N 25000 >= 1
JET_N 25000 <= 4
MV2C10_N 0.645925 < 1
SAVE

############################################

#probe trigger, constant between years
SELECTION Probe_HLT_mu50
INITIAL
GRL
GOODCALO
PRIVTX
TRIGDEC HLT_mu50
MU_N 15000 >= 1
MU_N 15000 == 1
EL_N 15000 == 0
JETCLEAN LooseBad
JET_N 25000 >= 1
JET_N 25000 <= 4
MV2C10_N 0.645925 < 1
TRIGMATCH
#SAVE

#probe trigger, constant between years
SELECTION Probe_HLT_mu60_0eta105_msonly
INITIAL
GRL
GOODCALO
PRIVTX
TRIGDEC HLT_mu60_0eta105_msonly
MU_N 15000 >= 1
MU_N 15000 == 1
EL_N 15000 == 0
JETCLEAN LooseBad
JET_N 25000 >= 1
JET_N 25000 <= 4
MV2C10_N 0.645925 < 1
TRIGMATCH
#SAVE

#lowest unprescaled 2016
SELECTION Probe_HLT_mu26_ivarmedium_2016
INITIAL
GRL
GOODCALO
PRIVTX
TRIGDEC HLT_mu26_ivarmedium
MU_N 15000 >= 1
MU_N 15000 == 1
EL_N 15000 == 0
JETCLEAN LooseBad
JET_N 25000 >= 1
JET_N 25000 <= 4
MV2C10_N 0.645925 < 1
TRIGMATCH
#SAVE

############################################
