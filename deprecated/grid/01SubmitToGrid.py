#!/usr/bin/env python
import TopExamples.grid
#import DerivationTags
import Data
import MC15

def submit(submission_type):
    config = TopExamples.grid.Config()
    config.code          = 'top-xaod'
    config.gridUsername  = 'srettie'
    config.excludedSites = ''
    config.noSubmit      = False
    config.mergeType     = 'Default' #'None', 'Default' or 'xAOD'
    config.destSE        = '' #This is the default (anywhere), or try e.g. 'UKI-SOUTHGRID-BHAM-HEP_LOCALGROUPDISK'
         
    ###############################################################################
    
    config.settingsFile  = 'serviceWorkCuts_2017.txt'
    names = [submission_type]
    config.suffix        = '2.4.25_2017_v0-' + names[0]
    samples = TopExamples.grid.Samples(names)
    TopExamples.grid.submit(config, samples)

    ###############################################################################

if __name__ == '__main__':
    ###submit('data_2015')
    submit('data_2016')
    submit('diboson')
    submit('stop')
    submit('ttbar')
    submit('ttV')
    submit('wjets_newSherpa')
    submit('zjets_newSherpa')
    #submit('resub')
    #submit('testJob')
    print 'Submitted all samples!'
