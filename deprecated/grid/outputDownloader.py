from subprocess import call

import subprocess
import os
import urllib2


dlDir = '/global/srettie/ServiceWork/OutputFiles/Paper2017_2.4.25/'

systematics = [
           '', # Nominal
           #'_Syst_TightWP',
           #'_Syst_NoIso',
]
types = [
    #'data_2015',
    'data_2016',
    'diboson',
    'stop',
    'ttbar',
    'ttV',
    #'wjets',
    'wjets_newSherpa',
    #'zjets',
    'zjets_newSherpa',
    ]


for syst in systematics:
    identifier = '2.4.25_2017'+syst+'_v0-'
    for t in types:
        if 'data' in t and syst != '':
            continue
        print 'Downloading %s %s'%(t,syst)
        if syst == '':
            logName = 'log_'+t+'.out'
        else:
            logName = 'log_'+t+'_'+syst+'.out'
        log = open(dlDir+logName,'w')
        dlCommand = 'nohup rucio download --dir %s --ndownloader 5 user.srettie:user.srettie.*.%s%s_output.root &'%(dlDir, identifier,t)
        print dlCommand
        call(dlCommand, stdout=log, stderr=log, shell=True)
                                 

print 'Submitted all download requests!'

'''
sampleType = 'ttbar'
ATVersion = '2.4.24'
prodVersion = 'v0'
readySamples = []
suffix = '_out.root'
response = urllib2.urlopen('http://bigpanda.cern.ch/tasks/?taskname=user.srettie*'+ATVersion+'*'+prodVersion+'*'+sampleType+'*&superstatus=done')


for line in response.readlines():
           if sampleType in line and ATVersion in line and prodVersion in line and 'taskname' not in line:
                      #print line.split()[2].split('>')[1].split('/')[0] + suffix + ' ',
                      readySamples.append(line.split()[2].split('>')[1].split('/')[0] + suffix)

print '\nThere are %s samples ready to download.'%len(readySamples)

for sample in readySamples:
           if sample in failed:
                      continue
           print 'rucio -T 240 download --ndownloader 5 '+sample
           os.system('rucio download --ndownloader 5 '+sample)
'''
