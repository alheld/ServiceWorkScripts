import ROOT
import sys
import os
import codecs
import shlex
import glob
from array import array

ROOT.gROOT.SetBatch(True)
ROOT.TH1.SetDefaultSumw2()
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetPalette(112)

user = 'srettie.'

debug = True
do2015 = False
do2016 = True # Note that doSys must be true when merging to get proper MUON_ systematics trees included
# DON'T FORGET TO RENAME THE RESUBS BEFORE STARTING TO MERGE!!!
doMerging = False
useNewSherpa = True
skip2D = False
doSys = True
doClosure = False

plotDir = '/home/srettie/ServiceWork/Plots/Paper2017_2.4.25/'
rootFilesDir = '/global/srettie/ServiceWork/OutputFiles/Paper2017_2.4.25/'
mergedFilesDir = rootFilesDir + 'mergedOutput/'

identifier_2015 = '2.4.18_2015*_v*-'
identifier_2016 = '2.4.25_2017*_v*-'

lumi_2015 = 3.21296 # fb^{-1}
lumi_2016 = 32.8616 # fb^{-1}

# 2-D Binning
Eta_Bins_2D_barrel = [-1.05,-0.908,-0.791,-0.652,-0.476,-0.324,-0.132,0.0,+0.132,+0.324,+0.476,+0.652,+0.791,+0.908,+1.05]
nBinsEta_2D_barrel = len(Eta_Bins_2D_barrel) - 1
nBinsPhi_2D_barrel = 8
Eta_Bins_2D_endcap = [-2.5,-2.4,-1.918,-1.623,-1.348,-1.2329,-1.1479,-1.05,+1.05,+1.1479,+1.2329,+1.348,+1.623,+1.918,+2.4,+2.5]
nBinsEta_2D_endcap = len(Eta_Bins_2D_endcap) - 1
nBinsPhi_2D_endcap = 12

minPhi = -3.15
maxPhi =  3.15


# p_T Binning
Pt_Bins = [ 0.0, 60.0, 80.0, 100.0, 120.0, 140.0, 160.0, 180.0, 200.0, 250.0, 300.0, 600.0]
NumBinsInPt = len(Pt_Bins) - 1

# MCP Binning
Eta_Bins_MCP = [-2.5,-2.0,-1.6,-1.3,-1.0,-0.7,-0.4,-0.1,0.1,0.4,0.7,1.0,1.3,1.6,2.0,2.5]
nBinsEta_MCP = len(Eta_Bins_MCP) - 1
Phi_Bins_MCP = [-3.16,-2.905,-2.59,-2.12,-1.805,-1.335,-1.02,-0.55,-0.235,0.235,0.55,1.02,1.335,1.805,2.12,2.59,2.905,3.16]
nBinsPhi_MCP = len(Phi_Bins_MCP) - 1
    
# Input these once calculated for closure test
scaleFactors = {
    'HLT_mu50' : [0.9116, 0.9785],# barrel, endcaps
    'HLT_mu26_ivarmedium' : [0.9120, 0.9786]# barrel, endcaps
}

sysList = [
    '', # Nominal
    #'b_tag_70',
    #'met_150',
    #'jet_pt_30',
    #'_Syst_NoIso',
    #'_Syst_TightWP',
    #'MUON_ID__1down',
    #'MUON_ID__1up',
    #'MUON_MS__1down',
    #'MUON_MS__1up',
    #'MUON_SCALE__1down',
    #'MUON_SCALE__1up',
]
#myCategories = ['_v0-data_2015','_v0-data_2016','_v0-wjets','_v1-wjets_newSherpa','_v0-zjets','_v1-zjets_newSherpa','_v0-ttbar','_v0-stop','_v0-diboson','_v0-ttV']
#myBackgrounds = ['_v0-wjets','_v1-wjets_newSherpa','_v0-zjets','_v1-zjets_newSherpa','_v0-ttbar','_v0-stop','_v0-diboson','_v0-ttV']
#myCategories = ['_v3-data_2015','_v3-data_2016','_v3-wjets_newSherpa','_v3-zjets_newSherpa','_v3-ttbar','_v3-stop','_v3-diboson','_v3-ttV']
myBackgrounds = ['_v0-wjets_newSherpa','_v0-zjets_newSherpa','_v0-ttbar','_v0-stop','_v0-diboson','_v0-ttV']
dataTag = '_v0-data_2016'
myCategories = myBackgrounds + [dataTag]

yRatioMin = 0.8
yRatioMax = 1.2

EventWeight = '(weight_mc)*(weight_pileup)*(weight_jvt)*(weight_norm)*(weight_bTagSF_77)*(weight_leptonSF)/weight_indiv_SF_MU_Trigger'
myCuts = '(met_met>200000)*(Sum$(jet_mv2c10>=0.645925)<1)'#'1' 

if not os.path.exists(mergedFilesDir):
    print 'Creating merged files directory...',
    os.makedirs(mergedFilesDir)
    print 'Done!'

print 'Removing old plots...',
os.system('rm -rf '+plotDir+'*')
print 'Done!'

TagTrigger_2015 = 'HLT_xe70'
if do2015:
    if not os.path.exists(plotDir+'TagTrigger_'+TagTrigger_2015):
        os.makedirs(plotDir+'TagTrigger_'+TagTrigger_2015)
        
TagTrigger_2016 = 'HLT_xe100_L1XE60'
if do2016:
    if not os.path.exists(plotDir+'TagTrigger_'+TagTrigger_2016):
        os.makedirs(plotDir+'TagTrigger_'+TagTrigger_2016)

ProbeTriggers_2015 = [
    'HLT_mu50',
    'HLT_mu60_0eta105_msonly',
    'HLT_mu20_iloose_L1MU15', 
]
ProbeTriggers_2016 = [
    'HLT_mu50',
    'HLT_mu26_ivarmedium',
    #'HLT_mu60_0eta105_msonly',
]


if doMerging:
    if do2015:
        mcSamples_2015 = sorted(set(glob.glob(rootFilesDir+'user.srettie*'+identifier_2015+'*/'))
                                - set(glob.glob(rootFilesDir+'user.srettie*'+identifier_2015+'data*/')))
        print 'We have a total of %s 2015 MC samples, and they are:'%len(mcSamples_2015)
        if debug:
            for sample in mcSamples_2015:
                print sample

        dataSamples_2015 = glob.glob(rootFilesDir+'user.srettie*'+identifier_2015+'*data_2015*/')
        dataSamples_2015.sort()
        print 'We have a total of %s 2015 Data samples, and they are:'%len(dataSamples_2015)
        if debug:
            for sample in dataSamples_2015:
                print sample


    if do2016:
        mcSamples_2016 = sorted(set(glob.glob(rootFilesDir+'user.srettie*'+identifier_2016+'*/'))
                                - set(glob.glob(rootFilesDir+'user.srettie*'+identifier_2016+'data*/')))
        print 'We have a total of %s 2016 MC samples, and they are:'%len(mcSamples_2016)
        if debug:
            for sample in mcSamples_2016:
                print sample


        dataSamples_2016 = glob.glob(rootFilesDir+'user.srettie*'+identifier_2016+'*data_2016*/')
        dataSamples_2016.sort()
        print 'We have a total of %s 2016 Data samples, and they are:'%len(dataSamples_2016)
        if debug:
            for sample in dataSamples_2016:
                print sample


# Note: Should have numbers as below for MC samples
# 3(for nominal + each systematic production) * ( 36(non vjets) + 72(wjetsNew) + 96(zjetsNew) ) = 612 (each for 2015 and 2016)

# For 2017
# ( 38(non vjets) + 42(wjetsNew) + 42(zjetsNew) ) = 122

def GetN(sampleName):
    # Open file and get first entry in cutflow
    myfile = ROOT.TFile.Open(sampleName+'myOutput.root', 'OPEN')
    if '2015' in sampleName:
        cutflowHist = myfile.Get('Tag_'+TagTrigger_2015+'_2015/cutflow_mc_pu_zvtx')
    else:
        cutflowHist = myfile.Get('Tag_'+TagTrigger_2016+'_2016/cutflow_mc_pu_zvtx')
    N = cutflowHist.GetBinContent(1)
    myfile.Close()
    return N

def GetXS(sampleName):
    sampleNumber = sampleName[sampleName.find(user)+len(user):sampleName.find(user)+len(user)+6]
    XSFile = codecs.open('/home/srettie/ServiceWork/ServiceWorkScripts/plotting/XSection-MC15-13TeV.data', 'r').read().splitlines()   
    for line in XSFile:
        if len(line) == 0 or not line[0].isdigit():
            continue
        buff = shlex.split(line, '\t')
        if str(buff[0]) != sampleNumber:
            continue
        else:
            # Return XS, kFactor
            #if debug:
            #    print 'XS is '+str(buff[1])
            #    print 'kFactor is '+str(buff[2])
            return float(buff[1]), float(buff[2])
    print "Sample number not found! Returning 0..."
    return 0

def AddNormToTree(sampleName):
    if (not doSys) or ('_Syst_NoIso' in sampleName) or ('_Syst_TightWP' in sampleName) or ('v3' in sampleName):
        systs = ['nominal']
    else:
        systs = ['nominal','MUON_ID__1down','MUON_ID__1up','MUON_MS__1down','MUON_MS__1up','MUON_SCALE__1down','MUON_SCALE__1up']

    # Get normaliazation
    XS, kFactor = GetXS(sampleName)
    normN = GetN(sampleName)
    normWeight = XS*kFactor/normN
    if debug:
        print 'Adding normalization weight %s to %s'%(normWeight,sampleName)

    for syst in systs:
        if debug:
            print 'Merging systematics tree %s'%syst
        ifile = ROOT.TFile.Open(sampleName+'myOutput.root', 'update')
        itree = ifile.Get(syst)
        otree = ROOT.TTree(syst + '_norm', 'recreate')
        itree.AddFriend( otree )
        weight_norm = array( 'f', [0] )
        otree.Branch( 'weight_norm', weight_norm, 'weight_norm/F' )

        for ientry in xrange( itree.GetEntries() ):
            nb = itree.GetEntry( ientry )
            if nb <= 0:
                print 'Error reading file'
                sys.exit()
            weight_norm[0] = normWeight
            otree.Fill()
            
        itree.Write()
        otree.Write()
        ifile.Close()
    
    return

def GetHisto(sampleName, varexp, selection, emptyHisto, syst):
    isData = 'data' in sampleName
    # Initial Histogram Setup
    setupFile = ROOT.TFile.Open(sampleName+'myOutput.root', 'read')
    # Get tree from file
    if (not isData) and ('MUON_' in syst):
        readTree = setupFile.Get(syst)
    else:
        readTree = setupFile.Get('nominal')
    # Clone empty histogram for desired binning
    if isData:
        sampleNumber = sampleName[sampleName.find(user)+len(user):sampleName.find(user)+len(user)+8]
    else:
        sampleNumber = sampleName[sampleName.find(user)+len(user):sampleName.find(user)+len(user)+6]
    fullHisto = emptyHisto.Clone( sampleNumber )
    fullHisto.Sumw2()
    tempCanvas = ROOT.TCanvas()
    tempCanvas.cd()
    # Draw tree onto canvas
    #print 'About to draw %s >> %s, %s'%(varexp,sampleNumber,selection)
    readTree.Draw( varexp+'>>'+sampleNumber, selection)
    # Scale histogram by XS/N
    if not isData:
        XS, kFactor = GetXS(sampleName)
        #print 'Scaling by '+str(XS*kFactor*filterEff)
        fullHisto.Scale( (XS*kFactor) / GetN(sampleName) )
        #fullHisto.Scale( (XS) / GetN(sampleName) )
    fullHisto.SetDirectory(0)
    setupFile.Close()
    return fullHisto

def GetProcessHisto(process, varexp, selection, emptyHisto, syst):
    isData = 'data' in process
    # Initial Histogram Setup
    if isData or ('_Syst_' not in syst):
        if debug:
            print 'About to open file %s'%(mergedFilesDir+process[4:]+'.root')
        setupFile = ROOT.TFile.Open(mergedFilesDir+process[4:]+'.root', 'read')
    else:
        if debug:
            print 'About to open file %s'%(mergedFilesDir+process[4:]+syst+'.root')
        setupFile = ROOT.TFile.Open(mergedFilesDir+process[4:]+syst+'.root', 'read')
    # Get tree from file
    if (not isData) and ('MUON_' in syst):
        readTree = setupFile.Get(syst)
    else:
        readTree = setupFile.Get('nominal')
    #if '_newSherpa' in process:
    #    selection += '*(weight_sherpa_22_vjets)'
    # Clone empty histogram for desired binning
    fullHisto = emptyHisto.Clone( process )
    fullHisto.Sumw2()
    tempCanvas = ROOT.TCanvas()
    tempCanvas.cd()
    # Draw tree onto canvas
    #print 'About to draw %s >> %s, %s'%(varexp,sampleNumber,selection)
    if '_pt' in varexp or varexp == 'met_met':
        readTree.Draw( varexp+'/1000>>'+process, selection)
    else:
        readTree.Draw( varexp+'>>'+process, selection)
    fullHisto.SetDirectory(0)
    setupFile.Close()
    return fullHisto
    
def CombineCutStrings(ListOfCutStrings, Operator):
    NumStrings = len(ListOfCutStrings)
    ReturnCutString = ''
    for entry in range( NumStrings ):
        ReturnCutString += '(' + ListOfCutStrings[entry] + ')'
        if entry < NumStrings-1:
            ReturnCutString += ' '+Operator+' '
    return ReturnCutString

def ExtractPtCutFromTriggerName(TriggerName):
    TriggerName.lower
    PtCut = TriggerName[TriggerName.find('_mu')+3:]    
    if PtCut.find('_')<0:
        return float(PtCut)
    else:
        return float(PtCut[:PtCut.find('_')])

def CreateCutFlow(samples, TagTrig):
    can = ROOT.TCanvas()
    can.cd()
    cutflow = ROOT.TH1D()
    first = True
    isData = 'data' in samples[0]
    for sample in samples:
        # Don't use systematic variations for cutflow
        if ('_Syst_NoIso' in sample) or ('_Syst_TightWP' in sample):
            continue
        # Use appropriate vjets samples
        if ('wjets' in sample) or ('zjets' in sample):
            if not useNewSherpa and ('_newSherpa' in sample):
                continue
            if useNewSherpa and ('_newSherpa' not in sample):
                continue
        # Open file and get cutflow
        if debug:
            print 'Opening file for cutflow of sample %s'%sample
        myfile = ROOT.TFile.Open(sample+'myOutput.root', 'OPEN')
        if '2015' in sample:
            cutflowTemp = myfile.Get('Tag_'+TagTrigger_2015+'_2015/cutflow_mc_pu_zvtx')
        else:
            cutflowTemp = myfile.Get('Tag_'+TagTrigger_2016+'_2016/cutflow_mc_pu_zvtx')

        if not isData:
            XS, kFactor = GetXS(sample)
            cutflowTemp.Scale( (XS*kFactor) / GetN(sample) )
        if first == True:
            cutflow = cutflowTemp
            first = False
        else:
            cutflow.Add( cutflow, cutflowTemp )
        cutflow.SetDirectory(0)
        myfile.Close()
    
    cutflow.Draw('text')

    if isData:
        if '2015' in samples[0]:
            can.Print(plotDir+'TagTrigger_'+TagTrigger_2015+'/cutflow_data.pdf')
        else:
            can.Print(plotDir+'TagTrigger_'+TagTrigger_2016+'/cutflow_data.pdf')
    else:
        if '2015' in samples[0]:
            can.Print(plotDir+'TagTrigger_'+TagTrigger_2015+'/cutflow.pdf')
        else:
            can.Print(plotDir+'TagTrigger_'+TagTrigger_2016+'/cutflow.pdf')

def CreateValidationPlots(TagTrig, EventWeight, cuts, Luminosity):

    year = ''
    if TagTrig == TagTrigger_2015:
        year = '2015'
    elif TagTrig == TagTrigger_2016:
        year = '2016'
        
    can = ROOT.TCanvas("single_plot_canvas_sub_validation"+year,"Single Plot with Subratio Canvas", 0, 0, 1000, 850)
    r = 0.3
    epsilon = 0.02
    can.cd()

    pad1 = ROOT.TPad("pad1", "pad1", 0, r-epsilon, 1, 1.0)
    pad1.SetBottomMargin(epsilon)
    pad1.Draw()
    pad1.cd()

    can.cd()
    pad2 = ROOT.TPad("pad2", "pad2", 0, 0, 1, r*(1-epsilon))
    pad2.SetTopMargin(0.)
    pad2.SetFillColor(0)
    pad2.SetFillStyle(0)
    pad2.SetBottomMargin(0.3)
    pad2.SetGridy() # horizontal grid
    pad2.Draw()
    pad2.cd()       # pad2 becomes the current pad

    # Now back to pad one for initial plot
    pad1.cd()
    
    nBinsPt = 20#150
    minPt      =   0.0
    maxPt      = 400.0#300.0
        
    nBinsEta_jet =  24
    minEta     = -2.5
    maxEta      =  2.5
    
    nBinsPhi_jet =  24

    nBinsMET = 20
    minMET = 160.0
    maxMET = 560.0

    nBinsMu = 50
    minMu = 0
    maxMu = 50.0

    
    
    # Define which validation variables to plot
    myVars = [
        ['mu_pt', nBinsPt, minPt, maxPt],
        ['mu_eta', nBinsEta_MCP, Eta_Bins_MCP],
        ['mu_phi', nBinsPhi_MCP, Phi_Bins_MCP],
        ['jet_pt', nBinsPt, minPt, maxPt],
        ['jet_eta', nBinsEta_jet, minEta, maxEta],
        ['jet_phi', nBinsPhi_jet, minPhi, maxPhi],
        ['met_met', nBinsMET, minMET, maxMET],
        ['mu', nBinsMu, minMu, maxMu]
    ]

    for var in myVars:
        varExp = var[0]
        nBins = var[1]
        if len(var) == 4:
            varMin = var[2]
            varMax = var[3]
        elif len(var) == 3:
            varMin = var[2][0]
            varMax = var[2][len(var[2])-1]
            varBins = var[2]
        
        # Create MC and Data plots
        if len(var) == 4:
            emptyHist = ROOT.TH1D('emptyHist'+varExp, '', nBins, varMin, varMax)
        elif len(var) == 3:
            emptyHist = ROOT.TH1D('emptyHist'+varExp, '', nBins, array('d', varBins))


        for bkg in myBackgrounds:
            if 'diboson' in bkg:
                mcPlot_diboson = GetProcessHisto(bkg, varExp, '(' + EventWeight + ')*(' + cuts + ')*(' + TagTrig + ')', emptyHist, '')
            elif 'stop' in bkg:
                mcPlot_stop = GetProcessHisto(bkg, varExp, '(' + EventWeight + ')*(' + cuts + ')*(' + TagTrig + ')', emptyHist, '')
            elif 'ttbar' in bkg:
                mcPlot_ttbar = GetProcessHisto(bkg, varExp, '(' + EventWeight + ')*(' + cuts + ')*(' + TagTrig + ')', emptyHist, '')
            elif 'ttV' in bkg:
                mcPlot_ttV = GetProcessHisto(bkg, varExp, '(' + EventWeight + ')*(' + cuts + ')*(' + TagTrig + ')', emptyHist, '')
            elif 'wjets' in bkg:
                if useNewSherpa and 'wjets_newSherpa' in bkg:
                    mcPlot_wjets = GetProcessHisto(bkg, varExp, '(' + EventWeight + ')*(' + cuts + ')*(' + TagTrig + ')', emptyHist, '')
                if not useNewSherpa and '_newSherpa' not in bkg:
                    mcPlot_wjets = GetProcessHisto(bkg, varExp, '(' + EventWeight + ')*(' + cuts + ')*(' + TagTrig + ')', emptyHist, '')
            elif 'zjets' in bkg:
                if useNewSherpa and 'zjets_newSherpa' in bkg:
                    mcPlot_zjets = GetProcessHisto(bkg, varExp, '(' + EventWeight + ')*(' + cuts + ')*(' + TagTrig + ')', emptyHist, '')
                if not useNewSherpa and '_newSherpa' not in bkg:
                    mcPlot_zjets = GetProcessHisto(bkg, varExp, '(' + EventWeight + ')*(' + cuts + ')*(' + TagTrig + ')', emptyHist, '')


        mcPlot_diboson.Sumw2()
        mcPlot_stop.Sumw2()
        mcPlot_ttbar.Sumw2()
        mcPlot_ttV.Sumw2()
        mcPlot_wjets.Sumw2()
        mcPlot_zjets.Sumw2()
        mcPlot_total = ROOT.THStack('MCHist'+varExp, '')

        dataPlot = GetProcessHisto(dataTag, varExp, '(' + cuts + ')*(' + TagTrig + ')', emptyHist, '')
            
        dataPlot.Sumw2()

        # Set Background Colors
        mcPlot_diboson.SetFillColor(ROOT.kYellow)
        mcPlot_stop.SetFillColor(ROOT.kSpring)
        mcPlot_ttbar.SetFillColor(ROOT.kRed+1)
        mcPlot_ttV.SetFillColor(ROOT.kCyan)
        mcPlot_wjets.SetFillColor(ROOT.kAzure-9)
        mcPlot_zjets.SetFillColor(ROOT.kOrange)
        # Scale MC to luminosity
        # Note that XS used are given in /pb, so multiply by 1000 to convert to /fb
        mcPlot_diboson.Scale(1000*Luminosity)
        mcPlot_stop.Scale(1000*Luminosity)
        mcPlot_ttbar.Scale(1000*Luminosity)
        mcPlot_ttV.Scale(1000*Luminosity)
        mcPlot_wjets.Scale(1000*Luminosity)
        mcPlot_zjets.Scale(1000*Luminosity)

        # Scale mu distribution for 2016 data
        #if year == '2016' and varExp == 'mu':
            #dataPlot.Scale(1.0/1.16)
            #dataPlot.Scale(1.0/1.09)
        
        # Add MC to THStack
        mcPlot_total.Add(mcPlot_ttV)
        mcPlot_total.Add(mcPlot_diboson)
        mcPlot_total.Add(mcPlot_stop)
        mcPlot_total.Add(mcPlot_ttbar)
        mcPlot_total.Add(mcPlot_zjets)
        mcPlot_total.Add(mcPlot_wjets)
        # Draw MC and data on same plot
        pad1.cd()
        mcPlot_total.Draw("Fhist")
        mcPlot_total.GetXaxis().SetLabelSize(0.)
        mcPlot_total.GetXaxis().SetTitleSize(0.)
        mcPlot_total.SetMinimum(0)
        mcPlot_total.SetMaximum(1.5*mcPlot_total.GetMaximum())
        mcPlot_total.GetYaxis().SetTitle('Events')
        mcPlot_total.GetYaxis().SetTitleOffset(1.5)
        mcPlot_total.Draw("Fhist")
        statErr = mcPlot_total.GetStack().Last().Clone()
        statErr.SetFillColorAlpha(ROOT.kBlack, 1.0)
        #statErr.SetFillStyle(3244)
        statErr.SetFillStyle(3004)
        statErr.Draw("SAME E2")
        dataPlot.Draw("SAME E0")
        # Add legend
        leg = ROOT.TLegend(0.45, 0.6, 0.88, 0.88)
        intError = ROOT.Double()
        
        integral = round(dataPlot.IntegralAndError(0, dataPlot.GetNbinsX(), intError), 2)
        leg.AddEntry(dataPlot,       '%s Data       %.2f +/- %.2f'%(year, integral, intError), 'lep')
        integral = round(mcPlot_wjets.IntegralAndError(0, mcPlot_wjets.GetNbinsX(), intError), 2)
        leg.AddEntry(mcPlot_wjets,   'W+Jets        %.2f +/- %.2f'%(integral, intError), 'F')
        integral = round(mcPlot_zjets.IntegralAndError(0, mcPlot_zjets.GetNbinsX(), intError), 2)
        leg.AddEntry(mcPlot_zjets,   'Z+Jets        %.2f +/- %.2f'%(integral, intError), 'F')
        integral = round(mcPlot_ttbar.IntegralAndError(0, mcPlot_ttbar.GetNbinsX(), intError), 2)
        leg.AddEntry(mcPlot_ttbar,   'ttbar         %.2f +/- %.2f'%(integral, intError), 'F')
        integral = round(mcPlot_stop.IntegralAndError(0, mcPlot_stop.GetNbinsX(), intError), 2)
        leg.AddEntry(mcPlot_stop,    'Single Top    %.2f +/- %.2f'%(integral, intError), 'F')
        integral = round(mcPlot_diboson.IntegralAndError(0, mcPlot_diboson.GetNbinsX(), intError), 2)
        leg.AddEntry(mcPlot_diboson, 'Diboson       %.2f +/- %.2f'%(integral, intError), 'F')
        integral = round(mcPlot_ttV.IntegralAndError(0, mcPlot_ttV.GetNbinsX(), intError), 2)
        leg.AddEntry(mcPlot_ttV,     'ttV           %.2f +/- %.2f'%(integral, intError), 'F')
        leg.SetTextAlign(13)
        leg.Draw("SAME")
        # Add latex
        t = ROOT.TLatex()
        t.SetNDC(1)
        t.SetTextAlign(13)
        t.SetTextColor(ROOT.kBlack)
        t.SetTextSize(0.045)
        t.DrawLatex(0.15, 0.88, '#font[72]{ATLAS} Internal')
        t.DrawLatex(0.15, 0.82, '#sqrt{s} = 13 TeV, %.1f fb^{-1}'%(Luminosity))
        totalYield = dataPlot.Integral() / mcPlot_total.GetStack().Last().Integral()
        t.DrawLatex(0.15, 0.74, 'Total Data/MC: %.3f'%(totalYield))
        
        pad1.Modified()

        # Draw ratio plot
        pad2.cd()
        ratio = dataPlot.Clone()
        ratio.Divide(mcPlot_total.GetStack().Last())
        ratio.GetXaxis().SetTitle(varExp)
        ratio.GetXaxis().SetTitleSize(0.10)
        ratio.GetXaxis().SetLabelSize(0.10)
        ratio.SetLineColor(ROOT.kBlack)
        ratio.GetYaxis().SetTitleSize(0.0)
        ratio.GetYaxis().SetLabelSize(0.0)
        ratio.SetMinimum(yRatioMin)
        ratio.SetMaximum(yRatioMax)
        ratio.Draw()
        
        yTickMin = 1 - ((1-yRatioMin)/2)
        yTickMax = 1 + ((yRatioMax-1)/2)
        axis = ROOT.TGaxis(varMin, yTickMin, varMin, yTickMax, yTickMin, yTickMax, 3, '')
        axis.SetTitle('Data/MC')
        axis.SetTitleFont(42)
        axis.SetTitleSize(0.1)
        axis.SetTitleOffset(0.5)
        axis.SetTitleFont(42)
        axis.SetLabelSize(0.1)
        axis.Draw('same')
        
        line = ROOT.TLine(varMin, 1., varMax, 1.)
        line.SetLineColor(ROOT.kRed)
        line.SetLineWidth(2)
        line.Draw('same')
        ratio.Draw('same,P,e0')
        
        pad2.Modified()
        
        if year == '2015':
            can.Print(plotDir+'TagTrigger_'+TagTrigger_2015+'/Validation_'+varExp+'.pdf')
        elif year == '2016':
            can.Print(plotDir+'TagTrigger_'+TagTrigger_2016+'/Validation_'+varExp+'.pdf')

        pad1.Clear()
        pad2.Clear()

def CreateEfficiencyHistograms(TagTrig, ProbeTrig, Region, EventWeight, cuts, Luminosity, syst):
    year = ''
    if TagTrig == TagTrigger_2015:
        year = '2015'
    elif TagTrig == TagTrigger_2016:
        year = '2016'

    if debug:
        print 'Creating Efficiency Histogram for:'
        print 'Tag Trigger: '+str(TagTrig)
        print 'Probe Trigger: '+str(ProbeTrig)
        print 'Region: '+str(Region)
        print 'Year: '+str(year)
        print 'Systematic Variation: '+str(syst)
        
    if syst == 'b_tag_70':
        cuts = '(met_met>200000)'
        EventWeight += '*(weight_bTagSF_70)/(weight_bTagSF_77)'
    elif syst == 'met_150':
        cuts = '(met_met>150000)*(Sum$(jet_mv2c10>=0.645925)<1)'
    elif syst == 'jet_pt_30':
        cuts += '*(Sum$(jet_pt<=30000) == 0)'        
        
    Pt_Min      =   0.0
    Pt_Max      = 600.0#300.0
    Pt_Cutoff = 100.0#ExtractPtCutFromTriggerName(ProbeTrig)

    #NumBinsInEta =  24
    #if Region=='barrel':
    #    Eta_Min      = -1.05
    #    Eta_Max      =  1.05
    #elif Region=='endcaps':
    #    Eta_Min      = -2.5
    #    Eta_Max      =  2.5
        
    #NumBinsInPhi =  24
    #Phi_Min      = -3.15
    #Phi_Max      =  3.15

    if Region=='barrel':
        CutString_EtaRange = 'mu_eta > -1.05 && mu_eta < 1.05'
    elif Region=='endcaps':
        CutString_EtaRange = 'mu_eta <= -1.05 || mu_eta >= 1.05'
    else:
        print 'ERROR: Region', Region, 'not found'
        sys.exit(1)
        
    CutString_tagOnly   = CombineCutStrings([ TagTrig, 
                                              CutString_EtaRange
                                             ], '&&')

    CutString_withProbe = CombineCutStrings([ TagTrig, 
                                              ProbeTrig, 
                                              'mu_trigMatch_'+ProbeTrig, 
                                              CutString_EtaRange
                                             ], '&&')
    
    if not os.path.exists(plotDir+'TagTrigger_'+TagTrig):
        os.makedirs(plotDir+'TagTrigger_'+TagTrig)
        
    # Trigger efficiency as a function of muon pT
    emptyHist         = ROOT.TH1D('emptyHist',         '', NumBinsInPt, array('d', Pt_Bins))
        

    Hist_pt_tagOnly_data = GetProcessHisto(dataTag, 'mu_pt', '(' + cuts + ')*(' + CutString_tagOnly + ')', emptyHist, syst)
    Hist_pt_withProbe_data = GetProcessHisto(dataTag, 'mu_pt', '(' + cuts + ')*(' + CutString_withProbe + ')', emptyHist, syst)
        

    Hist_pt_tagOnly_data.Sumw2()
    Hist_pt_withProbe_data.Sumw2()



    
    Hist_pt_tagOnly_mc   = ROOT.TH1D('Hist_pt_tagOnly_mc',   '', NumBinsInPt, array('d', Pt_Bins))
    Hist_pt_tagOnly_mc.Sumw2()
    Hist_pt_withProbe_mc = ROOT.TH1D('Hist_pt_withProbe_mc', '', NumBinsInPt, array('d', Pt_Bins))
    Hist_pt_withProbe_mc.Sumw2()


    for bkg in myBackgrounds:
        if not useNewSherpa and '_newSherpa' in bkg:
            continue
        if useNewSherpa and ('jets' in bkg) and ('_newSherpa' not in bkg):
            continue
        
        Hist_pt_tagOnly_mc.Add( Hist_pt_tagOnly_mc, GetProcessHisto(bkg, 'mu_pt', '(' + EventWeight + ')*(' + cuts + ')*(' + CutString_tagOnly   +')', emptyHist, syst) )
        Hist_pt_withProbe_mc.Add( Hist_pt_withProbe_mc, GetProcessHisto(bkg, 'mu_pt', '(' + EventWeight + ')*(' + cuts + ')*(' + CutString_withProbe   +')', emptyHist, syst) )


    if doClosure:
        if Region == 'barrel':
            Hist_pt_withProbe_mc.Scale(scaleFactors[ProbeTrig][0])
        elif Region == 'endcaps':
            Hist_pt_withProbe_mc.Scale(scaleFactors[ProbeTrig][1])
        
    Hist_efficiency_data = ROOT.TGraphAsymmErrors(NumBinsInPt)
    Hist_efficiency_data.Divide(Hist_pt_withProbe_data, Hist_pt_tagOnly_data)
    Hist_efficiency_mc   = ROOT.TGraphAsymmErrors(NumBinsInPt)
    Hist_efficiency_mc.Divide(Hist_pt_withProbe_mc, Hist_pt_tagOnly_mc)

    data_probe = Hist_pt_withProbe_data.Clone()
    data_tag   = Hist_pt_tagOnly_data.Clone()

    mc_probe   = Hist_pt_withProbe_mc.Clone()
    mc_tag     = Hist_pt_tagOnly_mc.Clone()
    
    data_probe.Divide(data_tag)
    mc_probe.Divide(mc_tag)
        
    # This needs to be done with asym errors
    Hist_SF = ROOT.TH1D('Hist_SF', '', NumBinsInPt, array('d',Pt_Bins))
    Hist_SF.Divide(data_probe, mc_probe)   # default divide, errors adjusted below!
    
    # Calculate proper errors for SF histogram
    xval_data = ROOT.Double()
    yval_data = ROOT.Double()
    xval_mc   = ROOT.Double()
    yval_mc   = ROOT.Double()
    for binNumber in range(1, Hist_SF.GetNbinsX() + 1):
        Hist_efficiency_data.GetPoint(binNumber-1, xval_data, yval_data)
        Hist_efficiency_mc.GetPoint(binNumber-1, xval_mc, yval_mc)
        if Hist_SF.GetBinContent(binNumber) == 0:
            print '--- !!! WARNING: empty bin in SF calculation, will most likely cause issues - consider rebinning around', xval_mc, xval_data, 'GeV'
        try:
            asym_err = Hist_SF.GetBinContent(binNumber) * ( (Hist_efficiency_data.GetErrorY(binNumber-1) / yval_data)**2 + (Hist_efficiency_mc.GetErrorY(binNumber-1) / yval_mc)**2 )**0.5
        except:
            asym_err = 0.0
        Hist_SF.SetBinError(binNumber, asym_err)    

    # Style
    Hist_efficiency_mc.SetTitle(ProbeTrig+' ('+Region+')')
    #Hist_efficiency_mc.GetXaxis().SetTitle('muon p_{T} [GeV]')
    Hist_efficiency_mc.GetYaxis().SetTitle('Efficiency')
    Hist_efficiency_mc.SetLineColor(ROOT.kGreen)
    Hist_efficiency_mc.SetFillColorAlpha(ROOT.kGreen, 0.5)
    Hist_efficiency_mc.SetFillStyle(3244)

    Hist_efficiency_data.SetLineColor(ROOT.kBlack)
    Hist_efficiency_data.SetFillColorAlpha(ROOT.kBlack, 0.5)
    Hist_efficiency_data.SetFillStyle(3244)

    Hist_SF.SetLineColor(ROOT.kBlue)
    Hist_SF.SetFillColorAlpha(ROOT.kBlue, 0.5)
    Hist_SF.SetFillStyle(3244)

    # Fits
    Fit_data = ROOT.TF1('pol1','[0]+[1]*x', Pt_Cutoff, Pt_Max)
    Fit_c_data = ROOT.TF1('pol0','[0]', Pt_Cutoff, Pt_Max)
    Fit_data.SetParameter(0,1)
    Fit_data.SetParameter(1,0)
    Fit_c_data.SetParameter(0,1)
    Fit_c_data.SetLineColor(ROOT.kBlack)
    Fit_c_data.SetLineWidth(2)
    
    Fit_mc = ROOT.TF1('pol1','[0]+[1]*x', Pt_Cutoff, Pt_Max)
    Fit_c_mc = ROOT.TF1('pol0','[0]', Pt_Cutoff, Pt_Max)
    Fit_mc.SetParameter(0,1)
    Fit_mc.SetParameter(1,0)
    Fit_c_mc.SetParameter(0,1)
    Fit_c_mc.SetLineColor(ROOT.kGreen)
    Fit_c_mc.SetLineWidth(2)
    
    Fit_c_SF = ROOT.TF1('pol0','[0]', Pt_Cutoff, Pt_Max)
    Fit_c_SF.SetParameter(0,1)
    Fit_c_SF.SetLineColor(ROOT.kBlue+2)
    #Fit_c_SF.SetLineStyle(2)
    Fit_c_SF.SetLineWidth(2)
    
    Hist_efficiency_data.Fit(Fit_data, 'R Q')   # order 1
    Hist_efficiency_data.Fit(Fit_c_data, 'R Q') # order 0
    
    Hist_efficiency_mc.Fit(Fit_mc, 'R Q')   # order 1
    Hist_efficiency_mc.Fit(Fit_c_mc, 'R Q') # order 0
    
    Hist_SF.Fit(Fit_c_SF, 'R Q') # order 0
    
    Fit_P0_data     = Fit_data.GetParameter(0)
    Fit_P0_Err_data = Fit_data.GetParError(0)
    
    Fit_P1_data     = Fit_data.GetParameter(1)
    Fit_P1_Err_data = Fit_data.GetParError(1)
    
    Fit_c_P0_data     = Fit_c_data.GetParameter(0)
    Fit_c_P0_Err_data = Fit_c_data.GetParError(0)
    
    Fit_P0_mc     = Fit_mc.GetParameter(0)
    Fit_P0_Err_mc = Fit_mc.GetParError(0)
    
    Fit_P1_mc     = Fit_mc.GetParameter(1)
    Fit_P1_Err_mc = Fit_mc.GetParError(1)
    
    Fit_c_P0_mc     = Fit_c_mc.GetParameter(0)
    Fit_c_P0_Err_mc = Fit_c_mc.GetParError(0)
    
    Fit_c_P0_SF     = Fit_c_SF.GetParameter(0)
    Fit_c_P0_Err_SF = Fit_c_SF.GetParError(0)
    
    #######################
    can = ROOT.TCanvas("single_plot_canvas_sub"+year+ProbeTrig+Region+syst,"Single Plot with Subratio Canvas")
    r = 0.3
    epsilon = 0.02
    can.cd()

    pad1 = ROOT.TPad("pad1", "pad1", 0, r-epsilon, 1, 1.0)
    pad1.SetBottomMargin(epsilon)
    pad1.Draw()
    pad1.cd()

    can.cd()
    pad2 = ROOT.TPad("pad2", "pad2", 0, 0, 1, r*(1-epsilon))
    pad2.SetTopMargin(0.)
    pad2.SetFillColor(0)
    pad2.SetFillStyle(0)
    pad2.SetBottomMargin(0.3)
    pad2.SetGridy() # horizontal grid
    pad2.Draw()
    pad2.cd()       # pad2 becomes the current pad

    # Now back to pad one for initial plot
    pad1.cd()
    
    Hist_efficiency_mc.GetXaxis().SetLabelSize(0.)
    Hist_efficiency_mc.GetXaxis().SetTitleSize(0.)
    Hist_efficiency_mc.SetMinimum(0.0)
    Hist_efficiency_mc.SetMaximum(1.0)
    Hist_efficiency_mc.GetXaxis().SetRangeUser(Pt_Min, Pt_Max)
    Hist_efficiency_mc.Draw('A,P,E2')
    Hist_efficiency_mc.Draw('SAME,E')
    Hist_efficiency_data.Draw('SAME,E2')
    Hist_efficiency_data.Draw('SAME,E')

    t = ROOT.TLatex()
    t.SetNDC(1)
    t.SetTextAlign(13)
    t.SetTextColor(ROOT.kBlack)
    t.SetTextSize(0.035)
    t.DrawLatex(0.15, 0.85, '#font[72]{ATLAS} Internal, #sqrt{s} = 13 TeV, %.1f fb^{-1}'%(Luminosity))

    
    PlotLabel = ROOT.TText()
    PlotLabel.SetTextFont(42)
    PlotLabel.SetTextSize(0.03)
    
    data_eff = Fit_c_P0_data
    data_eff_err = Fit_c_P0_Err_data
    mc_eff = Fit_c_P0_mc
    mc_eff_err = Fit_c_P0_Err_mc
    SF = Fit_c_P0_SF
    SF_err = Fit_c_P0_Err_SF
    SF_ratio = Fit_c_P0_data / Fit_c_P0_mc
    SF_err_ratio = ( (data_eff_err / data_eff)**2  + (mc_eff_err / mc_eff)**2 )**0.5 * SF
        
    PlotLabel.DrawText(70, 0.50, 'SF:')
    PlotLabel.DrawText(120, 0.50, 'ratio of fits:')
    PlotLabel.DrawText(120, 0.46, 'direct fit:')
    PlotLabel.DrawText(215, 0.50, str(round(SF_ratio,6)) + ' +/- ' + str(round(SF_err_ratio,6)))
    PlotLabel.DrawText(215, 0.46, str(round(SF,6)) + ' +/- ' + str(round(SF_err,6)))
    
    PlotLabel.DrawText(70, 0.40,  'MC:')
    PlotLabel.DrawText(120, 0.40, 'fit offset:')
    PlotLabel.DrawText(120, 0.36, 'slope:')
    PlotLabel.DrawText(120, 0.32, 'const. fit: ')
    PlotLabel.DrawText(215, 0.40, str(round(Fit_P0_mc,6)) + ' +/- ' + str(round(Fit_P0_Err_mc,6)))
    PlotLabel.DrawText(215, 0.36, str(round(Fit_P1_mc,6)) + ' +/- ' + str(round(Fit_P1_Err_mc,6)))
    PlotLabel.DrawText(215, 0.32, str(round(Fit_c_P0_mc,6)) + ' +/- ' + str(round(Fit_c_P0_Err_mc,6)))
    
    PlotLabel.DrawText(70, 0.26, 'data:')
    PlotLabel.DrawText(120, 0.26, 'fit offset:')
    PlotLabel.DrawText(120, 0.22, 'slope:')
    PlotLabel.DrawText(120, 0.18, 'const. fit:')
    PlotLabel.DrawText(215, 0.26, str(round(Fit_P0_data,6)) + ' +/- ' + str(round(Fit_P0_Err_data,6)))
    PlotLabel.DrawText(215, 0.22, str(round(Fit_P1_data,6)) + ' +/- ' + str(round(Fit_P1_Err_data,6)))
    PlotLabel.DrawText(215, 0.18, str(round(Fit_c_P0_data,6)) + ' +/- ' + str(round(Fit_c_P0_Err_data,6)))

    can.Update()

    # Summary for spreadsheet
    print ' ~~~ '
    print ' ~~~ ', ProbeTrig, ' ~~~ ', Region, ' ~~~ ', year, ' ~~~ ', syst
    print ' ~~~ ', str(round(mc_eff,5)) + ',' + str(round(mc_eff_err,5)) + ',' + str(round(data_eff,5)) + ',' + str(round(data_eff_err,5)) + ',' + str(round(SF,5)) + ',' + str(round(SF_err,5))
    print ' ~~~ '
    
    leg = ROOT.TLegend(0.61, 0.06, 0.89, 0.29)
    leg.AddEntry(Hist_efficiency_mc,   'MC eff.', 'f')
    leg.AddEntry(Fit_c_mc,   'MC fit', 'l')
    leg.AddEntry(Hist_efficiency_data, 'data eff.', 'F')
    leg.AddEntry(Fit_c_data, 'data fit', 'l')
    leg.AddEntry(Hist_SF, 'SF', 'F')
    leg.AddEntry(Fit_c_SF, 'SF fit', 'l')
    leg.SetTextSize(0.03)
    leg.SetNColumns(2)
    leg.SetBorderSize(1)
    leg.SetFillColorAlpha(0,1)
    leg.Draw("SAME")
    
    pad2.cd()
    
    Hist_SF.GetXaxis().SetTitle('muon p_{T} [GeV]')
    Hist_SF.GetYaxis().SetTitle('SF (data / MC eff.)')
    
    Hist_SF.GetYaxis().SetTitleOffset(0.5)
    
    Hist_SF.GetXaxis().SetTitleSize(0.1)
    Hist_SF.GetYaxis().SetTitleSize(0.1)
    
    Hist_SF.GetXaxis().SetLabelSize(0.1)
    Hist_SF.GetYaxis().SetLabelSize(0.1)
    
    Hist_SF.Draw('E2') 
    Hist_SF.Draw('SAME E') 
    
    pad1.Modified()
    pad2.Modified()
    can.Modified()

    if syst == '':
        can.Print(plotDir+'TagTrigger_'+TagTrig+'/Pt_'+Region+'_'+ProbeTrig+'_Nominal.pdf')
    else:
        can.Print(plotDir+'TagTrigger_'+TagTrig+'/Pt_'+Region+'_'+ProbeTrig+'_'+syst+'.pdf')

    # Clean up
    pad1.Delete()
    pad2.Delete()
    can.Clear()
    emptyHist.Delete()
    Hist_pt_tagOnly_data.Delete()
    Hist_pt_withProbe_data.Delete()
    Hist_pt_tagOnly_mc.Delete()
    Hist_pt_withProbe_mc.Delete()
    Hist_efficiency_data.Delete()
    Hist_efficiency_mc.Delete()
    data_probe.Delete()
    data_tag.Delete()
    mc_probe.Delete()
    mc_tag.Delete()
    Hist_SF.Delete()

    # Eta-Phi trigger effiency map
    if skip2D:
        return
    if syst != '':
        return
    if debug:
        print 'Now producing 2D distributions'
    CutString_tagOnly   = CombineCutStrings([CutString_tagOnly,   'mu_pt/1000>'+str(Pt_Cutoff)], '&&')
    CutString_withProbe = CombineCutStrings([CutString_withProbe, 'mu_pt/1000>'+str(Pt_Cutoff)], '&&')
 
    if Region=='barrel':
        Eta_Bins = Eta_Bins_2D_barrel
        nBinsEta = nBinsEta_2D_barrel
        nBinsPhi = nBinsPhi_2D_barrel
    elif Region=='endcaps':
        Eta_Bins = Eta_Bins_2D_endcap
        nBinsEta = nBinsEta_2D_endcap
        nBinsPhi = nBinsPhi_2D_endcap
    else:
        print 'Region needs to be either barrel or endcaps!'
        return

    empty2DHist           = ROOT.TH2D('empty2DHist',           '', nBinsEta, array('d', Eta_Bins), nBinsPhi, minPhi, maxPhi)

    Hist_etaphi_tagOnly_data = GetProcessHisto(dataTag, 'mu_phi:mu_eta', '(' + cuts + ')*(' + CutString_tagOnly + ')', empty2DHist, syst)
    Hist_etaphi_withProbe_data = GetProcessHisto(dataTag, 'mu_phi:mu_eta', '(' + cuts + ')*(' + CutString_withProbe + ')', empty2DHist, syst)
    
    
    Hist_etaphi_tagOnly_mc   = ROOT.TH2D('Hist_etaphi_tagOnly_mc',   '', nBinsEta, array('d', Eta_Bins), nBinsPhi, minPhi, maxPhi)
    Hist_etaphi_withProbe_mc = ROOT.TH2D('Hist_etaphi_withProbe_mc', '', nBinsEta, array('d', Eta_Bins), nBinsPhi, minPhi, maxPhi)

    for bkg in myBackgrounds:
        if not useNewSherpa and '_newSherpa' in bkg:
            continue
        if useNewSherpa and ('jets' in bkg) and ('_newSherpa' not in bkg):
            continue
        Hist_etaphi_tagOnly_mc.Add( Hist_etaphi_tagOnly_mc, GetProcessHisto(bkg, 'mu_phi:mu_eta', '(' + cuts + ')*(' + CutString_tagOnly   +')', empty2DHist, syst) )
        Hist_etaphi_withProbe_mc.Add( Hist_etaphi_withProbe_mc, GetProcessHisto(bkg, 'mu_phi:mu_eta', '(' + cuts + ')*(' + CutString_withProbe   +')', empty2DHist, syst) )


    if doClosure:
        if Region == 'barrel':
            Hist_etaphi_withProbe_mc.Scale(scaleFactors[ProbeTrig][0])
        elif Region == 'endcaps':
            Hist_etaphi_withProbe_mc.Scale(scaleFactors[ProbeTrig][1])

    Hist_etaphi_withProbe_data.Divide(Hist_etaphi_tagOnly_data)
    Hist_etaphi_withProbe_mc.Divide(Hist_etaphi_tagOnly_mc)
    
    t = ROOT.TLatex()
    t.SetNDC(1)
    t.SetTextAlign(13)
    t.SetTextColor(ROOT.kBlack)
    t.SetTextSize(0.035)
    t.DrawLatex(0.15, 0.85, '#font[72]{ATLAS} Internal, #sqrt{s} = 13 TeV, %.1f fb^{-1}'%(Luminosity))

    
    Hist_etaphi_withProbe_data.SetTitle('#font[72]{ATLAS} Internal, '+ProbeTrig+' ('+Region+', p_{T}^{#mu}>'+str(Pt_Cutoff)+') Data Eff.')
    Hist_etaphi_withProbe_data.GetXaxis().SetTitle('#eta')
    Hist_etaphi_withProbe_data.GetYaxis().SetTitle('#phi')
    Hist_etaphi_withProbe_mc.SetTitle('#font[72]{ATLAS} Internal, '+ProbeTrig+' ('+Region+', p_{T}^{#mu}>'+str(Pt_Cutoff)+') MC Eff.')
    Hist_etaphi_withProbe_mc.GetXaxis().SetTitle('#eta')
    Hist_etaphi_withProbe_mc.GetYaxis().SetTitle('#phi')
    
    Hist_etaphi_withProbe_data.SetAxisRange(0,1,'Z')
    Hist_etaphi_withProbe_mc.SetAxisRange(0,1,'Z')
    
    can.cd()
    can.SetRightMargin(0.13)
    
    Hist_etaphi_withProbe_data.Draw('colz')
    if syst == '':
        can.Print(plotDir+'TagTrigger_'+TagTrig+'/EtaPhi_'+Region+'_'+ProbeTrig+'_Nominal_data.pdf')
    else:
        can.Print(plotDir+'TagTrigger_'+TagTrig+'/EtaPhi_'+Region+'_'+ProbeTrig+'_'+syst+'_data.pdf')
        
    Hist_etaphi_withProbe_mc.Draw('colz')
    if syst == '':
        can.Print(plotDir+'TagTrigger_'+TagTrig+'/EtaPhi_'+Region+'_'+ProbeTrig+'_Nominal_mc.pdf')
    else:
        can.Print(plotDir+'TagTrigger_'+TagTrig+'/EtaPhi_'+Region+'_'+ProbeTrig+'_'+syst+'_mc.pdf')


    # Produce error plots
    Hist_etaphi_withProbe_data_err = empty2DHist.Clone()
    Hist_etaphi_withProbe_data_err.Sumw2()
    Hist_etaphi_withProbe_mc_err = empty2DHist.Clone()
    Hist_etaphi_withProbe_mc_err.Sumw2()
    
    
    for ibin in range(1, Hist_etaphi_withProbe_data.GetNbinsX()+1):
        for jbin in range(1,Hist_etaphi_withProbe_data.GetNbinsY()+1):
            Hist_etaphi_withProbe_data_err.SetBinContent(ibin, jbin, Hist_etaphi_withProbe_data.GetBinError(ibin, jbin))
            Hist_etaphi_withProbe_mc_err.SetBinContent(ibin, jbin, Hist_etaphi_withProbe_mc.GetBinError(ibin, jbin))


    Hist_etaphi_withProbe_data_err.SetTitle('#font[72]{ATLAS} Internal, '+ProbeTrig+' ('+Region+', p_{T}^{#mu}>'+str(Pt_Cutoff)+') Data Eff. Unc.')
    Hist_etaphi_withProbe_data_err.Draw('colz')
    if syst == '':
        can.Print(plotDir+'TagTrigger_'+TagTrig+'/EtaPhi_'+Region+'_'+ProbeTrig+'_Nominal_data_err.pdf')
    else:
        can.Print(plotDir+'TagTrigger_'+TagTrig+'/EtaPhi_'+Region+'_'+ProbeTrig+'_'+syst+'_data_err.pdf')
        
    Hist_etaphi_withProbe_mc_err.SetTitle('#font[72]{ATLAS} Internal, '+ProbeTrig+' ('+Region+', p_{T}^{#mu}>'+str(Pt_Cutoff)+') MC Eff. Unc.')
    Hist_etaphi_withProbe_mc_err.Draw('colz')
    if syst == '':
        can.Print(plotDir+'TagTrigger_'+TagTrig+'/EtaPhi_'+Region+'_'+ProbeTrig+'_Nominal_mc_err.pdf')
    else:
        can.Print(plotDir+'TagTrigger_'+TagTrig+'/EtaPhi_'+Region+'_'+ProbeTrig+'_'+syst+'_mc_err.pdf')

            
    # Clean up
    empty2DHist.Delete()
    Hist_etaphi_tagOnly_data.Delete()
    Hist_etaphi_withProbe_data.Delete()
    Hist_etaphi_tagOnly_mc.Delete()
    Hist_etaphi_withProbe_mc.Delete()
    Hist_etaphi_withProbe_data_err.Delete()
    Hist_etaphi_withProbe_mc_err.Delete()
    can.Clear()

     
if __name__ == '__main__':
    # Start by merging sub-files to get proper normalizaiton N
    if doMerging:
        if do2015:
            for sample in mcSamples_2015:
                print 'Producing merged output for %s'%sample
                os.system('hadd -f '+sample+'myOutput.root '+sample+'*.output.root')
                AddNormToTree(sample)
            for sample in dataSamples_2015:
                print 'Producing merged output for %s'%sample
                os.system('hadd -f '+sample+'myOutput.root '+sample+'*.output.root')
            for cat in myCategories:
                for syst in ['','_Syst_NoIso','_Syst_TightWP']:
                    if not doSys and syst != '':
                        continue
                    if 'data' in cat and syst != '':
                        continue
                    if 'data_2016' in cat:
                        continue
                    print 'Producing final output tree for %s %s'%(syst, cat)
                    os.system('hadd -f '+mergedFilesDir+cat[4:]+syst+'_2015.root '+rootFilesDir+'user.srettie.*2015'+syst+cat+'_output.root/myOutput.root')
            os.system('mv '+mergedFilesDir+'data_2015_2015.root '+mergedFilesDir+'data_2015.root')
            CreateCutFlow(mcSamples_2015, TagTrigger_2015)
            CreateCutFlow(dataSamples_2015, TagTrigger_2015)
                
        if do2016:
            for sample in mcSamples_2016:
                print 'Producing merged output for %s'%sample
                os.system('hadd -f '+sample+'myOutput.root '+sample+'*.output.root')
                AddNormToTree(sample)
            for sample in dataSamples_2016:
                print 'Producing merged output for %s'%sample
                os.system('hadd -f '+sample+'myOutput.root '+sample+'*.output.root')
            for cat in myCategories:
                for syst in ['','_Syst_NoIso','_Syst_TightWP']:
                    if not doSys and syst != '':
                        continue
                    if 'data' in cat and syst != '':
                        continue
                    if 'data_2015' in cat:
                        continue
                    print 'Producing final output tree for %s %s'%(syst, cat)
                    os.system('hadd -f '+mergedFilesDir+cat[4:]+syst+'.root '+rootFilesDir+'user.srettie.*'+syst+cat+'_output.root/myOutput.root')
            CreateCutFlow(mcSamples_2016, TagTrigger_2016)
            CreateCutFlow(dataSamples_2016, TagTrigger_2016)
        print 'Done merging output files! Please re-run macro with merging option turned off to produce plots.'
        sys.exit()

    if do2015:
        CreateValidationPlots(TagTrigger_2015, EventWeight, myCuts, lumi_2015)
        for ProbeTrig in ProbeTriggers_2015:
            for Region in ['barrel', 'endcaps']:
                if ('msonly' in ProbeTrig) and (Region == 'endcaps'):
                    continue
                for syst in sysList:
                    if not doSys and (syst != ''):
                        continue
                    CreateEfficiencyHistograms(TagTrigger_2015, ProbeTrig, Region, EventWeight, myCuts, lumi_2015, syst)

    if do2016:
        CreateValidationPlots(TagTrigger_2016, EventWeight, myCuts, lumi_2016)
        for ProbeTrig in ProbeTriggers_2016:
            for Region in ['barrel', 'endcaps']:
                if ('msonly' in ProbeTrig) and (Region == 'endcaps'):
                    continue
                for syst in sysList:
                    if not doSys and (syst != ''):
                        continue
                    CreateEfficiencyHistograms(TagTrigger_2016, ProbeTrig, Region, EventWeight, myCuts, lumi_2016, syst)

    print 'Done producing all plots!'

