setupATLAS
cd build/
asetup AnalysisTop,21.2.59,here
cmake ../athena
cmake --build ./
source ${CMTCONFIG}/setup.sh
cd ..

lsetup panda
export RUCIO_ACCOUNT=$USER
lsetup rucio
lsetup pyami
lsetup git
#voms-proxy-init -voms atlas -old # in case any issues arise
