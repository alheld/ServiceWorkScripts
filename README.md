### Rough how-to:
* install / set up your environment in ```0_AT_rel21``` for job submission
* run your jobs in ```1_submission```
    * generate the desired cuts files with the utility in ```1_submission/cuts```
    * make sure the list of samples is up to date in ```1_submission/samples```
    * if needed, generate updated PRW config files via ```1_submission/PRW``` (but now mostly taken care of automatically)
* manage and download jobs in ```2_download``` (see [ttH-offline-wrapper](https://gitlab.cern.ch/alheld/ttH-offline-wrapper/tree/master/util) project for more functionality)
* do everything else in ```3_analysis``` 
* see readme files within these directories for more information

### Tasks to be completed
- [ ] consider switching to [TEfficiency](https://root.cern.ch/doc/master/classTEfficiency.html) and/or [TDataFrame](https://root.cern.ch/doc/master/classROOT_1_1Experimental_1_1TDataFrame.html) ([slides](https://indico.cern.ch/event/684668/contributions/2857077/attachments/1588785/2513286/TDF__ATLAS_Software_Tutorial.pdf))
- [X] debug 20.7 / 21 difference:
    - [live page](https://indico.cern.ch/event/640618/)
    - variable to suppress pile-up:```jet_isTrueHS```
- [X] understand  modelling:
    - jet pT: [PMG report](https://indico.cern.ch/event/648945/contributions/2855634/attachments/1605624/2547360/atlasweek.pdf) - much better with updated CP recommendations
    - ttbar MC excess: [SUSY](https://indico.cern.ch/event/648945/contributions/2855613/attachments/1603893/2543819/susy_2017data_swiatlow.pdf)
- [X] move to rel21
    - [X] update to new AnalysisTop: ```0_AT_rel21/setup_AT.sh``` for initial setup, ```0_AT_rel21/env_setup.sh``` for environment setup after installation
    - [X] update to mc16: see ```1_submission/``` for current status
        - [X] include first set of mc16a dsids 
        - [X] add very small backgrounds (ttV, tWZ etc.) 
        - [X] include first set of mc16c dsids (currently incomplete)
        - [X] update to latest p-tags and GRLs as they become available
    - [X] compare against old results
    - [X] select and validate new tag trigger for 2017 (```HLT_xe110_pufit_L1XE55```?), see also [LowestUnprescaled twiki](https://twiki.cern.ch/twiki/bin/viewauth/Atlas/LowestUnprescaled)
- [X] harmonize plotting
- [ ] find and eliminate bugs

### Collection of relevant links / information
* new AT options for global trigger SFs: [slides](https://indico.cern.ch/event/754648/contributions/3126933/attachments/1724044/2784220/TopRecoIntro20180927.pdf)
* mc16a is for 2015-2016 data, mc16c/d is for 2017, mc16e for 2018
    * [mc16 TOPQ1 derivations](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TopDerivationMC16MergedList)
    * TOPQ1 data is skimmed ([script](https://svnweb.cern.ch/trac/atlasoff/browser/PhysicsAnalysis/DerivationFramework/DerivationFrameworkTop/trunk/python/TOPQCommonSelection.py)), mc is not skimmed in p3215, but skimmed in p3260
    * data containers and runs: [TopDerivationR21Data twiki](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TopDerivationR21Data)
    * some p-tag info can be found on the [TopDerivations twiki](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TopDerivations)
    * ttH(bb) sample list is on [ttH(bb) sharepoint](https://espace.cern.ch/atlas-tthbb/SitePages/Home.aspx?RootFolder=%2Fatlas-tthbb%2FShared%20Documents%2FNtuple%20Production)
* [AnalysisTop-21.2](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AnalysisTop21) release notes
    * [TopxAODStartGuideR21](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TopxAODStartGuideR21)
    * [AnalysisTopGit](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AnalysisTopGit)
* [TopFocusGroup mc16 status](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TopFocusGroup#MC16_sample_status)
* [GRLs](https://atlas-groupdata.web.cern.ch/atlas-groupdata/GoodRunsLists/)
* current [TopDataPreparation x-secs](https://atlas-groupdata.web.cern.ch/atlas-groupdata/dev/AnalysisTop/TopDataPreparation/XSection-MC15-13TeV.data) (now on cvmfs)
* PRW info:
    * from TopReco meeting: [slides](https://indico.cern.ch/event/678812/contributions/2780595/attachments/1555959/2446821/TopRecoIntro.pdf)
    * ASG meeting: [slides](https://indico.cern.ch/event/679306/contributions/2784652/attachments/1556491/2447942/PRWForMC16.pdf)
    * twiki: [ExtendedPileupReweighting](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/ExtendedPileupReweighting)
    * pile-up reference distribution: [slides](https://indico.cern.ch/event/679669/contributions/2792485/attachments/1560133/2455656/JamesHowarth_16_11_17_TopReco_PMGFeedback.pdf), p10
    * top pileup reweighting twiki: [TopPileupReweightingFilesMC16](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TopPileupReweightingFilesMC16)
* link to new location for [TopExamples](https://gitlab.cern.ch/atlas/athena/tree/master/PhysicsAnalysis/TopPhys/xAOD/TopExamples)
* b-tagging in rel 21: [BTaggingBenchmarksRelease21](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTaggingBenchmarksRelease21)
* muon information:
    * [MCP guidelines for rel 21](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MCPAnalysisGuidelinesMC16)
    * [Muon isolation](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/IsolationSelectionTool)
    * [more up-to-date isolation info?](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MCPAnalysisWinterMC16#PU_robust_and_PFlow_isolation_WP)
* [Muon trigger twiki](https://twiki.cern.ch/twiki/bin/view/Atlas/MuonTriggerPhysicsRecommendationsRel212017)
* [V+jets focus group](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BosonJetsFocusGroup#Lepton_eta_asymmetry_in_Sherpa_2) with comment on eta asymmetry
* SUSY analysis with similar ttbar final states + MET trigger: [INT note](https://cds.cern.ch/record/2640942)
