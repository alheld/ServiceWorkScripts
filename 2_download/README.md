### description of files for main workflow:
* ```01DownloadAndRetry.py``` is used to retry grid jobs and create lists of files that can be downloaded, sorted by event topology (currently supporting up to 5000 jobs from the last 100 days, hardcoded in script)
* ```02RucioWrapper.py``` can be executed in a directory containing a list of jobs (in ```files.txt```, produced by ```01DownloadAndRetry.py```), it will download these jobs via rucio
