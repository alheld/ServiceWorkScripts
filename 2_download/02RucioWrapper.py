#!/usr/bin/env python2

# download datasets from files created by 1_CreateSortedDownloadLists.py,
# either run interactively or hand over number of file to process when
# calling this script (execute interactively to see list of allowed
# numbers), make sure rucio + proxy work before using this
#
# Alexander Held, 2017-2018

import subprocess
import glob
import sys

# ###############################
# configure the script here
# ###############################
target_directory = 'Files/'  # parent directory from 1_CreateSortedDownloadLists.py
out_txt          = 'files.txt'      # output filename containing list of jobs
# ###############################


def list_all_input_files():
  """
  finds all input files containing datasets to be downloaded
  and lists them
  :returns: list of files containing datasets
  """
  #find all lists of files that need to be downloaded
  file_list = glob.glob(target_directory + "*/" + out_txt)
  file_list.sort()

  # list all files found
  for ifile, cur_f in enumerate(file_list):
    print ifile, "-", cur_f
    with open(cur_f) as f:
      lines = f.readlines()
      if len(lines)==0:
        print "   - no jobs found"
        continue                 # no job exists (yet)
      #for line in lines:
      #  print line.strip()
  return file_list


def select_file(file_list):
  """
  select input file containing list of datasets for download,
  either interactively or as handed over in sys.argv[-1]
  :param file_list: list of paths to files containing datasets
  :returns: position of chosen file in file_list
  """
  if sys.argv[-1] != __file__:
    try:
      choice = int(sys.argv[-1])
    except:
      print "could not identify chosen file, aborting"
      raise SystemExit

  else:
    choice = "-1"
    while choice not in [str(i) for i in range(len(file_list))]:
      choice = raw_input("# pick file to download: ")

  return choice


def download_datasets(download_input_file):
  """
  downloads datasets and prints status
  :param download_input_file: path to file containing datasets
  """
  success = True

  print "\n# downloading datasets in", download_input_file + "\n"

  with open(download_input_file) as f:
    dataset_list = f.readlines()

  cwd = ("/").join(download_input_file.split('/')[0:-1])

  for ds in dataset_list:
    print "rucio", "download", "--ndownloader 5", ds.strip('\n').strip(' ')

    p = subprocess.Popen(["rucio", "download", "--ndownloader", "5",\
                          ds.strip('\n').strip(' ')],\
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE,\
                         cwd=cwd)
    out, err = p.communicate()

    try:
      # to: total files, dl: downloaded files, fl: found locally, cd: cannot download
      to = int(out[out.find("Total files :")+13:out.find("Downloaded files")-1])
      dl = int(out[out.find("Downloaded files")+18:out.find("Files already found locally")-1])
      fl = int(out[out.find("Files already found locally")+29:out.find("Files that cannot be downloaded :")-1])
      cd = int(out[out.find("Files that cannot be downloaded :")+33:-1])
    except:
      print out

    print " - locally available:", dl+fl, "/", to
    if dl+fl != to:
      print " - missing files\n"*5
      success = False
    if cd != 0:
      print (" - cannot download: " + str(cd) + "\n")*5
      success = False
    if to == 0:
      print " - total is zero\n"*5
      success = False

  return success


if __name__ == '__main__':
  file_list = list_all_input_files()

  choice = select_file(file_list)

  if download_datasets(file_list[int(choice)]) == True:
    print "#"*30 + "\n" + "# all downloads successful" + "\n" + "#"*30
  else:
    print "#"*30 + "\n" + "# there were issues" + "\n" + "#"*30

