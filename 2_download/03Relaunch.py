import getpass
import os
import json

# ###############################
# configure the script here
# ###############################
suffix          = '29_'             # this corresponds to the suffix used in the grid submission script
username        = getpass.getuser() # manual overwrite needed if username on current machine != grid user name
UpdateJSON      = True              # update json file every time if True (recommended)
out_name        = "broken.json"     # list of broken jobs saved to this file
# ###############################


class gridjob():
  def __init__(self, j):
    self.name     = j['taskname'][0:-1]
    self.dsid     = self.name.split('.')[2]
    self.category = self.name.split('-')[-1]
    self.status   = j['superstatus']
    self.task_id  = j['jeditaskid']    # jedi task id
    self.n_failed = j['dsinfo']['nfilesfailed']  # amount of failed files


def get_bigpanda_link(username, suffix, status="", json=True):
  """
  status: 'done' (safe to download), 'finished' (retry job), 'broken' (find out what is wrong)
  if status is "", no status requirement is made
  """
  link = 'https://bigpanda.cern.ch/tasks/?taskname=user.'
  link += username + '*' + suffix + '*'
  link += "&display_limit=5000&limit=5000&days=100"
  if status != "":
    link += '&superstatus=' + status
  if json:
    link += '&json'
  return link


def download_json(username, suffix):
  """
  download json file containing job details from bigpanda
  returns json file name
  """
  json_file_name = "jobs.json"
  link = get_bigpanda_link(username, suffix, status="", json=True)
  download_command = "wget --no-check-certificate -O " + json_file_name +\
                     " \"" + link + "\""
  print 'executing:', download_command
  os.system(download_command)
  return json_file_name


def list_jobs(job_list, status, save=False):
  prod_list = ["29_med_noIso_v0", "29_med_grad_v0", "29_med_tight_v1", "29_hpt_noIso_v0"]

  print "\nlisting jobs with status", status
  for p in prod_list:
    print p
    for j in job_list:
      if p not in j.name: continue
      if j.status != status: continue
      if "med_tight_v0" in j.name: continue # skip buggy tight definition
      print " ", j.category, j.dsid, j.name
    print
  print

  # save to json, sorted by campaign
  # only saving dsid and tags, separated by "."
  if save==True:
    with open(out_name, "w+") as f:
      f.write("{\n")
      for ip, p in enumerate(prod_list):
        comma_skip = True # needed to skip first comma in list, ugly hack
	f.write("  \"" + p + "\":[\n")
	for ij, j in enumerate(job_list):
          if p not in j.name: continue
          if j.status != status: continue
          if "med_tight_v0" in j.name: continue # skip buggy tight definition
	  if comma_skip:
	    f.write("    \"" + ".".join(list(j.name.split(".")[islice] for islice in [2,5])) + "\"")
	    comma_skip = False
	  else:
	    f.write(",\n    \"" + ".".join(list(j.name.split(".")[islice] for islice in [2,5])) + "\"")
        f.write("\n  ]")
	if ip != len(prod_list)-1:
	  f.write(",\n")  # need the comma
	else:
	  f.write("\n")  # no need for comma at the end of block
      f.write("}\n")

def create_job_list(overwrite=True):
  """
  create a list of gridjob() instances
  if overwrite==False, do not re-download the json file
  """
  if overwrite:
    fname = download_json(username, suffix) # get json object
  else:
    fname = "jobs.json"

  with open(fname) as f:
    job_info = json.load(f)
  job_list = []
  for j in job_info:
    job_list.append(gridjob(j)) # append list by instances of gridjob objects

  for j in job_list:
    print '  - found:', j.category, j.dsid, j.status   # print list of jobs found

  print " # total:    ", len(job_list)
  print " # done:     ", len([j for j in job_list if (j.status == "done")])
  print " # finished: ", len([j for j in job_list if (j.status == "finished")])
  print " # failed:   ", len([j for j in job_list if (j.status == "failed")])
  print " # broken:   ", len([j for j in job_list if (j.status == "broken")])
  print " # other:    ", len([j for j in job_list if ((j.status != "done") and (j.status != "finished")\
                                                     and (j.status == "failed") and (j.status == "broken"))])

  list_jobs(job_list, "finished")
  list_jobs(job_list, "failed")
  list_jobs(job_list, "broken", save=True)

  return job_list


def sync_jobs():
  os.system('pbook -c \"sync()\"')


if __name__ == '__main__':
  print "# to use this script:"
  print "# make sure to setup rucio and panda (lsetup)"
  print "# and verify that a valid proxy is configured\n"

  sync_jobs()

  job_list = create_job_list(overwrite=UpdateJSON) # obtain list of grid jobs
