import sys
import ROOT

debug = False

errorTest = False

out_dir = '/analysis/plots/'#'/Users/Sebastien/Dropbox/Courses/ServiceWork/PaperPlots/'

colors = [
    ROOT.kBlack,
    ROOT.kRed,
    ROOT.kBlue,
    ROOT.kGreen+1,
]

marker_styles_data = [24,25,26,32]#[20,21,22,23]
marker_styles_mc = [24,25,26,32]

filenames = [
    '/analysis/MuTPaper/L1/check_trigeff_all.root',# L1
    '/analysis/MuTPaper/L1/RatePlots.root',# L1
    '/analysis/MuTPaper/JPsimumu/JPsimumu.root',# JPsimumu
    '/analysis/MuTPaper/Zmumu/Zmumu.root',# Zmumu
    '/analysis/MuTPaper/HighPt/ttbar.root',# ttbar
    '/analysis/MuTPaper/HighPt/wjets.root',# wjets
    '/analysis/MuTPaper/HI/HI.root',# HI
]

# Use ATLAS Style
ROOT.gROOT.SetBatch(1)
ROOT.gROOT.LoadMacro("/analysis/atlasrootstyle/AtlasStyle.C+")
ROOT.gROOT.LoadMacro("/analysis/atlasrootstyle/AtlasLabels.C")
ROOT.gROOT.LoadMacro("/analysis/atlasrootstyle/AtlasUtils.C")
ROOT.SetAtlasStyle()

'''
From Robin:

For the 1D plots (TGraphAsymmErrors):
Naming scheme: plot_prepTP_WP_region_Match_Trigger_probe_variable_1d_eff_key
Example: plot_prepTP_Medium_barrel_Match_HLT_mu20_iloose_L1MU15_probe_pt_1d_eff_ratio

For the 2D plots (TH2): 
Naming scheme: plot_prepTP_WP_region_Match_Trigger_probe_region_eta_phi_data_mc_ratio_2d 
Example: plot_prepTP_Medium_barrel_Match_HLT_mu20_iloose_L1MU15_probe_barrel_eta_phi_data_mc_ratio_2d

Possible thresholds: 20, 26, 50
Possible years: 2015, 2016, 2017, 2018
Possible WP: Medium
Possible regions: barrel, endcap
Possible triggers: HLT_mu20_iloose_L1MU15, HLT_mu26_ivarmedium, HLT_mu50
Possible variables: pt, eta, NRecVtx, phi_barrel, phi_endcap
Possible keys: 0 (for data efficiencies), 1 (for MC efficiencies), ratio (for scale factors)
'''

# Master list of all paper plots we want
# Make sure data always comes before MC in list for efficiency plots!
# TODO -  ADD ACTUAL NAMES
# ex: 'plot_prepTP_Medium_barrel_Match_HLT_mu20_iloose_L1MU15_probe_pt_1d_eff_ratio'
paper_plots = {
    # 'L1_effVsEta_newRPC' : ['noNewRPC', 'withNewRPC'],

    ###'L1_effVsPt_BA' : ['eff_pt2015_rpc_mu20', 'eff_pt2016_rpc_mu20', 'eff_pt2017_rpc_mu20', 'eff_pt2018_rpc_mu20'],

    ###'L1_effVsPt_EC' : ['eff_pt2015_tgc_mu20', 'eff_pt2016_tgc_mu20', 'eff_pt2017_tgc_mu20', 'eff_pt2018_tgc_mu20'],

    ###'L1_effVsPhi_BA' : ['eff_phi2015_rpc_mu20', 'eff_phi2016_rpc_mu20', 'eff_phi2017_rpc_mu20', 'eff_phi2018_rpc_mu20'],

    ###'L1_effVsPhi_EC' : ['eff_phi2015_tgc_mu20', 'eff_phi2016_tgc_mu20', 'eff_phi2017_tgc_mu20', 'eff_phi2018_tgc_mu20'],

    # 'L1_rateVsEta_newEIFI' : ['noNewEIFI', 'withNewEIFI'],

    # 'L1_rateVsEta_newTile' : ['noNewTile', 'withNewTile'],

    # 'L1_rateVsLumi_newTile' : ['noNewTile', 'withNewTile'],

    # 'L1_effVsPt_TGCStations' : ['MC', 'data_2st', 'data_3st'],

    ###'L1_rateVsLumi_CW' : ['L1_MU6_before', 'L1_MU6_after'],

    # 'L1_effVsPt_FastReco' : [], # PROBABLY GOING TO DROP FROM PAPER

    # 'L1_rateVsLumi' : ['L1_MU20', 'L1_3MU4', 'L1MU6', 'L1_2MU10'],

    # 'HLT_rateVsLumi' : ['HLT_mu26_ivarmedium', 'HLT_mu50', 'HLT_3mu6', 'HLT_2mu14'],

    # 'timing_fast' : ['nov15', 'sep16', 'jun17', 'may18'],

    # 'timing_precision' : ['nov15', 'sep16', 'jun17', 'may18'],

    # 'LowPt_effVsPt_BA' : [
    #     'efficiency_barrel_HLT_mu4_nomucomb_data',
    #     'efficiency_barrel_HLT_mu4_nomucomb_MC',
    #     'efficiency_barrel_HLT_mu6_nomucomb_data',
    #     'efficiency_barrel_HLT_mu6_nomucomb_MC',
    # ],
    
    # 'LowPt_effVsPt_EC' : [
    #     'efficiency_endcap_HLT_mu4_nomucomb_data',
    #     'efficiency_endcap_HLT_mu4_nomucomb_MC',
    #     'efficiency_endcap_HLT_mu6_nomucomb_data',
    #     'efficiency_endcap_HLT_mu6_nomucomb_MC',
    # ],
    
    'MidPt_effVsPt_BA' : [
        'plot_prepTP_Medium_barrel_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_probe_pt_1d_eff0_2016',
        'plot_prepTP_Medium_barrel_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_probe_pt_1d_eff1_2016',
        'plot_prepTP_Medium_barrel_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_probe_pt_1d_eff0_2017',
        'plot_prepTP_Medium_barrel_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_probe_pt_1d_eff1_2017',
        'plot_prepTP_Medium_barrel_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_probe_pt_1d_eff0_2018',
        'plot_prepTP_Medium_barrel_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_probe_pt_1d_eff1_2018'
    ],
    
    'MidPt_effVsPt_EC' : [
        'plot_prepTP_Medium_endcap_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_probe_pt_1d_eff0_2016',
        'plot_prepTP_Medium_endcap_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_probe_pt_1d_eff1_2016',
        'plot_prepTP_Medium_endcap_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_probe_pt_1d_eff0_2017',
        'plot_prepTP_Medium_endcap_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_probe_pt_1d_eff1_2017',
        'plot_prepTP_Medium_endcap_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_probe_pt_1d_eff0_2018',
        'plot_prepTP_Medium_endcap_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_probe_pt_1d_eff1_2018'
    ],

    # 'MidPt_effVsNRecVtx_BA' : [
    #     'plot_prepTP_Medium_barrel_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_NRecVtx_1d_eff0_2016',
    #     'plot_prepTP_Medium_barrel_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_NRecVtx_1d_eff1_2016',
    #     'plot_prepTP_Medium_barrel_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_NRecVtx_1d_eff0_2017',
    #     'plot_prepTP_Medium_barrel_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_NRecVtx_1d_eff1_2017',
    #     'plot_prepTP_Medium_barrel_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_NRecVtx_1d_eff0_2018',
    #     'plot_prepTP_Medium_barrel_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_NRecVtx_1d_eff1_2018'
    # ],
    
    # 'MidPt_effVsNRecVtx_EC' : [
    #     'plot_prepTP_Medium_endcap_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_NRecVtx_1d_eff0_2016',
    #     'plot_prepTP_Medium_endcap_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_NRecVtx_1d_eff1_2016',
    #     'plot_prepTP_Medium_endcap_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_NRecVtx_1d_eff0_2017',
    #     'plot_prepTP_Medium_endcap_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_NRecVtx_1d_eff1_2017',
    #     'plot_prepTP_Medium_endcap_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_NRecVtx_1d_eff0_2018',
    #     'plot_prepTP_Medium_endcap_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_NRecVtx_1d_eff1_2018'
    # ],

    # 'MidPt_effVsMu_BA' : [
    #     'plot_prepTP_Medium_barrel_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_average_mu_1d_eff0_2016',
    #     'plot_prepTP_Medium_barrel_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_average_mu_1d_eff1_2016',
    #     'plot_prepTP_Medium_barrel_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_average_mu_1d_eff0_2017',
    #     'plot_prepTP_Medium_barrel_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_average_mu_1d_eff1_2017',
    #     'plot_prepTP_Medium_barrel_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_average_mu_1d_eff0_2018',
    #     'plot_prepTP_Medium_barrel_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_average_mu_1d_eff1_2018'
    # ],
    
    # 'MidPt_effVsMu_EC' : [
    #     'plot_prepTP_Medium_endcap_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_average_mu_1d_eff0_2016',
    #     'plot_prepTP_Medium_endcap_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_average_mu_1d_eff1_2016',
    #     'plot_prepTP_Medium_endcap_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_average_mu_1d_eff0_2017',
    #     'plot_prepTP_Medium_endcap_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_average_mu_1d_eff1_2017',
    #     'plot_prepTP_Medium_endcap_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_average_mu_1d_eff0_2018',
    #     'plot_prepTP_Medium_endcap_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_average_mu_1d_eff1_2018'
    # ],

    'MidPt_effVsMu_BA_isoOnly' : [
        'plot_prepTP_Medium_barrel_Match_HLT_mu26_ivarmedium_average_mu_1d_eff0_2016',
        'plot_prepTP_Medium_barrel_Match_HLT_mu26_ivarmedium_average_mu_1d_eff1_2016',
        'plot_prepTP_Medium_barrel_Match_HLT_mu26_ivarmedium_average_mu_1d_eff0_2017',
        'plot_prepTP_Medium_barrel_Match_HLT_mu26_ivarmedium_average_mu_1d_eff1_2017',
        'plot_prepTP_Medium_barrel_Match_HLT_mu26_ivarmedium_average_mu_1d_eff0_2018',
        'plot_prepTP_Medium_barrel_Match_HLT_mu26_ivarmedium_average_mu_1d_eff1_2018'
    ],
    
    'MidPt_effVsMu_EC_isoOnly' : [
        'plot_prepTP_Medium_endcap_Match_HLT_mu26_ivarmedium_average_mu_1d_eff0_2016',
        'plot_prepTP_Medium_endcap_Match_HLT_mu26_ivarmedium_average_mu_1d_eff1_2016',
        'plot_prepTP_Medium_endcap_Match_HLT_mu26_ivarmedium_average_mu_1d_eff0_2017',
        'plot_prepTP_Medium_endcap_Match_HLT_mu26_ivarmedium_average_mu_1d_eff1_2017',
        'plot_prepTP_Medium_endcap_Match_HLT_mu26_ivarmedium_average_mu_1d_eff0_2018',
        'plot_prepTP_Medium_endcap_Match_HLT_mu26_ivarmedium_average_mu_1d_eff1_2018'
    ],

    'MidPt_ratioVsEtaPhi_BA' : ['plot_prepTP_Medium_barrel_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_probe_barrel_eta_phi_data_mc_ratio_2d_2017'],

    'MidPt_ratioVsEtaPhi_EC' : ['plot_prepTP_Medium_endcap_Match_HLT_mu26_ivarmedium_OR_HLT_mu50_probe_endcap_eta_phi_data_mc_ratio_2d_2017'],

    # 'MidPt_effVsPt_BA_Dimuon' : ['HLT_mu10_data15', 'HLT_mu10_MC15', 'HLT_mu10_data16', 'HLT_mu10_MC16'],

    # 'MidPt_effVsPt_EC_Dimuon' : ['HLT_mu10_data15', 'HLT_mu10_MC15', 'HLT_mu10_data16', 'HLT_mu10_MC16'],

    'HighPt_effVsPt_BA_WJets' : [
        'HLT_mu26_ivarmedium_OR_HLT_mu50.barrel.2016.wjets.medium_wp.no_iso.nominal.root.eff_data',
        'HLT_mu26_ivarmedium_OR_HLT_mu50.barrel.2016.wjets.medium_wp.no_iso.nominal.root.eff_mc',
        'HLT_mu26_ivarmedium_OR_HLT_mu50.barrel.2017.wjets.medium_wp.no_iso.nominal.root.eff_data',
        'HLT_mu26_ivarmedium_OR_HLT_mu50.barrel.2017.wjets.medium_wp.no_iso.nominal.root.eff_mc',
        'HLT_mu26_ivarmedium_OR_HLT_mu50.barrel.2018.wjets.medium_wp.no_iso.nominal.root.eff_data',
        'HLT_mu26_ivarmedium_OR_HLT_mu50.barrel.2018.wjets.medium_wp.no_iso.nominal.root.eff_mc',
    ],
    
    'HighPt_effVsPt_EC_WJets' : [
        'HLT_mu26_ivarmedium_OR_HLT_mu50.endcaps.2016.wjets.medium_wp.no_iso.nominal.root.eff_data',
        'HLT_mu26_ivarmedium_OR_HLT_mu50.endcaps.2016.wjets.medium_wp.no_iso.nominal.root.eff_mc',
        'HLT_mu26_ivarmedium_OR_HLT_mu50.endcaps.2017.wjets.medium_wp.no_iso.nominal.root.eff_data',
        'HLT_mu26_ivarmedium_OR_HLT_mu50.endcaps.2017.wjets.medium_wp.no_iso.nominal.root.eff_mc',
        'HLT_mu26_ivarmedium_OR_HLT_mu50.endcaps.2018.wjets.medium_wp.no_iso.nominal.root.eff_data',
        'HLT_mu26_ivarmedium_OR_HLT_mu50.endcaps.2018.wjets.medium_wp.no_iso.nominal.root.eff_mc',
    ],
    
    'HighPt_effVsPt_BA_ttbar' : [
        'HLT_mu26_ivarmedium_OR_HLT_mu50.barrel.2016.ttbar.medium_wp.no_iso.nominal.root.eff_data',
        'HLT_mu26_ivarmedium_OR_HLT_mu50.barrel.2016.ttbar.medium_wp.no_iso.nominal.root.eff_mc',
        'HLT_mu26_ivarmedium_OR_HLT_mu50.barrel.2017.ttbar.medium_wp.no_iso.nominal.root.eff_data',
        'HLT_mu26_ivarmedium_OR_HLT_mu50.barrel.2017.ttbar.medium_wp.no_iso.nominal.root.eff_mc',
        'HLT_mu26_ivarmedium_OR_HLT_mu50.barrel.2018.ttbar.medium_wp.no_iso.nominal.root.eff_data',
        'HLT_mu26_ivarmedium_OR_HLT_mu50.barrel.2018.ttbar.medium_wp.no_iso.nominal.root.eff_mc',
    ],
    
    'HighPt_effVsPt_EC_ttbar' : [
        'HLT_mu26_ivarmedium_OR_HLT_mu50.endcaps.2016.ttbar.medium_wp.no_iso.nominal.root.eff_data',
        'HLT_mu26_ivarmedium_OR_HLT_mu50.endcaps.2016.ttbar.medium_wp.no_iso.nominal.root.eff_mc',
        'HLT_mu26_ivarmedium_OR_HLT_mu50.endcaps.2017.ttbar.medium_wp.no_iso.nominal.root.eff_data',
        'HLT_mu26_ivarmedium_OR_HLT_mu50.endcaps.2017.ttbar.medium_wp.no_iso.nominal.root.eff_mc',
        'HLT_mu26_ivarmedium_OR_HLT_mu50.endcaps.2018.ttbar.medium_wp.no_iso.nominal.root.eff_data',
        'HLT_mu26_ivarmedium_OR_HLT_mu50.endcaps.2018.ttbar.medium_wp.no_iso.nominal.root.eff_mc',
    ],
    
    # 'HILow_effVsPt_BA' : ['Eff_pt_barrel_0_LowpT_periodC'],

    # 'HILow_effVsPt_EC' : ['Eff_pt_endcap_0_LowpT_periodC'],

    # 'HILow_effVsEta' : ['Eff_eta_0_LowpT_periodC'],

    # 'HILow_effVsPhi_BA' : ['Eff_phi_barrel_0_LowpT_periodC'],

    # 'HILow_effVsPhi_EC' : ['Eff_phi_endcap_0_LowpT_periodC'],

    # HLT_mu15 = 0, HLT_mu15_L1MU10 = 1, HLT_mu15_L1MU6 = 2
    # 'HIHigh_effVsPt_BA' : [
    #     'Eff_pt_barrel_0_HighpT_periodC',
    #     'Eff_pt_barrel_1_HighpT_periodC',
    #     'Eff_pt_barrel_2_HighpT_periodC'
    # ],

    # 'HIHigh_effVsPt_EC' : [
    #     'Eff_pt_endcap_0_HighpT_periodC',
    #     'Eff_pt_endcap_1_HighpT_periodC',
    #     'Eff_pt_endcap_2_HighpT_periodC'
    # ],
    
    # 'HIHigh_effVsEta' : [
    #     'Eff_eta_0_HighpT_periodC',
    #     'Eff_eta_1_HighpT_periodC',
    #     'Eff_eta_2_HighpT_periodC'
    # ],
    
    # 'HIHigh_effVsPhi_BA' : [
    #     'Eff_phi_barrel_0_HighpT_periodC',
    #     'Eff_phi_barrel_1_HighpT_periodC',
    #     'Eff_phi_barrel_2_HighpT_periodC'
    # ],
    
    # 'HIHigh_effVsPhi_EC' : [
    #     'Eff_phi_endcap_0_HighpT_periodC',
    #     'Eff_phi_endcap_1_HighpT_periodC',
    #     'Eff_phi_endcap_2_HighpT_periodC'
    # ],
    
    # 'ResPtVsPt' : ['CB_barrel', 'CB_endcaps', 'MSONLY_barrel', 'MSONLY_endcaps'],
    
    # 'ResEtaVsPt' : ['CB_barrel', 'CB_endcaps', 'MSONLY_barrel', 'MSONLY_endcaps'],

    # 'ResPhiVsPt' : ['CB_barrel', 'CB_endcaps', 'MSONLY_barrel', 'MSONLY_endcaps'],
}
    

# Master list of all histogram names we need
needed_hists = []
for plot_key, hist_list in paper_plots.items():
    needed_hists += hist_list


def get_histograms(filenames):
    if debug:
        print('Getting histograms...')
    
    hists = {}

    for f_name in filenames:
        if debug:
            print('Opening file: ' + f_name)
        # Get file
        f = ROOT.TFile.Open(f_name)

        # Get histograms
        for k in f.GetListOfKeys():
            h_name = k.GetName()
            
            #if debug:
            #    print('Found histogram: ' + h_name)

            # Only keep relevant histograms
            is_needed = h_name in needed_hists

            if 'syst_' in h_name:
                is_needed = True
                
            if is_needed:
                hists[h_name] = f.Get(h_name).Clone()
                if 'TH2F' in hists[h_name].IsA().GetName() or 'TProfile' in hists[h_name].IsA().GetName():
                    hists[h_name].SetDirectory(0)
                if debug:
                    print('GOOD HIST: ' + h_name)
                    print('Type: ' + hists[h_name].IsA().GetName())    
                
        # Close file
        f.Close()

    # Check that all required histograms are found
    for h_name in needed_hists:
        if h_name not in hists:
            print('ERROR!! HISTOGRAM NOT FOUND: ' + h_name)
            return None

    # Return all relevant histograms
    return hists

def get_legend_title(h_name):
    
    title = 'UNKNOWN'

    # Zmumu
    if 'eff0' in h_name:
        title = 'Data '
        title += h_name[-4:]
    if 'eff1' in h_name:
        title = 'MC '
        title += h_name[-4:]

    # High-pT
    if 'eff_data' in h_name:
        title = 'Data '
        title += h_name.split('.')[2]
    if 'eff_mc' in h_name:
        title = 'MC '
        title += h_name.split('.')[2]

    # Low-pT
    if 'HLT_mu4_nomucomb_MC' in h_name:
        title = 'HLT_mu4_nomucomb MC'
    if 'HLT_mu4_nomucomb_data' in h_name:
        title = 'HLT_mu4_nomucomb Data'
    if 'HLT_mu6_nomucomb_MC' in h_name:
        title = 'HLT_mu6_nomucomb MC'
    if 'HLT_mu6_nomucomb_data' in h_name:
        title = 'HLT_mu6_nomucomb Data'

    # HI
    if 'LowpT_periodC' in h_name:
        title = 'HLT_mu4 Data'
    # HLT_mu15 = 0, HLT_mu15_L1MU10 = 1, HLT_mu15_L1MU6 = 2
    if '0_HighpT_periodC' in h_name:
        title = 'HLT_mu15 Data'
    if '1_HighpT_periodC' in h_name:
        title = 'HLT_mu15_L1MU10 Data'
    if '2_HighpT_periodC' in h_name:
        title = 'HLT_mu15_L1MU6 Data'

    # L1
    if '_mu20' in h_name:
        title = 'L1_MU20'
        if '2015' in h_name:
            title += ' Data 2015'
        if '2016' in h_name:
            title += ' Data 2016'
        if '2017' in h_name:
            title += ' Data 2017'
        if '2018' in h_name:
            title += ' Data 2018'

    if 'L1_MU6_before' in h_name:
        title = 'L1_MU6 Data before CW optimization'
    if 'L1_MU6_after' in h_name:
        title = 'L1_MU6 Data after CW optimization'
        
    return title

def get_lumi(plot_key):

    # HI plots use period C: 107.8/nb
    isHI = 'HI' in plot_key
    if isHI:
        return '107.8 nb^{-1}'

    isLowPt = 'LowPt_' in plot_key
    if isLowPt:
        return 'Data 2017'

    if 'L1_eff' in plot_key:
        return '139.0 fb^{-1}'

    if 'VsEtaPhi' in plot_key:
        # 2D plots only use 2017 data
        return '44.3 fb^{-1}'
    # 2016: 33.0/fb
    # 2017: 44.3/fb
    # 2018: 58.4/fb    
    # For now, just use sum of all 3 years in range
    return '135.7 fb^{-1}'

def draw_labels(plot_key):
    # Options
    vsMRecVtx = '_effVsNRecVtx' in plot_key
    vsMu = '_effVsMu' in plot_key
    isHI = 'HI' in plot_key
    isBarrel = '_BA' in plot_key
    isEndcap = '_EC' in plot_key
    is2D = 'VsEtaPhi' in plot_key

    # Draw ATLAS label and luminosity label
    # lx1 = 0.7
    # ly1 = 0.6
    # lx2 = 0.9
    # ly2 = 0.9
    # leg = ROOT.TLegend(lx1, ly1, lx2, ly2)
    # leg.SetTextSize(0.07)
    
    labelx = 0.2#0.25
    labely = 0.85#0.85
    offset = 0.09#06
    if is2D and isEndcap:
        labelx = 0.34
        offset = 0.06

    if is2D and isBarrel:
        labelx = 0.15
        labely = 0.9
        
    # Extra labels for selection
    extraLabel = ''
    jpsimm = ['LowPt_eff', 'HILow_eff']
    zmm = ['L1_eff', 'MidPt_eff', 'HIHigh_eff']
    wjets = ['WJets']
    ttbar = ['ttbar']
    if any(x in plot_key for x in zmm):
        extraLabel += 'pp data, Z #rightarrow #mu#mu'
    if any(x in plot_key for x in jpsimm):
        extraLabel += 'pp data, J/#psi #rightarrow #mu#mu'
    if 'WJets' in plot_key:
        extraLabel += 'pp data, W+Jets'
    if 'ttbar' in plot_key:
        extraLabel += 'pp data, t#bar{t}'        
    if isBarrel:
        if extraLabel == '':
            extraLabel += '|#eta| < 1.05'
        else:
            extraLabel += ', |#eta| < 1.05'
    elif isEndcap:
        if extraLabel == '':
            extraLabel += '1.05 < |#eta| < 2.5'
        else:
            extraLabel += ', 1.05 < |#eta| < 2.5'


    # TODO - automatically get luminosity for each plot...
    lumi = get_lumi(plot_key)

    
    if is2D and isBarrel:
        ROOT.ATLASLabel(labelx, labely, ', #sqrt{s} = 13 TeV, '+lumi+', '+extraLabel)
        return
            
    ROOT.ATLASLabel(labelx, labely, '')
    
    
    if isHI:
        # Make sure to change COM energy for HI
        ROOT.myText(labelx, labely-offset, 1, '#sqrt{s_{NN}} = 8.16 TeV, ' + lumi)#%.1f fb^{-1}'%lumi)
    else:
        ROOT.myText(labelx, labely-offset, 1, '#sqrt{s} = 13 TeV, ' + lumi)#%.1f fb^{-1}'%lumi)

            
    if extraLabel != '' and is2D:
        ROOT.myText(labelx, labely-2*offset, 1, extraLabel)
    elif extraLabel != '':
        ROOT.myText(labelx, labely-2*offset, 1, extraLabel)
        

def produce_plot(hists, histnames, plot_key = ''):
    if debug:
        print('Producing plot: ' + plot_key)

    isRateVsLumi = '_rateVsLumi' in plot_key
    isEtaPhi = 'VsEtaPhi' in plot_key
    
    cnv = ROOT.TCanvas()
    cnv.cd()

    # For 2D plots, simply draw
    if isEtaPhi:
        cnv = ROOT.TCanvas('cnv','cnv',800,500)
        cnv.cd()
        cnv.SetRightMargin(0.14)
        cnv.SetLeftMargin(0.1)
        hists[histnames[0]].SetTitle(';Muon #eta;Muon #phi')
        hists[histnames[0]].GetXaxis().SetTitleOffset(0)
        hists[histnames[0]].GetYaxis().SetTitleOffset(0)
        hists[histnames[0]].GetZaxis().SetTitleOffset(0.8)
        ROOT.gStyle.SetPaintTextFormat('.2f')
        if 'MidPt_ratioVsEtaPhi_EC' in plot_key:
            hists[histnames[0]].SetMarkerSize(1.1)
            hists[histnames[0]].Draw('colz,text45')
        else:
            hists[histnames[0]].SetMarkerSize(1.75)
            hists[histnames[0]].Draw('colz,text')

        # Change palette placement
        pal = hists[histnames[0]].GetListOfFunctions().FindObject('palette')
        pal.SetX1NDC(0.87);
        pal.SetX2NDC(0.91);
        pal.SetY1NDC(0.16);
        pal.SetY2NDC(0.95);

        if plot_key == 'MidPt_ratioVsEtaPhi_BA':
            cnv.SetTopMargin(0.15)
            pal.SetY2NDC(0.85);

        draw_labels(plot_key)

        
    # For 1D plots
    else:

        # Legend
        lx1 = 0.4
        ly1 = 0.2
        lx2 = 0.8
        ly2 = 0.4
        leg = ROOT.TLegend(lx1, ly1, lx2, ly2)
        for i, h_name in enumerate(histnames):
            if debug:
                print('Drawing: ' + h_name)

            hists[h_name].SetLineColor(colors[i])
            hists[h_name].SetMarkerColor(colors[i])

            # Set axis titles
            if isRateVsLumi:
                hists[h_name].GetYaxis().SetTitle('Trigger Rate [kHz]')
            else:
                hists[h_name].GetYaxis().SetTitle('MISSING TITLE')

            # Add legend entry
            leg.AddEntry(hists[h_name], get_legend_title(h_name), 'lep')
        
            if i == 0:
                #hists[h_name].SetMinimum(0)
                #hists[h_name].SetMaximum(1.4)
                hists[h_name].Draw()
            else:
                hists[h_name].Draw('psame')


        leg.Draw('same')

        draw_labels(plot_key)
            
    # Save output
    cnv.SaveAs(out_dir + plot_key + '.pdf')
    cnv.Close()
    # Add linear fits for lumi plots
    #if isRateVsLumi:

def isDataMC(plot_key):
    if 'LowPt_effVsPt' in plot_key:
        return True
    if 'MidPt_effVsPt' in plot_key:
        return True
    if 'MidPt_effVsNRecVtx' in plot_key:
        return True
    if 'MidPt_effVsNRecVtx' in plot_key:
        return True
    if 'HighPt_effVsPt' in plot_key:
        return True
    if 'MidPt_effVsMu' in plot_key:
        return True
    return False

    
def isMC(h_name):
    if '_mc' in h_name:
        return True
    if '_MC' in h_name:
        return True
    if '_eff1_' in h_name:
        return True
    return False

def divide_tgraphs(tg1, tg2, name=''):
    npts = tg1.GetN()

    if tg1.GetN() != tg2.GetN():
        print('WARNING!! DIVING TWO GRAPHS WITH NON-IDENTICAL NUMBER OF POINTS!!')
        print(name)
    rp = ROOT.TGraphAsymmErrors(npts)

    for pt in range(npts):
        x_data, x_mc, y_data, y_mc = ROOT.Double(0), ROOT.Double(0), ROOT.Double(0), ROOT.Double(0)
        tg1.GetPoint(pt, x_data, y_data)
        tg2.GetPoint(pt, x_mc, y_mc)
        exl_data, exh_data, eyl_data, eyh_data = tg1.GetErrorXlow(pt), tg1.GetErrorXhigh(pt), tg1.GetErrorYlow(pt), tg1.GetErrorYhigh(pt)
        exl_mc, exh_mc, eyl_mc, eyh_mc = tg2.GetErrorXlow(pt), tg2.GetErrorXhigh(pt), tg2.GetErrorYlow(pt), tg2.GetErrorYhigh(pt)
        ey_data, ey_mc = tg1.GetErrorY(pt), tg2.GetErrorY(pt)

        try:
            ratio = y_data/y_mc
            asym_err = ratio * ( (ey_data / y_data)**2 + (ey_mc / y_mc)**2 )**0.5
        except:
            print('data pts: '+str(tg1.GetN())+', mc pts: '+str(tg2.GetN()))
            print(name+' exception for: y_data = '+str(y_data)+' and y_mc = '+str(y_mc))
            ratio = -999
            asym_err = 0.0

        rp.SetPoint(pt, x_data, ratio)
        rp.SetPointError(pt, exl_data, exh_data, asym_err, asym_err)

    return rp

def get_plot_extrema(plot_key):
    if 'HighPt_effVsPt' in plot_key or 'MidPt_effVsPt' in plot_key:
        if '_BA' in plot_key:
            return 0.4, 1.0
        if '_EC' in plot_key:
            return 0.6, 1.2
    if 'MidPt_effVsMu' in plot_key:
        if '_BA' in plot_key:
            return 0.54, 0.8
        if '_EC' in plot_key:
            return 0.75, 1.0
    if 'HIHigh' in plot_key:
        if 'effVsEta' in plot_key:
            return 0.2, 1.5
        if 'effVsPhi_BA' in plot_key:
            return 0.2, 1.5
        if 'effVsPhi_EC' in plot_key:
            return 0.7, 1.2
    if 'HILow' in plot_key:
        if 'effVsPt' not in plot_key:
            return 0.4, 1.4
        else:
            return 0.0, 1.5
    if 'L1_' in plot_key:
        if 'effVsPhi_EC' in plot_key:
            return 0.7, 1.2

    if debug:
        print('Using default extrema for: '+plot_key)
        
    return 0.0, 1.4

def plot_efficiency_ratio(hists, histnames, plot_key = ''):
    if debug:
        print('Producing efficiency comparison with ratio for plot_key: '+plot_key)
        print('Using histograms:')
        for h in histnames:
            print(h)
 
    # Options
    vsPt = '_effVsPt' in plot_key
    vsEta = '_effVsEta' in plot_key
    vsPhi = '_effVsPhi' in plot_key
    vsMRecVtx = '_efVsNRecVtx' in plot_key
    vsMu = '_effVsMu' in plot_key
    isHI = 'HI' in plot_key
    isBarrel = '_BA' in plot_key
    isEndcap = '_EC' in plot_key

    # Legend
    lx1 = 0.7
    ly1 = 0.6
    lx2 = 0.9
    ly2 = 0.9
    leg = ROOT.TLegend(lx1, ly1, lx2, ly2)
    leg.SetTextSize(0.07)
                    
    cnv_single = ROOT.TCanvas('cnv_single')
    cnv_single.cd()
    
    cnv = ROOT.TCanvas()
    cnv.cd()

    ratio_frac = 0.3
    x_title_size = 0.062
    y_title_size = 0.062
    x_title_offset = 2.2
    y_title_offset = 0.92
    y_label_offset = 0.011
    axis_label_size = 0.06
    
    p2 = ROOT.TPad('p2','p2',0.,0.,1.,ratio_frac)
    p2.Draw()
    p2.SetTopMargin(0.05)
    p2.SetBottomMargin(ratio_frac+0.03)

    p1 = ROOT.TPad('p1','p1',0.,ratio_frac,1.,1.)
    p1.Draw()
    p1.SetBottomMargin(0.03)
    p1.cd()

    names_data = []
    names_mc = []
    rps = []

    # Get and sort histogram names
    for n in histnames:
        if isMC(n):
            names_mc.append(n)
        else:
            names_data.append(n)

    if debug:
        print('Data: ' + str(names_data))
        print('MC: ' + str(names_mc))

    # Prepare histograms
    for i, h_name in enumerate(names_data):
        # Fix MeV --> GeV for certain plots
        if plot_key == 'MidPt_effVsPt_BA' or plot_key == 'MidPt_effVsPt_EC':
            # data
            for pt in range(hists[h_name].GetN()):
                xval, yval = ROOT.Double(0), ROOT.Double(0)
                hists[h_name].GetPoint(pt, xval, yval)

                exl, exh, eyl, eyh = hists[h_name].GetErrorXlow(pt), hists[h_name].GetErrorXhigh(pt), hists[h_name].GetErrorYlow(pt), hists[h_name].GetErrorYhigh(pt)
                
                hists[h_name].SetPoint(pt, xval/1000., yval)
                hists[h_name].SetPointError(pt, exl/1000., exh/1000., eyl, eyh)
            # mc
            if len(names_mc) > 0:
                for pt in range(hists[names_mc[i]].GetN()):
                    xval, yval = ROOT.Double(0), ROOT.Double(0)
                    hists[names_mc[i]].GetPoint(pt, xval, yval)

                    exl, exh, eyl, eyh = hists[names_mc[i]].GetErrorXlow(pt), hists[names_mc[i]].GetErrorXhigh(pt), hists[names_mc[i]].GetErrorYlow(pt), hists[names_mc[i]].GetErrorYhigh(pt)

                    hists[names_mc[i]].SetPoint(pt, xval/1000., yval)
                    hists[names_mc[i]].SetPointError(pt, exl/1000., exh/1000., eyl, eyh)


        # Only use points that have both data and MC in the inputs...
        if 'MidPt_effVsMu' in plot_key:
            new_tgraph_data = ROOT.TGraphAsymmErrors()
            new_tgraph_mc = ROOT.TGraphAsymmErrors()
            pt_ctr = 0
            for pt in range(hists[h_name].GetN()):
                xval, yval = ROOT.Double(0), ROOT.Double(0)
                hists[h_name].GetPoint(pt, xval, yval)
                exl, exh, eyl, eyh = hists[h_name].GetErrorXlow(pt), hists[h_name].GetErrorXhigh(pt), hists[h_name].GetErrorYlow(pt), hists[h_name].GetErrorYhigh(pt)

                for pt_mc in range(hists[names_mc[i]].GetN()):
                    xval_mc, yval_mc = ROOT.Double(0), ROOT.Double(0)
                    hists[names_mc[i]].GetPoint(pt_mc, xval_mc, yval_mc)
                    exl_mc, exh_mc, eyl_mc, eyh_mc = hists[names_mc[i]].GetErrorXlow(pt_mc), hists[names_mc[i]].GetErrorXhigh(pt_mc), hists[names_mc[i]].GetErrorYlow(pt_mc), hists[names_mc[i]].GetErrorYhigh(pt_mc)
                    if xval == xval_mc:
                        new_tgraph_data.SetPoint(pt_ctr, xval, yval)
                        new_tgraph_data.SetPointError(pt_ctr, exl, exh, eyl, eyh)
                        new_tgraph_mc.SetPoint(pt_ctr, xval_mc, yval_mc)
                        new_tgraph_mc.SetPointError(pt_ctr, exl_mc, exh_mc, eyl_mc, eyh_mc)
                        pt_ctr += 1

            hists[h_name] = new_tgraph_data
            hists[names_mc[i]] = new_tgraph_mc


        # Style
        hists[h_name].SetLineColor(colors[i])
        hists[h_name].SetMarkerColor(colors[i])
        hists[h_name].SetMarkerStyle(marker_styles_data[i])

        # Set axis titles
        if vsPt:
            hists[h_name].SetTitle(';Muon p_{T} [GeV];Trigger efficiency')
        if vsEta:
            hists[h_name].SetTitle(';Muon #eta;Trigger efficiency')
        if vsPhi:
            hists[h_name].SetTitle(';Muon #phi;Trigger efficiency')
        if vsMRecVtx:
            hists[h_name].SetTitle(';Number of reconstructed vertices;Trigger efficiency')
        if vsMu:
            hists[h_name].SetTitle(';Average #mu;Trigger efficiency')
  
        # Add legend entry
        leg.AddEntry(hists[h_name], get_legend_title(h_name), 'ep')
  
        # Set plotting maxima and minima
        isTefficiency = 'TEfficiency' in hists[h_name].IsA().GetName()
        mini, maxi = get_plot_extrema(plot_key)
        if not isTefficiency:
            hists[h_name].SetMinimum(mini)
            hists[h_name].SetMaximum(maxi)
            if errorTest:
                hists[h_name].SetPointError(3,hists[h_name].GetErrorX(3),hists[h_name].GetErrorX(3),0.2,0.2)
        else:
            hists[h_name].Draw()
            p1.Update()
            tmpgraph = hists[h_name].GetPaintedGraph()
            tmpgraph.SetMinimum(mini)
            tmpgraph.SetMaximum(maxi)
            p1.Update()
            
            
    # Plot histograms
    ratio_min = 0.7
    ratio_max = 1.1
    if 'effVsMu_BA' in plot_key:
        ratio_max = 1.0
    if 'effVsMu_EC' in plot_key:
        ratio_min = 0.8
    if 'MidPt_effVsPt_BA' in plot_key:
        ratio_max = 1.0
    if 'MidPt_effVsPt_EC' in plot_key:
        ratio_min = 0.8
    for i in range(len(names_data)):
        draw_option = 'psame'
        if i==0:
            draw_option = 'ap'
            
        if len(names_mc) > 0:
            if debug:
                print('MC is: ' + hists[names_mc[i]].IsA().GetName())
                print('Data is: ' + hists[names_data[i]].IsA().GetName())

            # Top pad
            p1.cd()
            hists[names_data[i]].Draw(draw_option)
            hists[names_data[i]].GetXaxis().SetTitleSize(0.0)
            hists[names_data[i]].GetYaxis().SetTitleSize(y_title_size)
            hists[names_data[i]].GetXaxis().SetLabelSize(0.0)
            hists[names_data[i]].GetYaxis().SetLabelSize(axis_label_size)
            hists[names_data[i]].GetXaxis().SetTitleOffset(x_title_offset)
            hists[names_data[i]].GetYaxis().SetTitleOffset(y_title_offset)
            hists[names_data[i]].GetYaxis().SetLabelOffset(y_label_offset)
            
            # Bottom (ratio) pad
            p2.cd()
            rps.append(divide_tgraphs(hists[names_data[i]], hists[names_mc[i]],names_data[i]))
            rps[i].SetMinimum(ratio_min)
            rps[i].SetMaximum(ratio_max)
            rps[i].SetLineColor(colors[i])
            rps[i].SetMarkerColor(colors[i])
            rps[i].SetMarkerStyle(marker_styles_data[i])
            rps[i].Draw(draw_option)
            rps[i].GetXaxis().SetTitleSize(x_title_size*(1/ratio_frac-1))
            rps[i].GetYaxis().SetTitleSize(y_title_size*(1/ratio_frac-1))
            rps[i].GetXaxis().SetLabelSize(axis_label_size*(1/ratio_frac-1))
            rps[i].GetYaxis().SetLabelSize(axis_label_size*(1/ratio_frac-1))
            rps[i].GetYaxis().SetTitleOffset(y_title_offset*ratio_frac/(1-ratio_frac))
            rps[i].GetXaxis().SetTitleOffset(x_title_offset*ratio_frac/(1-ratio_frac))
            rps[i].GetYaxis().SetLabelOffset(y_label_offset)
            rps[i].GetYaxis().SetNdivisions(505)
            rps[i].GetYaxis().SetTitle('Data/MC')
            rps[i].GetXaxis().SetTitle(hists[names_data[i]].GetXaxis().GetTitle())
            #unityLine = ROOT.TLine()
            #unityLine.SetLineColor(ROOT.kBlack)
            #unityLine.DrawLineNDC(0.0,0.5,1.0,0.5)
            p2.Update()
            p1.cd()
        else:
            cnv_single.cd()
            hists[names_data[i]].Draw(draw_option)


        # Plot systematic uncertainty separately for HI
        if isHI and not vsPt:
            hists[names_data[i].replace('Eff_','syst_')].SetFillColor(colors[i])
            hists[names_data[i].replace('Eff_','syst_')].SetFillStyle(3004)
            hists[names_data[i].replace('Eff_','syst_')].Draw('same,e2')
            hists[names_data[i]].Draw('same,p')


    # Lumi plots need 2017 plotted first...
    if 'MidPt_effVsMu' in plot_key:
        p1.cd()
        hists[names_data[1]].GetXaxis().SetRangeUser(10, 100)
        hists[names_data[1]].Draw('ap')
        hists[names_data[0]].Draw('psame')
        hists[names_data[2]].Draw('psame')
        p2.cd()
        rps[1].GetXaxis().SetRangeUser(10, 100)
        rps[1].Draw('ap')
        rps[0].Draw('psame')
        rps[2].Draw('psame')
        p1.cd()
    
    #leg.SetNColumns(2)
    leg.Draw('same')
    draw_labels(plot_key)
    
    # Save efficiency plot as pdf
    if isDataMC(plot_key):
        cnv.SaveAs(out_dir + plot_key + '.pdf')
    else:
        cnv_single.SaveAs(out_dir + plot_key + '.pdf')

    # Delete canvases
    cnv.Close()
    cnv_single.Close()
    
if __name__ == "__main__":
    #if len(sys.argv) < 2:
    #    print('No input files provided, exiting.')
    #    sys.exit()
 
    #filenames = sys.argv[1:]
    
    print('Loading ' + str(len(filenames)) + ' files:')
    for f in filenames:
        print(f)
    
    # Get relevant histograms from input files
    hists = get_histograms(filenames)

    if hists == None:
        print('ERROR!! HISTOGRAM CONTAINER EMPTY, EXITING...')
        sys.exit()
        
    if debug:
        print('Histograms:')
        for h_name, h in hists.items():
            print(h_name + ' : ' + str(h))

    # Produce pretty plots for paper
    for plot_key, hist_list in paper_plots.items():
        if '_effVs' in plot_key:
            plot_efficiency_ratio(hists, hist_list, plot_key)
        else:
            produce_plot(hists, hist_list, plot_key)
    
    print('Done!')
