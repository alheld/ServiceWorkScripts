import sys
import ROOT
import glob

path = '/Users/sebastien/Downloads/MuTPaper/'

# Zmumu
f_out = ROOT.TFile(path+'Zmumu/Zmumu.root', 'RECREATE')
for year in ['2015','2016','2017','2018']:        
    if year == '2015':
        f_current = ROOT.TFile.Open(path+'Zmumu/20-'+year+'/eff_ratio.root')
    else:
        f_current = ROOT.TFile.Open(path+'Zmumu/26-'+year+'/eff_ratio.root')
    for k in f_current.GetListOfKeys():
        name = k.GetName()
        h = f_current.Get(name).Clone()
        h.SetName(h.GetName() + '_' + year)
        f_out.cd()
        h.Write()
    f_current.Close()

    # Want the average mu plots for the latest two updated files
    if year in ['2016','2017']:
        f_current = ROOT.TFile.Open(path+'Zmumu/updated/26-'+year+'-updated/eff_ratio.root')
        for k in f_current.GetListOfKeys():
            name = k.GetName()
            if '_average_mu_' not in name:
                continue
            h = f_current.Get(name).Clone()
            h.SetName(h.GetName() + '_' + year)
            f_out.cd()
            h.Write()
        f_current.Close()
f_out.Close()

# Jpsimumu
f_data = ROOT.TFile.Open(path+'JPsimumu/data17_13TeV.periodAllYear.physics_Main.triggerEfficiency.HLT_mu4_nomucomb.HTG.root')
f_MC = ROOT.TFile.Open(path+'JPsimumu/mc16_13TeV.424100.triggerEfficiency.HLT_mu4_nomucomb.HTG.root')
f_out = ROOT.TFile(path+'JPsimumu/JPsimumu.root', 'RECREATE')

for k in f_data.GetListOfKeys():
    name = k.GetName()
    h = f_data.Get(name).Clone()
    if 'TGraphAsymmErrors' not in h.IsA().GetName():
        continue
    h.SetName(h.GetName() + '_data')
    f_out.cd()
    h.Write()
f_data.Close()

for k in f_MC.GetListOfKeys():
    name = k.GetName()
    h = f_MC.Get(name).Clone()
    if 'TGraphAsymmErrors' not in h.IsA().GetName():
        continue
    h.SetName(h.GetName() + '_MC')
    f_out.cd()
    h.Write()
f_MC.Close()
f_out.Close()

# W+Jets & ttbar
for sel in ['wjets','ttbar']:
    f_out = ROOT.TFile(path+'HighPt/'+sel+'.root', 'RECREATE')
    for f_name in glob.glob(path+'HighPt/HLT*'+sel+'*'):
        f_current = ROOT.TFile.Open(f_name)
        for k in f_current.GetListOfKeys():
            name = k.GetName()
            h = f_current.Get(name).Clone()
            h.SetName(name)
            f_out.cd()
            h.Write()
        f_current.Close()
    f_out.Close()

# HI
f_HI_Low_pt = ROOT.TFile.Open(path+'HI/Low_pt/Eff_all_LowpT_periodC.root')
f_HI_Low_etaphi = ROOT.TFile.Open(path+'HI/Low_pt/Eff_syst_LowpT_periodC.root')
f_HI_High_pt = ROOT.TFile.Open(path+'HI/High_pt/Eff_all_HighpT_periodC.root')
f_HI_High_etaphi = ROOT.TFile.Open(path+'HI/High_pt/Eff_syst_HighpT_periodC.root')

f_HI_out = ROOT.TFile(path+'HI/HI.root', 'RECREATE')

for k in f_HI_Low_pt.GetListOfKeys():
    name = k.GetName()
    h = f_HI_Low_pt.Get(name).Clone()
    if 'TGraphAsymmErrors' not in h.IsA().GetName():
        continue
    h.SetName(h.GetName() + '_LowpT_periodC')
    f_HI_out.cd()
    h.Write()
f_HI_Low_pt.Close()

for k in f_HI_Low_etaphi.GetListOfKeys():
    name = k.GetName()
    h = f_HI_Low_etaphi.Get(name).Clone()
    if 'TGraphAsymmErrors' not in h.IsA().GetName():
        continue
    h.SetName(h.GetName() + '_LowpT_periodC')
    f_HI_out.cd()
    h.Write()
f_HI_Low_etaphi.Close()

for k in f_HI_High_pt.GetListOfKeys():
    name = k.GetName()
    h = f_HI_High_pt.Get(name).Clone()
    if 'TGraphAsymmErrors' not in h.IsA().GetName():
        continue
    h.SetName(h.GetName() + '_HighpT_periodC')
    f_HI_out.cd()
    h.Write()
f_HI_High_pt.Close()

for k in f_HI_High_etaphi.GetListOfKeys():
    name = k.GetName()
    h = f_HI_High_etaphi.Get(name).Clone()
    if 'TGraphAsymmErrors' not in h.IsA().GetName():
        continue
    h.SetName(h.GetName() + '_HighpT_periodC')
    f_HI_out.cd()
    h.Write()
f_HI_High_etaphi.Close()

f_HI_out.Close()
