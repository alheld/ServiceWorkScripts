# DoTight/DoLoose to activate the loose and tight trees
# each should be one in: Data, MC, Both, False
DoTight Both
DoLoose False

# Turn on MetaData to pull IsAFII from metadata
UseAodMetaData True
#IsAFII False # should not be needed any more

BTaggingWP MV2c10:FixedCutBEff_60 MV2c10:FixedCutBEff_70 MV2c10:FixedCutBEff_77 MV2c10:FixedCutBEff_85

#NEvents 100

