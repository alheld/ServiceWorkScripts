import sys
from itertools import compress
import rucio.client
# see /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/rucio-clients/1.13.2/lib/python2.7/site-packages/rucio/client/didclient.py
rucio_client = rucio.client.Client()

def getDSIDList(input_file, mc_identifier='mc16_'):
  """
  :param input_file: txt file containing one dsid/ file per line
  :returns: list of dsids for consideration
  """
  dsids = []
  with open(input_file) as f:
    for line in f.readlines():
      if mc_identifier not in line:
        continue
      elif line.lstrip()[0] == '#':
        continue # skip comments
      else:
        l = line.replace(",","").replace("\'","").replace("\"","").strip()
        dsids.append(l.split(".")[1])
  return dsids


def getDAODs(scope = 'mc16_13TeV', dsid='*', deriv='DAOD_TOPQ1',\
             rtag='*', ptag='*'):
  """
  :param scope: rucio scope
  :param dsid: dsid, wildcard by default
  :param deriv: derivation name
  :param ptag: p-tag
  :returns: list of matching containers
  """
  file_name = '*.' + dsid + '.*.' + deriv + '.*' + rtag + '*' +  ptag + '*'
  results = rucio_client.list_dids(scope, {'name': file_name},\
                                   type='container', long=False)
  matches = [r.encode("utf-8") for r in results] # translate to list
  return matches


def getUpdatedDAODs(dsid_list, matches):
  """
  :param dsid_list: output of getDSIDList()
  :param matches: output of getDAODs()
  :returns: updated list of DAODs with desired p-tags
  """
  for d in dsid_list:
    dsid_match_mask = [m.find(d) != -1 for m in matches]
    # check whether dsid was found with new p-tag
    if any(dsid_match_mask):
      results = list(compress(matches,dsid_match_mask))
      if len(results) > 1:
        print '!! found multiple matches', results
      else:
        print '  \'' + results[0] + '\','

    # no match, print message
    else:
      print d, 'has not been matched with new p-tag!'


if __name__ == '__main__':
  # get a list of datasets to be updated to new p-tags
  dsid_list = getDSIDList(sys.argv[-1])

  # find matching DAODs
  dsid = '*'
  rtag = 'r9315' # mc16a
  #rtag = 'r9778' # mc16c
  ptag ='3390'
  matches = getDAODs(dsid=dsid, rtag=rtag, ptag=ptag)

  # obtain new DAODs for mc16a
  getUpdatedDAODs(dsid_list, matches)
