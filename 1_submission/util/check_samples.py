#import TopExamples.grid
import sys
import rucio.client
rucio_client = rucio.client.Client()

def clean_line(line):
  line = line.strip()
  if len(line) == 0:
    return None
  if line[0] == "#" or line[0] != '\'':
    return None
  else:
    return line.strip(',').strip('\'')

if __name__ == '__main__':
  """
  Loop over *txt files in directory and check whether
  the samples contained within exist (done via rucio)
  """
  path_to_file = sys.argv[-1]
  if path_to_file == __file__:
    print "call via \"python " + __file__ + " path/to/file.py\""
    raise SystemExit

  with open(path_to_file) as f:
    for line in f.readlines():
      line = clean_line(line)

      if line == None:
        continue # skip comments and empty lines

      scope = line.split('.')[0].split(':')[0]
      dsid = line.split('.')[1]
      #print '  ', scope, line

      results = [r for r in rucio_client.list_dids(scope, {'name' : line})]

      if len(results) == 0:
        print '\n!!\n!! no dataset found for', line, '\n!!\n'

      for res in results:
        print '  found: ', res.encode("ascii")
