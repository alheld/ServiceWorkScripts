import TopExamples.grid

# #################################################################################################################################
# 2018 data
#
# settings:
#
# GRLDir  GoodRunsLists
# GRLFile data18_13TeV/20181111/physics_25ns_Triggerno17e33prim.xml
# PRWLumiCalcFiles GoodRunsLists/data18_13TeV/20181111/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-001.root
#
# L = 59937.2 pb^{-1} (see https://atlas-groupdata.web.cern.ch/atlas-groupdata/GoodRunsLists/data18_13TeV/20181111/notes.txt)
#
TopExamples.grid.Add('data_2018').datasets = [
  'data18_13TeV.AllYear.physics_Main.PhysCont.DAOD_TOPQ1.grp18_v02_p3544_p3553_p3583',
]
# #################################################################################################################################


# #################################################################################################################################
# 2017 data
#
# settings:
#
# GRLDir  GoodRunsLists
# GRLFile data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.xml
# PRWLumiCalcFiles GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root
#
# L = 44307.4 pb^{-1} (see http://atlas-groupdata.web.cern.ch/atlas-groupdata/GoodRunsLists/data17_13TeV/20180619/notes.txt)
#
TopExamples.grid.Add('data_2017').datasets = [
  'data17_13TeV.AllYear.physics_Main.PhysCont.DAOD_TOPQ1.grp17_v01_p3601',
]
# #################################################################################################################################


# #################################################################################################################################
# 2016 data
#
# settings:
#
# GRLDir  GoodRunsLists
# GRLFile data16_13TeV/20180129/physics_25ns_21.0.19.xml
# PRWLumiCalcFiles GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root
#
# L = 32988.1 pb^{-1} (see http://atlas-groupdata.web.cern.ch/atlas-groupdata//GoodRunsLists/data16_13TeV/20180129/notes.txt)
#
TopExamples.grid.Add('data_2016').datasets = [
  'data16_13TeV.AllYear.physics_Main.PhysCont.DAOD_TOPQ1.grp16_v01_p3601',
]
# #################################################################################################################################


# #################################################################################################################################
# 2015 data
#
# settings:
#
# GRLDir  GoodRunsLists
# GRLFile data15_13TeV/20170619/physics_25ns_21.0.19.xml
# PRWLumiCalcFiles GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root
#
# L = 3219.56 pb^{-1} (see http://atlas-groupdata.web.cern.ch/atlas-groupdata/GoodRunsLists/data15_13TeV/20170619/notes.txt)
#
TopExamples.grid.Add('data_2015').datasets = [
  'data15_13TeV.AllYear.physics_Main.PhysCont.DAOD_TOPQ1.grp15_v01_p3601',
]
# #################################################################################################################################
