### description of files for main workflow:
* see ```PRW``` for functionality to create PRW configs, which is needed first
* ```01SubmitToGrid.py``` submits jobs to grid
* ```cuts/``` contains a script to produce all needed cut files for all muon quality and isolation settings
* ```samples/data.py``` contains TOPQ1 derivations for 2015-2018 data
* ```samples/mc16a.py```, ```samples/mc16d.py```, ```samples/mc16e.py``` contain TOPQ1 derivations for mc16a-e

### important settings:
* ```config.suffix``` in ```01SubmitToGrid.py``` needs to be changed for every production

### optional scripts:
* ```util/check_samples.py``` is a script to check whether samples listed in the python files are found by rucio
* ```util/extract_vars.py``` takes a ```.root``` file as argument and lists weights and variables in the ```nominal``` tree
* ```util/CompareTotalEvents.py``` takes a list of samples in a ```.txt``` file as input and compares the amount of total events in the AOD and DAOD, in both rucio and AMI
* ```util/get_DAODs.py``` is used to update sample lists, expects the path to an existing list (e.g. ```samples/mc16a.py```) as an argument

### production naming scheme:
currently in use:
* ```59_med_noIso_v0```: nominal, [bigpanda link](https://bigpanda.cern.ch/tasks/?taskname=user.alheld*59_med_noIso_v0*&limit=50000&display_limit=50000&days=150)

Resubmissions of broken jobs continue with higher version number, e.g. ```v1```.
The  ```59``` denotes the AT version (currently 21.2.59).
