### info
* new script version available [here](https://gitlab.cern.ch/will/athena/blob/a622cb22766ef6fbcc46bcc878046028c2e0404a/PhysicsAnalysis/AnalysisCommon/PileupReweighting/scripts/generatePRW.py), [merge request](https://gitlab.cern.ch/atlas/athena/merge_requests/6809) 
* JIRA to track datasets affected by INFN-T1 flooding: [ANALYSISTO-443](https://its.cern.ch/jira/browse/ANALYSISTO-443)
* missing: 410011,410014,364188,364189,364190,364195,364119
* weird:
    * ```channel 364171 is incomplete (missing 150000 events from config files)```
    * ```channel 364164 is suspect! (config files have additional 2420000 events)```
    * ```channel 364136 is incomplete (missing 1377950 events from config files)```
* more events in AOD than DAOD:
    * mc16_13TeV.410560.MadGraphPythia8EvtGen_A14_tZ_4fl_tchan_noAllHad.deriv.DAOD_TOPQ1.e5803_s3126_r9364_r9315_p3215
* more events in DAOD than AOD:
    * mc16_13TeV.343366.aMcAtNloPythia8EvtGen_A14_NNPDF23_NNPDF30ME_ttH125_semilep.deriv.DAOD_TOPQ1.e4706_e5984_s3126_r9364_r9315_p3215
    * mc16_13TeV.410014.PowhegPythiaEvtGen_P2012_Wt_inclusive_antitop.deriv.DAOD_TOPQ1.e3753_s3126_r9364_r9315_p3215
    * mc16_13TeV.364171.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_TOPQ1.e5340_s3126_r9364_r9315_p3215
    * mc16_13TeV.364172.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_BFilter.deriv.DAOD_TOPQ1.e5340_s3126_r9364_r9315_p3215
    * mc16_13TeV.364173.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_TOPQ1.e5340_s3126_r9364_r9315_p3215
    * mc16_13TeV.364175.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_BFilter.deriv.DAOD_TOPQ1.e5340_s3126_r9364_r9315_p3215
    * mc16_13TeV.364178.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV140_280_BFilter.deriv.DAOD_TOPQ1.e5340_s3126_r9364_r9315_p3215
    * mc16_13TeV.364156.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_TOPQ1.e5340_s3126_r9364_r9315_p3215
    * mc16_13TeV.364166.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_TOPQ1.e5340_s3126_r9364_r9315_p3215
    * mc16_13TeV.364167.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV280_500_BFilter.deriv.DAOD_TOPQ1.e5340_s3126_r9364_r9315_p3215
    * mc16_13TeV.364190.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_TOPQ1.e5340_s3126_r9364_r9315_p3215
    * mc16_13TeV.364196.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV500_1000.deriv.DAOD_TOPQ1.e5340_s3126_r9364_r9315_p3215

### description of files for main workflow:
* ```01SetupEnvironment.sh``` sets up the environment needed
* ```02GenerateSampleList.py``` builds the ```.txt``` file input required to generate PRW config files. Point it to either ```1_submission/samples/mc16a.py``` or ```1_submission/samples/mc16c.py```
* ```03SubmitToGrid.sh``` submits a grid job to obtain PRW config files
    * change ```PRW_ID``` in the script to track different versions of PRW configs
    * the script might output something along the lines of "There was an error ...", but this is probably (?) fine as long as a job was successfully submitted (i.e. you saw "INFO : succeeded. new jediTaskID=XXXXXXXX")

### optional steps:
* check validity of generated PRW configs by calling ```checkPRW.py --inDsTxt=prw_input.txt path/to/prwconfig.root```
* see the [ExtendedPileupReweighting twiki](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/ExtendedPileupReweighting#Checking_PRW_Config_files) for further details
