# #################
# SETTINGS
PRW_ID="prw_mc16a_v4"      # id to keep track of versions, will be used in grid job name
input_list="prw_input.txt" # file produced by 02GenerateSampleList.py
# #################

counter=0                  # keeps track of current position in input_list
tmp_file_name="tmp.txt"    # temporary file

while read f; do
 echo $f > $tmp_file_name
 generatePRW.py --inDsTxt=$tmp_file_name --outDS="user."$USER"."$PRW_ID$"."$counter
 let "counter++"
done <$input_list

rm $tmp_file_name

echo ""
echo "track the job here:"
echo "https://bigpanda.cern.ch/task/jediTaskID/"
echo "where \"jediTaskID\" was given above"
