import sys

mc_identifier = 'mc16_13TeV'
output_file   = 'prw_input.txt'

if __name__ == '__main__':
  list_location = sys.argv[-1]
  if list_location == __file__:
    print "call via \"python", __file__, "path/to/list.py\""
    raise SystemExit

  with open(list_location, 'r') as f:
    lines = f.readlines()

  output_list = []

  for l in lines:
    if mc_identifier not in l:
      continue
    if l.lstrip()[0] == '#':
      continue # skip comments

    output_list.append(l.replace(",","").replace("\'","").replace("\"","").strip())

  with open(output_file, 'w') as f:
    for l in output_list:
      f.write(l+'\n')
